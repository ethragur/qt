#pragma once


#include "pagingalgo.h"
#include <QObject>
#include <QVector>
#include <QList>
#include <QMap>
#include <memory>
#include <QVariant>
#include "pageframe.h"



enum Algorithm
{
	LIFO = 0,
	LRU,
	OPTIMAL,
	CLOCK,
};

//Pair to store a shared pointer of an Algorithm implementation and flag if the algorithm has been solved
typedef struct t_AlgoPair
{
	bool solve;
	std::shared_ptr<PagingAlgorithm> pagingAlgorithm;

	t_AlgoPair(const bool a_solve, const std::shared_ptr<PagingAlgorithm> & a_pagingAlgorithm)
		: solve(a_solve), pagingAlgorithm(a_pagingAlgorithm) {}

} AlgoPair;


class PagingController : public QObject
{
	Q_OBJECT
		Q_PROPERTY(int inputLength READ inputLength WRITE setInputLength NOTIFY inputLengthChanged)
		Q_PROPERTY(int pageNr READ pageNr WRITE setPageNr NOTIFY pageNrChanged)
		Q_PROPERTY(int pageFramesCount READ pageFramesCount WRITE setPageFramesCount NOTIFY pageFramesCountChanged)
		Q_PROPERTY(int pagingAlgo READ pagingAlgo WRITE setPagingAlgo NOTIFY pagingAlgoChanged)
		Q_PROPERTY(QVector<int> input READ input WRITE setInput NOTIFY inputChanged)

	public:
		explicit PagingController(QObject *parent = nullptr);


		int inputLength() const;
		int pageNr() const;
		int pageFramesCount() const;
		int pagingAlgo() const;
		QVector<int> input() const;

		void setInputLength(const int inputLength);
		void setPageNr(const int pageNr);
		void setPageFramesCount(const int pageFramesCount);
		void setPagingAlgo(const int pagingAlgo);
		void setInput(const QVector<int> m_input);


		Q_INVOKABLE QPoint getFrame(int step, int i);
		Q_INVOKABLE QPoint getFrame(int step, int i, int algorithm);
		Q_INVOKABLE void prevStep();
		Q_INVOKABLE QVector<int> getPageFault(int step);
		Q_INVOKABLE QVector<int> getPageFault(int step, int algorithm);
		Q_INVOKABLE int getPageFaultsCount(int algorithm);
		Q_INVOKABLE int getPageFaultsCount();
		Q_INVOKABLE int getPointerPos(int step);
		Q_INVOKABLE int getPointerPos(int step, int algorithm);
		Q_INVOKABLE void refresh();

signals:
		void inputLengthChanged();
		void pageNrChanged();
		void pageFramesCountChanged();
		void pagingAlgoChanged();
		void inputChanged();

	private:
		void resolveAll();

		int m_inputLength;
		int m_pageNr;
		int m_pageFramesCount;
		int m_step;
		Algorithm	 m_pagingAlgo;
		QVector<int> m_input;

		// bool flag if algorithm has to be solved again (changes in input)
		QMap<Algorithm, AlgoPair> m_AlgoImplementation;
};
