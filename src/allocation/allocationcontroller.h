#pragma once


#include <QObject>
#include <QVector>
#include <QPoint>
#include <QMap>
#include <memory>
#include "allocationalgo.h"

enum AllocAlgorithm
{
	FIRST = 0,
	NEXT,
	BEST,
	WORST,
};

class AllocationController : public QObject
{
	Q_OBJECT
	Q_PROPERTY(int memorySize READ memorySize WRITE setMemorySize NOTIFY memorySizeChanged)
	Q_PROPERTY(QVector<int> memoryList READ memoryList WRITE  setMemoryList NOTIFY memoryListChanged)
	Q_PROPERTY(AllocAlgorithm allocAlgo READ allocAlgo WRITE setAllocAlgo NOTIFY allocAlgoChanged)
	Q_PROPERTY(int pointerPos READ pointerPos WRITE setPointerPos NOTIFY pointerPosChanged)

public:
		explicit AllocationController(QObject *parent = nullptr);

		QVector<int> memoryList() const;
		AllocAlgorithm allocAlgo() const;
		int memorySize();
		int pointerPos() const;

		void setMemoryList(const QVector<int> memoryList);
		void setMemorySize(const int memorySize);
		void setAllocAlgo(const AllocAlgorithm allocAlgo);
		void setPointerPos(const int pointerPos);

		Q_INVOKABLE void fillRandomMemory();

		Q_INVOKABLE bool insertBlock(const int blockSize, const int blockID, const int allocAlgo);
		Q_INVOKABLE bool insertBlockFirstFit(const int blockSize, const int blockID);
		Q_INVOKABLE bool insertBlockNextFit(const int blockSize, const int blockID);
		Q_INVOKABLE bool insertBlockBestFit(const int blockSize, const int blockID);
		Q_INVOKABLE bool insertBlockWorstFit(const int blockSize, const int blockID);

		Q_INVOKABLE float getFreeRation();
		Q_INVOKABLE int getFreeSpace();
		Q_INVOKABLE float getFragmentation();

		Q_INVOKABLE bool evaluate();
		Q_INVOKABLE QVector<int> getMemoryList(int algorithm);


signals:
		void memorySizeChanged();
		void memoryListChanged();
		void allocAlgoChanged();
		void pointerPosChanged();

private:
		//using QVector so i can pass the data to qml, normally a linked list should be used
		// 0 means unused, > 1 is the id
		QVector<int>  m_memoryList;
		int m_memorySize;
		int m_pointerPos;
		AllocAlgorithm m_allocAlgo;
		QVector<QPair<int,int>> m_input;

		QMap<AllocAlgorithm, std::shared_ptr<AllocationAlgo>> m_algoImpl;
	
};
