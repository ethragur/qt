import "./buttons"
import "./input"
import "./output"
import QtQuick 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.0


import Paging 1.0

Page {
    id: pagingView

    property int iCurrentStep: 0
    property var colorArray: ["lightgrey", "aqua", "chartreuse", "crimson", "dodgerblue", "fuchsia", "gold", "lightgreen", "orange", "pink", "teal", "peru", "navy", "lightsteelblue", "yellow", "red"]

    PagingController {
        id: pagingController
    }

    Timer {
        id: timer
        function setTimeout(cb, delayTime) {
            timer.interval = delayTime;
            timer.repeat = true;
            timer.triggered.connect(cb);
            timer.start();
        }
    }

    ColumnLayout {
        visible: true
        width: parent.width

        Rectangle {
            height: parent.height / 16
            Layout.fillWidth: true

            Hint {
                text: "Main menu"
                width: 80
                height: 20
                anchors.bottom: menuButton.top
                anchors.horizontalCenter: menuButton.horizontalCenter
                visible: menuMouseArea.containsMouse
            }

            //Menu button
            Rectangle {
                id: menuButton
                anchors.left: parent.left
                anchors.leftMargin: 20
                anchors.top: parent.top
                anchors.topMargin: 20

                width: 40
                height: 40
                border.color: "#828282"
                border.width: 2

                Rectangle {
                    anchors.left: parent.left
                    anchors.leftMargin: 5
                    anchors.top: parent.top
                    anchors.topMargin: 5

                    width: 13
                    height: 13
                    color: "#828282"
                }

                Rectangle {
                    anchors.right: parent.right
                    anchors.rightMargin: 5
                    anchors.top: parent.top
                    anchors.topMargin: 5

                    width: 13
                    height: 13
                    color: "#828282"
                }

                Rectangle {
                    anchors.left: parent.left
                    anchors.leftMargin: 5
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 5

                    width: 13
                    height: 13
                    color: "#828282"
                }

                Rectangle {
                    anchors.right: parent.right
                    anchors.rightMargin: 5
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 5

                    width: 13
                    height: 13
                    color: "#828282"
                }

                MouseArea {
                    id: menuMouseArea
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    hoverEnabled: true
                    onClicked: stackView.pop("Paging.qml")
                }
            }


            //Heading
            Text {
                id: header
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 20
                color: "#828282"
                font.family: "Ubuntu"
                font.pixelSize: 28
                font.bold: true
                font.underline: true
                text: qsTr("Paging")
            }
            //__Input__ site switcher
            Rectangle {
                id: inputSelection

                property bool onPage: swipeView.currentIndex == 0

                height: 30
                width: 100
                anchors.bottom: header.bottom
                anchors.bottomMargin: -20
                anchors.left: parent.left
                anchors.leftMargin: parent.width/4 - 50
                opacity: onPage ? 1 : 0.3

                OwnText {
                    anchors.centerIn: parent
                    font.pixelSize: 20
                    font.bold: parent.onPage ? true : false
                    font.underline: true
                    text: qsTr("    Input    ")
                }

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: swipeView.setCurrentIndex(0)
                }
            }

            //__Output__ site switcher
            Rectangle {
                id: outputSelection

                property bool onPage: swipeView.currentIndex == 1

                height: 30
                width: 100
                anchors.bottom: header.bottom
                anchors.bottomMargin: -20
                anchors.left: parent.left
                anchors.leftMargin: 3*(parent.width/4) - 50
                opacity: onPage ? 1 : 0.3

                OwnText {
                    anchors.centerIn: parent
                    font.pixelSize: 20
                    font.bold: parent.onPage ? true : false
                    font.underline: true
                    text:qsTr("    Output    ")
                }

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: {pagingController.refresh(); swipeView.setCurrentIndex(1)}
                }
            }

        }
    }





    //horizontal line between heading and body
    Rectangle {
        id: headerSeperator
        width: parent.width
        height: 1
        color: "#828282"
        anchors.top: parent.top
        anchors.topMargin: 75
    }

    SwipeView {
        id: swipeView
        currentIndex: 0
        width: parent.width
        height: parent.height - 80
        anchors.top: headerSeperator.bottom
        anchors.topMargin: 10

        Item {
            id: inputPage
            width: swipeView.width - 15
            height: swipeView.height - 2



            Rectangle {
                id: pagingMain
                height: parent.height/12
                Layout.fillWidth: true
                anchors.top: inputPage.top
                anchors.left: inputPage.left
                border.color: "#828282"
                border.width: 1
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    id: pagenrHeading
                    color: "#828282"
                    text: "# of Pages: "
                    font.family: "Ubuntu"
                    font.bold: true
                    font.pixelSize: 24
                    anchors.top: parent.top
                    anchors.margins: 20
                    anchors.verticalCenter: parent.verticalCenter
                }

                SpinBox {
                    id: pageNumbers
                    font.family: "Ubuntu"
                    font.pixelSize: 24
                    value: pagingController.pageNr
                    anchors.left: pagenrHeading.right
                    anchors.top: parent.top
                    anchors.margins: 20
                    onValueChanged: pagingController.pageNr = value
                    ToolTip.visible: hovered
                    ToolTip.text: qsTr("The number of different Pages in Memory")
                }

                Text {
                    id:pageFramesHeading
                    color: "#828282"
                    text: "# of Frames: "
                    font.family: "Ubuntu"
                    font.bold: true
                    font.pixelSize: 24
                    anchors.left: pageNumbers.right
                    anchors.top: parent.top
                    anchors.margins: 20
                    anchors.verticalCenter: parent.verticalCenter
                }

                SpinBox {
                    id: pageFrames
                    font.family: "Ubuntu"
                    font.pixelSize: 28
                    value: pagingController.pageFramesCount
                    anchors.left: pageFramesHeading.right
                    anchors.top: parent.top
                    anchors.margins: 20
                    onValueChanged: pagingController.pageFramesCount = value
                    ToolTip.visible: hovered
                    ToolTip.text: qsTr("The number of Page Frames in our Memory Model")
                }

                Text {
                    id: inputLengthHeading
                    color: "#828282"
                    text: "# of Memory Acceses: "
                    font.family: "Ubuntu"
                    font.bold: true
                    font.pixelSize: 24
                    anchors.left: pageFrames.right
                    anchors.top: parent.top
                    anchors.margins: 20
                    anchors.verticalCenter: parent.verticalCenter
                }

                SpinBox {
                    id: inputLength
                    font.family: "Ubuntu"
                    value: pagingController.inputLength
                    font.pixelSize: 24
                    anchors.left: inputLengthHeading.right
                    anchors.top: parent.top
                    anchors.margins: 20
                    onValueChanged: pagingController.inputLength = value
                    ToolTip.visible: hovered
                    ToolTip.text: qsTr("The amount of memory accesses")
                }

                Text {
                    id: algorithmHeading
                    color: "#828282"
                    text: "Algorithm: "
                    font.family: "Ubuntu"
                    font.bold: true
                    font.pixelSize: 24
                    anchors.left: inputLength.right
                    anchors.top: parent.top
                    anchors.margins: 20
                    anchors.verticalCenter: parent.verticalCenter
                }

                ComboBox {
                    id: algorithm
                    width: 200
                    font.family: "Ubuntu"
                    font.bold: true
                    font.pixelSize: 24
                    model: [ "FIFO", "LRU", "Optimal", "Clock" ]
                    anchors.left: algorithmHeading.right
                    anchors.top: parent.top
                    anchors.margins: 20
                    ToolTip.visible: hovered
                    ToolTip.text: qsTr("The Algorithm that is in use for replacing pages")
                    onActivated: pagingController.pagingAlgo = currentIndex
                }

                //calculate button
                Rectangle {
                    id: generateButton
                    border.color: "#828282"
                    border.width: 1
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.margins: 20
                    color: generateButtonArea.containsMouse ? "#dddddd" : "#ffffff"
                    width: 240
                    height: 64

                    signal clicked()

                    Text {
                        text: qsTr("Generate Randomly")
                        anchors.centerIn: parent
                        color: "#000000"
                        font.family: "Ubuntu"
                        font.pixelSize: 16
                        font.bold: true
                        ToolTip.visible: hovered
                        ToolTip.text: qsTr("Generates a new Random Memory Access Pattern")
                    }

                    MouseArea {
                        id: generateButtonArea
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        hoverEnabled: true
                        onClicked: {
                            pagingController.pageNr = pagingController.pageNr;
                            iCurrentStep = 0;
                        }
                    }

                }

            }

            Rectangle {
                id: inputArea
                anchors.top: pagingMain.bottom
                height: parent.height/5
                anchors.horizontalCenter: parent.horizontalCenter
                Layout.fillWidth: true



                ColumnLayout {
                    visible: true
                    width: parent.width
                    height: parent.height
                    anchors.horizontalCenter: parent.horizontalCenter
                    Layout.fillWidth: true

                    spacing: 40

                    Rectangle {
                        id: pageInputArea
                        Layout.alignment: Qt.AlignTop
                        Layout.fillWidth: true
                        anchors.horizontalCenter: parent.horizontalCenter
                        border.color: "#828282"
                        border.width: 1

                        GridLayout {
                            id:  inputGrid
                            anchors.top: pageInputArea.top
                            columns: 32
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            rowSpacing: 36
                            columnSpacing: 0
                            anchors.margins: 20
                            Repeater {
                                id: inputRepeater
                                model: pagingController.inputLength 

                                Rectangle {
                                    width: 50
                                    height: 50
                                    id: inputItem
                                    property var itemIndex: pagingController.input[index]
                                    color: inputListArea.containsMouse ? "steelblue" : colorArray[itemIndex % colorArray.length];
                                    border { width: 1; color: "black" }
                                    MouseArea {
                                        id: inputListArea
                                        anchors.fill: parent
                                        cursorShape: Qt.PointingHandCursor
                                        onClicked: {
                                        }
                                    }
                                    TextInput {
                                        id: textEdit
                                        text: parent.itemIndex
                                        anchors.centerIn: parent
                                        color: "#000000"
                                        font.family: "Ubuntu"
                                        font.pixelSize: 24
                                        font.bold: true
                                        inputMethodHints: Qt.ImhDigitsOnly
                                        validator: IntValidator { bottom: 1; top: pagingController.pageNr }
                                        Keys.onPressed: {
                                            if (event.key == Qt.Key_Tab) {
                                                event.accepted = true;
                                                textEdit.deselect();
                                                textEdit.focus = false;
                                                if( index + 1 < pagingController.inputLength && textEdit.acceptableInput) {
                                                    inputRepeater.itemAt(index + 1).children[1].selectAll();
                                                    inputRepeater.itemAt(index + 1).children[1].focus = true;
                                                } 
                                            }
                                        }
                                        onEditingFinished: {
                                            if(textEdit.acceptableInput) {
                                                pagingController.input[index] = textEdit.text;
                                            }
                                        }
                                    }
                                    Arrow {
                                        id: inputArrow
                                        anchors.top: parent.bottom
                                        visible: (index == iCurrentStep) ? true : false
                                    }
                                }
                            }
                        }
                    }
                }
            }

            RowLayout {
                id: pageFramesArea
                anchors.top: inputArea.bottom
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                Layout.fillWidth: true

                Rectangle {
                    Layout.preferredWidth: (2*parent.width) /3
                    Layout.fillHeight: true
                    border.color: "#828282"
                    border.width: 1
                    color: "#e2e2e2"


                    Text {
                        id: pageFramesHeader
                        text: "Page Frames"
                        color: "#000000"
                        font.family: "Ubuntu"
                        font.pixelSize: 24
                        font.bold: true
                        font.underline: true
                    }

                    Row {
                        id: registerHeader
                        anchors.left: parent.left
                        anchors.top: pageFramesHeader.bottom

                        anchors.margins: 40
                        spacing: 30

                        Text {
                            text: "Frames:"
                            color: "#000000"
                            font.family: "Ubuntu"
                            font.pixelSize: 24
                            font.bold: true
                            width: 200
                        }



                        Repeater {
                            model: pagingController.pageFramesCount

                            delegate: Rectangle {
                                width: 50
                                height: 50
                                color: prevStepButtonArea1.containsMouse ? "red" : "pink"
                                border { width: 1; color: "black" }
                                MouseArea {
                                    id: prevStepButtonArea1
                                    anchors.fill: parent
                                    cursorShape: Qt.PointingHandCursor
                                    hoverEnabled: true
                                    onClicked: {
                                    }
                                }
                                Text {
                                    text: String.fromCharCode(65+ index);
                                    anchors.centerIn: parent
                                    color: "#000000"
                                    font.family: "Ubuntu"
                                    font.pixelSize: 16
                                    font.bold: true
                                }

                            }
                        }
                    }



                    ScrollView {
                        id: scrollView
                        anchors.top: registerHeader.bottom
                        anchors.left: parent.left
                        anchors.margins: {left: 40}
                        clip: true
                        Layout.fillHeight: true


                        height: 460
                        width: parent.width



                        Column {
                            id: stepColumns
                            Layout.fillWidth: true
                            height: 20

                            Repeater {
                                id: stepColumnsRepeater
                                model: iCurrentStep + 1
                                Row {
                                    spacing: 30
                                    property var rowIndex: index

                                    Text {
                                        width: 200
                                        text: "Step " + ((iCurrentStep) - index) + ":"
                                        color: "#000000"
                                        font.family: "Ubuntu"
                                        font.pixelSize: 24
                                        font.bold: true
                                    }
                                    property var pageFault:  pagingController.getPageFault(iCurrentStep - (index))
                                    property var pointerPos:  pagingController.getPointerPos(iCurrentStep - index) 

                                    Repeater {
                                        model: pagingController.pageFramesCount 
                                        id: stepRepeater

                                        delegate: Item {
                                            width: 50
                                            height: 50

                                            RectangularGlow {
                                                id: pageFaultEffect
                                                anchors.fill: pageFrameContent 
                                                glowRadius: 10
                                                spread: 0.2
                                                color: "red"
                                                cornerRadius: glowRadius
                                                visible: (parent.parent.pageFault[0] == index && parent.parent.pageFault[1] != 0)
                                            }

                                            RectangularGlow {
                                                id: pointerPosEffect
                                                anchors.fill: pageFrameContent 
                                                glowRadius: 10
                                                spread: 0.2
                                                color: "green"
                                                cornerRadius: glowRadius
                                                visible: index == parent.parent.pointerPos
                                            }

                                            Rectangle {
                                                id: pageFrameContent
                                                anchors.fill: parent
                                                property var pageFrame:  pagingController.getFrame(iCurrentStep - rowIndex, index)
                                                property var lastAccessedIndex: pageFrame.x
                                                property var itemIndex: pageFrame.y
                                                color: stepRowFields.containsMouse ? "steelblue" :  colorArray[itemIndex % colorArray.length];

                                                border { width: 1; color: "black"}
                                                MouseArea {
                                                    id: stepRowFields
                                                    anchors.fill: parent
                                                    cursorShape: Qt.PointingHandCursor
                                                    hoverEnabled: true
                                                    onClicked: {
                                                        console.log(Object.keys(pagingController.getFrame(iCurrentStep -(index))));
                                                    }
                                                }
                                                Text {
                                                    text: parent.itemIndex
                                                    anchors.centerIn: parent
                                                    color: "#000000"
                                                    font.family: "Ubuntu"
                                                    font.pixelSize: 20 
                                                    font.bold: true
                                                }
                                                Text {
                                                    text: parent.lastAccessedIndex 
                                                    anchors.top: parent.top
                                                    anchors.right: parent.right
                                                    color: "#000000"
                                                    font.family: "Ubuntu"
                                                    font.pixelSize: 12 
                                                    font.bold: true
                                                }

                                            }
                                        }
                                    }

                                    Rectangle {
                                        id: seperatorPageFault
                                        width: 2 
                                        height: 50
                                        border { width: 2; color: "black" }
                                    }

                                    Item {
                                        anchors.left: seperatorPageFault.right
                                        anchors.margins: 40
                                        visible: ( parent.pageFault[1] != 0)

                                        property var oldPage: parent.pageFault[1]
                                        property var newPage: parent.pageFault[2]

                                        Rectangle {
                                            id: oldPageFrameContent
                                            width: 50
                                            height: 50
                                            color: (iCurrentStep - (index) == 0) ? "white" : colorArray[parent.oldPage % colorArray.length];

                                            border { width: 1; color: "black" }
                                            MouseArea {
                                                id: pageFrameOld
                                                anchors.fill: parent
                                                cursorShape: Qt.PointingHandCursor
                                                hoverEnabled: true
                                                onClicked: {
                                                }
                                            }
                                            Text {
                                                text: parent.parent.oldPage
                                                anchors.centerIn: parent
                                                color: "#000000"
                                                font.family: "Ubuntu"
                                                font.pixelSize: 16
                                                font.bold: true
                                            }
                                        }

                                        Arrow {
                                            id: replaceArrow
                                            anchors.margins: 75
                                            anchors.left: oldPageFrameContent.right

                                            transform: Rotation { angle: 90 }
                                        }
                                        Rectangle {
                                            id: newPageFrameContent
                                            anchors.left: replaceArrow.right
                                            width: 50
                                            height: 50
                                            color: (iCurrentStep - (index) == 0) ? "white" : colorArray[parent.newPage % colorArray.length];

                                            border { width: 1; color: "black" }
                                            MouseArea {
                                                id: pageFrameReplace
                                                anchors.fill: parent
                                                cursorShape: Qt.PointingHandCursor
                                                hoverEnabled: true
                                                onClicked: {
                                                }
                                            }
                                            Text {
                                                text: parent.parent.newPage
                                                anchors.centerIn: parent
                                                color: "#000000"
                                                font.family: "Ubuntu"
                                                font.pixelSize: 16
                                                font.bold: true
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                Rectangle {
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.width/3
                    border.color: "#828282"
                    border.width: 1

                    color: "#f4f4f4"
                    Text {
                        text: ""
                        anchors.centerIn: parent
                        color: "#000000"
                        font.family: "Ubuntu"
                        font.pixelSize: 16
                        font.bold: true
                    }
                }
            }
            Rectangle {
                id: prevStepButton
                border.color: "#828282"
                border.width: 1

                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.margins: 20
                color: prevStepButtonArea.containsMouse ? "#dddddd" : "#ffffff"
                width: 240
                height: 64

                signal clicked()

                Text {
                    text: qsTr("Previous Step")
                    anchors.centerIn: parent
                    color: "#000000"
                    font.family: "Ubuntu"
                    font.pixelSize: 16
                    font.bold: true
                }

                MouseArea {
                    id: prevStepButtonArea
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    hoverEnabled: true
                    onClicked: {
                        if(iCurrentStep > 0)
                        {
                            iCurrentStep -= 1;
                            pagingController.prevStep();
                        }
                    }
                }
            }


                Rectangle {
                    id: nextStepButton
                    border.color: "#828282"
                    border.width: 1

                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.margins: 20
                    color: nextStepButtonArea.containsMouse ? "#dddddd" : "#ffffff"
                    width: 240
                    height: 64

                    signal clicked()

                    Text {
                        text: qsTr("Next Step")
                        anchors.centerIn: parent
                        color: "#000000"
                        font.family: "Ubuntu"
                        font.pixelSize: 16
                        font.bold: true
                    }

                    MouseArea {
                        id: nextStepButtonArea
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        hoverEnabled: true
                        onClicked: {
                            if(iCurrentStep < pagingController.inputLength)
                            iCurrentStep += 1;
                        }
                    }

                }

                Rectangle {
                    id: runButton
                    border.color: "#828282"
                    border.width: 1

                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.margins: 20
                    color: runButtonArea.containsMouse ? "#dddddd" : "#ffffff"
                    width: 240
                    height: 64

                    signal clicked()

                    Text {
                        text: qsTr("Run Automatically")
                        anchors.centerIn: parent
                        color: "#000000"
                        font.family: "Ubuntu"
                        font.pixelSize: 16
                        font.bold: true
                    }

                    MouseArea {
                        id: runButtonArea
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        hoverEnabled: true
                        onClicked: {
                            timer.setTimeout(function() {
                                if(iCurrentStep < pagingController.inputLength)
                                iCurrentStep +=1;
                                else
                                timer.stop();
                            }, 100);
                        }
                    }

                }
        }
        Item {
            id: outputPage
            width: swipeView.width - 15
            height: swipeView.height - 2

            Rectangle {
                id: inputPostView
                width: parent.width
                height: parent.height - 20
                anchors.topMargin: 10
                anchors.fill: parent
                anchors.leftMargin: 20
                anchors.rightMargin: 20
                border.color: "#828282"
                border.width: 2

                ScrollView {
                    clip: true
                    width: parent.width
                    height: parent.height
                    contentHeight: parent.height * 2
                    ScrollBar.vertical.policy: ScrollBar.AlwaysOn 

                    RowLayout {
                        Layout.fillWidth: true
                        height: 1080
                        width: parent.width
                        anchors.top: parent.top

                        Repeater {
                            model: 4
                            Rectangle {
                                width: 440
                                Layout.fillHeight: true
                                property var algorithmIndex: index
                                anchors.top: parent.top


                                Text {
                                    id: algorithmCompHeader
                                    color: "#828282"
                                    text: "Algorithm: "
                                    font.family: "Ubuntu"
                                    font.bold: true
                                    font.pixelSize: 24
                                    anchors.topMargin: 20
                                }

                                Text {
                                    id: algorithmName
                                    color: "#828282"
                                    text: parent.algorithmIndex == 0 ? "LIFO" : parent.algorithmIndex == 1 ? "LRU" : parent.algorithmIndex == 2 ? "Optimal" : "Clock"
                                    font.family: "Ubuntu"
                                    font.bold: true
                                    font.pixelSize: 24
                                    anchors.topMargin: 20
                                    anchors.left: algorithmCompHeader.right
                                }
                                Row {
                                    id: pageFaultsRow
                                    anchors.top: algorithmName.bottom

                                    Text{
                                        id: countPageFaultsHeader 
                                        color: "#828282"
                                        text: "Pagefaults: "
                                        font.family: "Ubuntu"
                                        font.bold: true
                                        font.pixelSize: 24
                                        anchors.topMargin: 20
                                    }
                                    Text{
                                        id: countPageFaults
                                        color: "#828282"
                                        text: pagingController.getPageFaultsCount(parent.parent.algorithmIndex);
                                        font.family: "Ubuntu"
                                        font.bold: true
                                        font.pixelSize: 24
                                        anchors.topMargin: 20
                                    }

                                }
                                Column {
                                    anchors.top: pageFaultsRow.bottom
                                    id: stepColumns
                                    height: 20

                                    Repeater {
                                        id: stepColumnsRepeater
                                        model: pagingController.inputLength + 1
                                        Row {
                                            spacing: 10
                                            property var rowIndex: index

                                            property var pageFault:  pagingController.getPageFault(pagingController.inputLength - (index), parent.parent.algorithmIndex)
                                            property var pointerPos:  pagingController.getPointerPos(pagingController.inputLength - index, parent.parent.algorithmIndex) 

                                            Repeater {
                                                model: pagingController.pageFramesCount 
                                                id: stepRepeater

                                                delegate: Item {
                                                    width: 50
                                                    height: 50

                                                    RectangularGlow {
                                                        id: pageFaultEffect
                                                        anchors.fill: pageFrameContent 
                                                        glowRadius: 10
                                                        spread: 0.2
                                                        color: "red"
                                                        cornerRadius: glowRadius
                                                        visible: (parent.parent.pageFault[0] == index && parent.parent.pageFault[1] != 0)
                                                    }
                                                    RectangularGlow {
                                                        id: pointerPosEffect
                                                        anchors.fill: pageFrameContent 
                                                        glowRadius: 10
                                                        spread: 0.2
                                                        color: "green"
                                                        cornerRadius: glowRadius
                                                        visible: index == parent.parent.pointerPos
                                                    }

                                                    Rectangle {
                                                        id: pageFrameContent
                                                        anchors.fill: parent
                                                        property var itemIndex: pagingController.getFrame(pagingController.inputLength - rowIndex, index,parent.parent.parent.parent.algorithmIndex).y
                                                        property var lastAccessedIndex: pagingController.getFrame(pagingController.inputLength - rowIndex, index,parent.parent.parent.parent.algorithmIndex).x
                                                        color: stepRowFields.containsMouse ? "steelblue" :  colorArray[itemIndex % colorArray.length];

                                                        border { width: 1; color: "black"}
                                                        MouseArea {
                                                            id: stepRowFields
                                                            anchors.fill: parent
                                                            cursorShape: Qt.PointingHandCursor
                                                            hoverEnabled: true
                                                            onClicked: {
                                                            }
                                                        }
                                                        Text {
                                                            text: parent.itemIndex
                                                            anchors.centerIn: parent
                                                            color: "#000000"
                                                            font.family: "Ubuntu"
                                                            font.pixelSize: 20 
                                                            font.bold: true
                                                        }
                                                        Text {
                                                            text: parent.lastAccessedIndex 
                                                            anchors.top: parent.top
                                                            anchors.right: parent.right
                                                            color: "#000000"
                                                            font.family: "Ubuntu"
                                                            font.pixelSize: 12 
                                                            font.bold: true
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Rectangle {
        id: errorMessage
        height: parent.height/4 + 20
        width: parent.width
        color: "#E53935"
        anchors.verticalCenter: parent.verticalCenter
        visible: processController.isErrorActive()

        Connections {
            target: processController
            onDataChanged: {
                errorMessage.visible = processController.isErrorActive()
                errorHeading.text = processController.getErrorHeading()
                errorDescription.text = processController.getErrorDescription()
            }
        }


        Text {
            id: errorHeading
            color: "#ffffff"
            text: "Error"
            font.family: "Ubuntu"
            font.bold: true
            font.pixelSize: 26
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 20
        }

        Rectangle {
            height: 1
            border.width: 4
            border.color: "#ffffff"
            anchors.left: errorHeading.left
            anchors.right: errorHeading.right
            anchors.top: errorHeading.bottom
            anchors.topMargin: 2
        }

        Rectangle {
            id: descriptionBox
            width: parent.width - 60
            height: parent.height/3
            anchors.top: errorHeading.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 30
            border.width: 1
            border.color: "#ffffff"
            color: "#E53935"
        }

        Text {
            id: errorDescription
            color: "#ffffff"
            text: "If you see this text, something went wrong!"
            font.family: "Ubuntu"
            font.pixelSize: 16
            anchors.verticalCenter: descriptionBox.verticalCenter
            anchors.left: descriptionBox.left
            anchors.leftMargin: 10
            anchors.right: descriptionBox.right
            anchors.rightMargin: 10
        }

        Rectangle {
            id: errorButton
            height: 30
            width: 80
            color: errorButtonMouse.containsMouse ? "#ffffff" : "#E53935"
            border.width: 1
            border.color: "#ffffff"
            anchors.top: descriptionBox.bottom
            anchors.topMargin: 5
            anchors.right: descriptionBox.right

            Text {
                text: "OK"
                anchors.centerIn: parent
                color: errorButtonMouse.containsMouse ? "#E53935" : "#ffffff"
                font.family: "Ubuntu"
                font.pixelSize: 14
            }

            MouseArea {
                id: errorButtonMouse
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                onClicked: {
                    processController.setError(false)
                    errorMessage.visible = false
                }
            }
        }
    }
}
