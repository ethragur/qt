#include "allocationcontroller.h"

#include <iostream>
#include <random>
#include <QDebug>
#include <climits>


AllocationController::AllocationController(QObject* parent)
    : QObject(parent)
{
    m_memorySize = 64;
    m_memoryList = QVector<int>(m_memorySize);
    m_pointerPos = 0;
    fillRandomMemory();
    emit memorySizeChanged();
    emit memoryListChanged();
    std::shared_ptr<AllocationAlgo> first = std::make_shared<FirstFitAlgo>(FirstFitAlgo(m_memorySize, m_memoryList, m_input));
    std::shared_ptr<AllocationAlgo> next  = std::make_shared<NextFitAlgo>(NextFitAlgo(m_memorySize, m_memoryList, m_input));
    std::shared_ptr<AllocationAlgo> best  = std::make_shared<BestFitAlgo>(BestFitAlgo(m_memorySize, m_memoryList, m_input));
    std::shared_ptr<AllocationAlgo> worst = std::make_shared<WorstFitAlgo>(WorstFitAlgo(m_memorySize, m_memoryList, m_input));
    m_algoImpl.insert(FIRST, first);
    m_algoImpl.insert(NEXT,  next);
    m_algoImpl.insert(BEST,  best);
    m_algoImpl.insert(WORST, worst);

}

void AllocationController::setMemorySize(const int memorySize)
{
    if (memorySize <= 128) {
	m_memorySize= memorySize;
	this->m_memoryList.resize(m_memorySize);

	fillRandomMemory();
	emit memorySizeChanged();
	emit memoryListChanged();

    } else {
	this->setMemorySize(128);
    }
}

int AllocationController::memorySize() 
{
    return m_memorySize;
}

void AllocationController::setMemoryList(const QVector<int> memoryList)
{
    m_memoryList = memoryList;
    emit memoryListChanged();
}

QVector<int> AllocationController::memoryList() const
{
    return m_memoryList;
}

void AllocationController::setAllocAlgo(const AllocAlgorithm allocAlgo)
{
    m_allocAlgo = allocAlgo;
    emit allocAlgoChanged();
}

AllocAlgorithm AllocationController::allocAlgo() const
{
    return m_allocAlgo;
}

int AllocationController::pointerPos() const
{
    return m_pointerPos;
}

void AllocationController::setPointerPos(const int pointerPos)
{
    m_pointerPos = pointerPos;
    emit pointerPosChanged();
}


int AllocationController::getFreeSpace()
{
    int freeSpace = 0;
    for(const auto & n : m_memoryList)
    {
	if(n == 0)
	    freeSpace++;
    }

    return freeSpace;
}

float AllocationController::getFragmentation()
{
    int freeSpace = getFreeSpace();

    int longestBlock = 0;
    for(int i = 0; i < m_memoryList.count(); i++)
    {
	if(m_memoryList[i] == 0)
	{
	    int tmpBlock = 0;
	    for(int j = 0; j < m_memoryList.count(); j++)
	    {
		if(m_memoryList[i+j] == 0)
		{
		    tmpBlock++;
		}
		else
		{
		    i += j;
		    break;
		}

		if( tmpBlock > longestBlock)
		    longestBlock = tmpBlock;
	    }
	}
    }

    return (( freeSpace - longestBlock ) / (float)freeSpace);
}

float AllocationController::getFreeRation()
{
    int freeSpace = getFreeSpace();

    return (float)freeSpace / (float)m_memoryList.count();
}


void AllocationController::fillRandomMemory()
{
    m_memoryList = QVector<int>(m_memorySize);
    m_pointerPos = 0;

    int fromLimit =  m_memorySize  / 6;
    int toLimit   =  m_memorySize  / 4;
    std::random_device device;
    std::mt19937 generator(device());
    std::uniform_int_distribution<int> distribution(fromLimit, toLimit);
    int numberOfBlocks = distribution(generator);

    std::uniform_int_distribution<int> place_distribution(1, m_memorySize - 1);
    std::uniform_int_distribution<int> size_distribution(0, m_memorySize /  10);

    int blockID = 1;
    for(int i = 0; i < numberOfBlocks; i++)
    {
	int blockSize = size_distribution(generator);
	int blockPos = place_distribution(generator);
	bool fit = true;

	for(int j = 0; j < blockSize; j++) 
	{
	    if(m_memoryList[blockPos+j]  != 0)
	    {
		fit = false;
		//numberOfBlocks++;
		i--;
		break;
	    }
	}

	if(fit)
	{
	    for(int j = 0; j < blockSize; j++) 
	    {
		if(blockPos + j < m_memorySize)
		    m_memoryList[blockPos+j] = blockID;
	    }

	    blockID++;

	}

    }
    emit memoryListChanged();
}


bool AllocationController::evaluate()
{
    std::shared_ptr<AllocationAlgo> first = std::make_shared<FirstFitAlgo>(FirstFitAlgo(m_memorySize, m_memoryList, m_input));
    std::shared_ptr<AllocationAlgo> next  = std::make_shared<NextFitAlgo>(NextFitAlgo(m_memorySize, m_memoryList, m_input));
    std::shared_ptr<AllocationAlgo> best  = std::make_shared<BestFitAlgo>(BestFitAlgo(m_memorySize, m_memoryList, m_input));
    std::shared_ptr<AllocationAlgo> worst = std::make_shared<WorstFitAlgo>(WorstFitAlgo(m_memorySize, m_memoryList, m_input));
    m_algoImpl.insert(FIRST, first);
    m_algoImpl.insert(NEXT,  next);
    m_algoImpl.insert(BEST,  best);
    m_algoImpl.insert(WORST, worst);

    m_algoImpl.find(FIRST).value()->solve();
    m_algoImpl.find(NEXT).value()->solve();
    m_algoImpl.find(BEST).value()->solve();
    m_algoImpl.find(WORST).value()->solve();

    m_algoImpl.find(FIRST).value()->print();

    emit memoryListChanged();
    emit memorySizeChanged();

    return true;
}


QVector<int> AllocationController::getMemoryList(int algorithm)
{
    return m_algoImpl.find((AllocAlgorithm)algorithm).value()->getMemoryList();
}


bool AllocationController::insertBlock(const int blockSize, const int blockID, const int allocAlgo)
{
    m_input.push_back(QPair<int,int>(blockSize, blockID));
    switch(allocAlgo)
    {
	case FIRST: 
	    return insertBlockFirstFit(blockSize,blockID);
	    break;
	case NEXT: 
	    return insertBlockNextFit(blockSize,blockID);
	    break;
	case BEST: 
	    return insertBlockBestFit(blockSize,blockID);
	    break;
	case WORST: 
	    return insertBlockWorstFit(blockSize,blockID);
	    break;
	default: 
	    qDebug() << " not a known algorithm ";
	    return false;
    }
}

bool AllocationController::insertBlockFirstFit(const int blockSize, const int blockID)
{
    for(int i = 0; i <= m_memorySize-blockSize; i++)
    {
	if(m_memoryList[i] != 0)
	    continue;
	else
	{
	    bool fit = true;
	    for(int j = 0; j < blockSize; j++)
	    {
		if(m_memoryList[i+j] != 0)
		{
		    i = i+j;
		    fit = false;
		    break;
		}
	    }

	    if(fit)
	    {
		for(int j = 0; j < blockSize; j++)
		{
		    m_memoryList[i+j]  = blockID;
		}

		emit memoryListChanged();
		return true;
	    }
	}
    }

    return false;
}

bool AllocationController::insertBlockNextFit(const int blockSize, const int blockID)
{
    qDebug() << "Insert Block: " << blockSize;
    for(int i = m_pointerPos; (i + m_pointerPos) <= (m_memorySize + m_pointerPos - blockSize)  ; i = (i+1) % m_memorySize)
    {
	if(m_memoryList[i] != 0)
	    continue;
	else
	{
	    bool fit = true;
	    for(int j = 0; j < blockSize; j++)
	    {
		if(m_memoryList[i+j] != 0)
		{
		    i = i+j;
		    fit = false;
		    break;
		}
	    }

	    if(fit)
	    {
		m_pointerPos = i;
		for(int j = 0; j < blockSize; j++)
		{
		    m_memoryList[i+j]  = blockID;
		}

		m_pointerPos += blockSize;
		emit memoryListChanged();
		emit pointerPosChanged();
		return true;
	    }
	}
    }
    emit pointerPosChanged();


    return false;
}

bool AllocationController::insertBlockBestFit(const int blockSize, const int blockID)
{
    int min_free = INT_MAX;
    int min_free_loc = -1;
    for(int i = 0; i <= m_memorySize - blockSize; i++)
    {
	if(m_memoryList[i] != 0)
	    continue;
	else
	{
	    int j;	
	    for(j = 0; j <= m_memorySize; j++)
	    {
		if(m_memoryList[i+j] != 0)
		{
		    i = i+j;
		    break;
		}
	    }

	    if(j == blockSize)
	    {
		//perfect fit, no need to search any further
		min_free = j;
		min_free_loc = i -j;
		break;
	    }
	    else if((j >= blockSize && j < min_free))
	    {
		//we found a block that is smaller than our current min_size
		min_free = j;
		min_free_loc = i -j;
	    }
	}
    }

    if( min_free_loc != -1 )
    {
	for(int j = min_free_loc; j < min_free_loc + blockSize; j++)
	{
	    m_memoryList[j]  = blockID;
	}
	emit memoryListChanged();
	return true;
    }

    return false;
}

bool AllocationController::insertBlockWorstFit(const int blockSize, const int blockID)
{
    int max_free = blockSize;
    int max_free_loc = -1;
    for(int i = 0; i <= m_memorySize - blockSize; i++)
    {
	if(m_memoryList[i] != 0)
	    continue;
	else
	{
	    int j;
	    for(j = 0; j <= m_memorySize; j++)
	    {
		if(m_memoryList[i+j] != 0)
		{
		    i = i+j;
		    break;
		}
	    }

	    //if we don't have a block already update, otherwise only update if we found a bigger one
	    if((j >= blockSize && (j > max_free || max_free_loc == -1)))
	    {
		max_free = j;
		max_free_loc = i -j;
	    }
	}
    }

    if( max_free_loc != -1 )
    {
	for(int j = max_free_loc; j < max_free_loc + blockSize; j++)
	{
	    m_memoryList[j]  = blockID;
	}
	emit memoryListChanged();
	return true;
    }

    return false;
}
