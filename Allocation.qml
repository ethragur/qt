import "./buttons"
import "./input"
import "./output"
import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0

import Allocation 1.0

Page {
    id: allocationView

    AllocationController {
	id: allocationController
    }

    property var colorArray: ["lightgrey", "aqua", "chartreuse", "crimson", "dodgerblue", "fuchsia", "gold", "lightgreen", "orange", "pink", "teal", "peru", "navy", "lightsteelblue", "yellow", "red"]
    property int selectedColor: 1

    property int runAlgorithm: 0
    property int nextAlgorithm: 0

    property var errorActive: false
    property var errorText: ""


    signal insertedBlock()


    property var freeSpace: 0
    property var fragmentation: 0.0
    property var freeRatio: 0.0

    onInsertedBlock: {
	console.log("inserted")
	parent.freeSpace = allocationController.getFreeSpace();
	parent.fragmentation = allocationController.getFragmentation();
	parent.freeRatio = allocationController.getFreeRation();

    }

    Timer {
	id: timer
	function setTimeout(cb, delayTime) {
	    timer.interval = delayTime;
	    timer.repeat = true;
	    timer.triggered.connect(cb);
	    timer.start();
	}
    }

    ColumnLayout {
	visible: true
	width: parent.width
	height: parent.height


	Rectangle {
	    id: headerRect
	    anchors.top: parent.top
	    height: parent.height / 16
	    Layout.fillWidth: true

	    Hint {
		text: "Main menu"
		width: 80
		height: 20
		anchors.bottom: menuButton.top
		anchors.horizontalCenter: menuButton.horizontalCenter
		visible: menuMouseArea.containsMouse
	    }

	    //Menu button
	    Rectangle {
		id: menuButton
		anchors.left: parent.left
		anchors.leftMargin: 20
		anchors.top: parent.top
		anchors.topMargin: 20

		width: 40
		height: 40
		border.color: "#828282"
		border.width: 2

		Rectangle {
		    anchors.left: parent.left
		    anchors.leftMargin: 5
		    anchors.top: parent.top
		    anchors.topMargin: 5

		    width: 13
		    height: 13
		    color: "#828282"
		}

		Rectangle {
		    anchors.right: parent.right
		    anchors.rightMargin: 5
		    anchors.top: parent.top
		    anchors.topMargin: 5

		    width: 13
		    height: 13
		    color: "#828282"
		}

		Rectangle {
		    anchors.left: parent.left
		    anchors.leftMargin: 5
		    anchors.bottom: parent.bottom
		    anchors.bottomMargin: 5

		    width: 13
		    height: 13
		    color: "#828282"
		}

		Rectangle {
		    anchors.right: parent.right
		    anchors.rightMargin: 5
		    anchors.bottom: parent.bottom
		    anchors.bottomMargin: 5

		    width: 13
		    height: 13
		    color: "#828282"
		}

		MouseArea {
		    id: menuMouseArea
		    anchors.fill: parent
		    cursorShape: Qt.PointingHandCursor
		    hoverEnabled: true
		    onClicked: stackView.pop("Allocation.qml")
		}
	    }


	    //Heading
	    Text {
		id: header
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.top: parent.top
		anchors.topMargin: 20
		color: "#828282"
		font.family: "Ubuntu"
		font.pixelSize: 28
		font.bold: true
		font.underline: true
		text: qsTr("Memory Allocation")
	    }
	    //__Input__ site switcher
	    Rectangle {
		id: inputSelection

		property bool onPage: swipeView.currentIndex == 0

		height: 30
		width: 100
		anchors.bottom: header.bottom
		anchors.bottomMargin: -20
		anchors.left: parent.left
		anchors.leftMargin: parent.width/4 - 50
		opacity: onPage ? 1 : 0.3

		OwnText {
		    anchors.centerIn: parent
		    font.pixelSize: 20
		    font.bold: parent.onPage ? true : false
		    font.underline: true
		    text: qsTr("    Input    ")
		}

		MouseArea {
		    anchors.fill: parent
		    cursorShape: Qt.PointingHandCursor
		    onClicked: swipeView.setCurrentIndex(0)
		}
	    }

	    //__Output__ site switcher
	    Rectangle {
		id: outputSelection

		property bool onPage: swipeView.currentIndex == 1

		height: 30
		width: 100
		anchors.bottom: header.bottom
		anchors.bottomMargin: -20
		anchors.left: parent.left
		anchors.leftMargin: 3*(parent.width/4) - 50
		opacity: onPage ? 1 : 0.3

		OwnText {
		    anchors.centerIn: parent
		    font.pixelSize: 20
		    font.bold: parent.onPage ? true : false
		    font.underline: true
		    text:qsTr("    Output    ")
		}

		MouseArea {
		    anchors.fill: parent
		    cursorShape: Qt.PointingHandCursor
		    onClicked: {
			swipeView.setCurrentIndex(1);
		    }
		}
	    }

	    //
	}
    }

    //horizontal line between heading and body
    Rectangle {
	id: headerSeperator
	width: parent.width
	height: 1
	color: "#828282"
	anchors.top: parent.top
	anchors.topMargin: 75
    }

    SwipeView {
	id: swipeView
	currentIndex: 0
	width: parent.width
	height: parent.height - 80
	anchors.top: headerSeperator.bottom
	anchors.topMargin: 10

	Item {
	    id: inputPage
	    width: swipeView.width - 15
	    height: swipeView.height - 2

	    Rectangle {
		id: allocationMain
		anchors.topMargin: 20
		height: parent.height/12
		anchors.top: inputPage.top
		anchors.left: inputPage.left
		border.color: "#828282"
		border.width: 1
		anchors.horizontalCenter: parent.horizontalCenter

		Text {
		    id: inputLengthHeading
		    color: "#828282"
		    text: "Memory Length: "
		    font.family: "Ubuntu"
		    anchors.top: parent.top
		    font.bold: true
		    font.pixelSize: 26
		    anchors.margins: 20
		}

		SpinBox {
		    id: inputLength
		    font.family: "Ubuntu"
		    value: allocationController.memorySize
		    font.pixelSize: 28
		    from: 16
		    stepSize:  16
		    to:  128
		    anchors.left: inputLengthHeading.right
		    anchors.top: parent.top
		    anchors.margins: 20
		    onValueChanged: allocationController.memorySize = value
		}

		Text {
		    id: algorithmHeading
		    color: "#828282"
		    text: "Algorithm: "
		    font.family: "Ubuntu"
		    font.bold: true
		    font.pixelSize: 26
		    anchors.left: inputLength.right
		    anchors.top: parent.top
		    anchors.margins: 20
		    anchors.verticalCenter: parent.verticalCenter
		}

		ComboBox {
		    id: algorithm
		    width: 200
		    font.family: "Ubuntu"
		    font.bold: true
		    font.pixelSize: 26
		    model: [ "First Fit", "Next Fit", "Best Fit" , "Worst Fit"]
		    anchors.left: algorithmHeading.right
		    anchors.top: parent.top
		    anchors.margins: 20
		    onActivated: runAlgorithm = currentIndex;
		}

		Text {
		    id: freeSpaceHeading
		    color: "#828282"
		    text: "FreeSpace: "
		    font.family: "Ubuntu"
		    font.bold: true
		    font.pixelSize: 22
		    anchors.left: algorithm.right
		    anchors.top: parent.top
		    anchors.margins: 20
		    anchors.verticalCenter: parent.verticalCenter
		}
		Text {
		    id: freeSpace
		    color: "#828282"
		    text: allocationController.getFreeSpace()
		    font.family: "Ubuntu"
		    font.bold: true
		    font.pixelSize: 22
		    anchors.left: freeSpaceHeading.right
		    anchors.top: parent.top
		    anchors.margins: 20
		    anchors.verticalCenter: parent.verticalCenter
		}

		Text {
		    id: freeRationHeading
		    color: "#828282"
		    text: "Free Ratio: "
		    font.family: "Ubuntu"
		    font.bold: true
		    font.pixelSize: 22
		    anchors.left: freeSpace.right
		    anchors.top: parent.top
		    anchors.margins: 20
		    anchors.verticalCenter: parent.verticalCenter
		}
		Text {
		    id: freeSpaceRatio
		    color: "#828282"
		    text: "test"
		    font.family: "Ubuntu"
		    font.bold: true
		    font.pixelSize: 22
		    anchors.left: freeRationHeading.right
		    anchors.top: parent.top
		    anchors.margins: 20
		    anchors.verticalCenter: parent.verticalCenter
		}

		Text {
		    id: fragmentationHeading
		    color: "#828282"
		    text: "Fragmentation: "
		    font.family: "Ubuntu"
		    font.bold: true
		    font.pixelSize: 22
		    anchors.left: freeSpaceRatio.right
		    anchors.top: parent.top
		    anchors.margins: 20
		    anchors.verticalCenter: parent.verticalCenter
		}
		Text {
		    id: fragmentation
		    color: "#828282"
		    text: allocationController.getFragmentation().toFixed(2)
		    font.family: "Ubuntu"
		    font.bold: true
		    font.pixelSize: 22
		    anchors.left: fragmentationHeading.right
		    anchors.top: parent.top
		    anchors.margins: 20
		    anchors.verticalCenter: parent.verticalCenter
		}

		//calculate button
		Rectangle {
		    id: generateButton
		    border.color: "#828282"
		    border.width: 1
		    anchors.right: parent.right
		    anchors.top: parent.top
		    anchors.margins: 20
		    color: generateButtonArea.containsMouse ? "#dddddd" : "#ffffff"
		    width: 240
		    height: 64

		    signal clicked()

		    Text {
			text: qsTr("Generate Random Input")
			anchors.centerIn: parent
			color: "#525252"
			font.family: "Ubuntu"
			font.pixelSize: 16
			font.bold: true
		    }

		    MouseArea {
			id: generateButtonArea
			anchors.fill: parent
			cursorShape: Qt.PointingHandCursor
			hoverEnabled: true
			onClicked: {
			    allocationController.fillRandomMemory();
			}
		    }

		}

	    }

	    Rectangle {
		id: inputArea
		anchors.top: allocationMain.bottom
		anchors.bottom: parent.bottom
		anchors.horizontalCenter: parent.horizontalCenter
		Layout.fillHeight: true
		Layout.fillWidth: true

		border.color: "#828282"
		border.width: 1
		ColumnLayout {
		    visible: true
		    width: parent.width
		    height: parent.height
		    Layout.fillHeight: true
		    Layout.fillWidth: true

		    spacing: 50

		    Rectangle {
			id: allocInputArea
			Layout.alignment: Qt.AlignTop
			Layout.fillWidth: true

			border.color: "#828282"
			border.width: 1

			height: 400

			GridLayout {
			    id:  inputGrid
			    anchors.top: allocInputArea.top
			    columns: 32
			    anchors.horizontalCenter: parent.horizontalCenter
			    anchors.verticalCenter: parent.verticalCenter
			    rowSpacing: 10 
			    columnSpacing: 0
			    anchors.margins: 20
			    Repeater {
				id: inputRepeater
				model:  allocationController.memorySize

				Rectangle {
				    width: 58
				    height: 72
				    id: inputItem
				    property var used: allocationController.memoryList[index] > 0
				    color: inputListArea.containsMouse ? "steelblue" : colorArray[allocationController.memoryList[index] % colorArray.length];
				    MouseArea {
					id: inputListArea
					anchors.fill: parent
					cursorShape: Qt.PointingHandCursor
					acceptedButtons: Qt.LeftButton | Qt.RightButton
					hoverEnabled: true
					onClicked: {
					    if(mouse.button == Qt.LeftButton) {
						allocationController.memoryList[index] = (allocationController.memoryList[index] == 0) ? selectedColor : 0;
					    } else if( mouse.button == Qt.RightButton) {
						console.log("Right Click on " + index);
						allocationController.pointerPos = index;
					    }

					}
				    }
				    Text {
					text: index
					anchors.top: parent.top
					anchors.right: parent.right
					color: "#525252"
					font.family: "Ubuntu"
					font.pixelSize: 12 
					font.bold: true
				    }

				    Arrow {
					id: inputArrow
					anchors.top: parent.bottom
					visible: (index == allocationController.pointerPos && (nextAlgorithm == 1 || runAlgorithm == 1) ) ? true : false
				    }
				}
			    }
			}
		    }

		    Rectangle {
			id: newBlockArea
			anchors.top: allocInputArea.bottom
			anchors.margins: 20
			Layout.fillWidth: true
			border.color: "#828282"
			border.width: 1

			GridLayout {
			    id: newBlockGrid
			    anchors.horizontalCenter: parent.horizontalCenter
			    anchors.top: newBlockArea.top
			    anchors.verticalCenter: parent.verticalCenter
			    anchors.margins: 20
			    rows: 2
			    columns: 6
			    rowSpacing: 32
			    columnSpacing: 64


			    Text {
				id: blockSizeHeader
				color: "#828282"
				text: "Block Size: "
				font.family: "Ubuntu"
				font.bold: true
				font.pixelSize: 26
			    }

			    SpinBox {
				id: blockSize 
				font.family: "Ubuntu"
				font.pixelSize: 28
				value: 5
				anchors.margins: 20
			    }

			    Text {
				id: currentColorHeader
				color: "#828282"
				text: "Choose Color: "
				font.family: "Ubuntu"
				font.bold: true
				font.pixelSize: 26
			    }

			    Rectangle {
				width: 50
				height: 50
				color: colorArray[selectedColor]
				MouseArea {
				    id: clickColorArea 
				    anchors.fill: parent
				    cursorShape: Qt.PointingHandCursor
				    hoverEnabled: true
				    onClicked: {
					selectedColor = (selectedColor+1) % colorArray.length
				    }
				}
			    }

			    ColorDialog {
				id: colorDialog
				title: "Please choose a color"
				color: "red"
				showAlphaChannel: false
				onAccepted: {
				    visible: false
				}

				onRejected: {
				    visible: false
				}
				Component.onCompleted: visible = false
			    }

			    Text {
				id: nextBlockAlgorithmHeader
				color: "#828282"
				text: "Algorithm: "
				font.family: "Ubuntu"
				font.bold: true
				font.pixelSize: 26
			    }

			    ComboBox {
				id: nextBlockAlgorithm
				Layout.preferredWidth: 200
				font.family: "Ubuntu"
				font.bold: true
				font.pixelSize: 26
				model: [ "First Fit", "Next Fit", "Best Fit", "Worst Fit"]
				onActivated: nextAlgorithm = currentIndex;
			    }

			    GridLayout {
				rows: 1
				columns: 6
				Layout.columnSpan: 3
				Layout.row: 2
				Layout.column: 2
				Layout.fillWidth: true
				Repeater {
				    anchors.verticalCenter: parent.verticalCenter
				    anchors.horizontalCenter: parent.horizontalCenter
				    model: blockSize.value
				    Rectangle {
					width: 58
					height: 72
					Layout.preferredWidth: 58
					id: newBlockShow
					color: colorArray[selectedColor]
				    }
				}
			    }

			    Button {
				id: insertNextButton
				Layout.columnSpan: 6
				Layout.fillWidth: true
				text: "Insert Block"
				font.family: "Ubuntu"
				font.bold: true
				font.pixelSize: 26
				onClicked: {
				    if(!allocationController.insertBlock(blockSize.value, selectedColor, nextBlockAlgorithm.currentIndex))
				    {
					errorActive = true
					errorText   = "Block with Size: " + blockSize.value + " does not fit in Memory"
				    }
				    selectedColor = Math.floor(Math.random() * (colorArray.length - 1)) + 1;
				    blockSize.value = Math.floor(Math.random() * 7) + 1;

				    allocationView.insertedBlock()
				}
			    }


			}
		    }
		}
	    }

	    Rectangle {
		id: runButton
		border.color: "#828282"
		border.width: 1

		anchors.bottom: parent.bottom
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.margins: 20
		color: runButtonArea.containsMouse ? "#dddddd" : "#ffffff"
		width: 240
		height: 64

		signal clicked()

		Text {
		    text: qsTr("Evaluate Input")
		    anchors.centerIn: parent
		    color: "#525252"
		    font.family: "Ubuntu"
		    font.pixelSize: 16
		    font.bold: true
		}

		MouseArea {
		    id: runButtonArea
		    anchors.fill: parent
		    cursorShape: Qt.PointingHandCursor
		    hoverEnabled: true
		    onClicked: {
			allocationController.evaluate();


			//var colorUsed = 1;
			//var blockSize = 1;
			//var notFull = true;
			//enabled = false;


			//timer.setTimeout(function() {
			//    colorUsed = Math.floor(Math.random() * (colorArray.length - 1)) + 1
			//    blockSize = Math.floor(Math.random() * 7) + 1
			//    if(!allocationController.insertBlock(blockSize, colorUsed, runAlgorithm))
			//    {
			//	timer.stop();
			//	enabled = true;
			//	return;
			//    }
			//}, 1000);

			//enabled = true;
		    }
		}

	    }


	}
	Item {
	    id: outputPage

	    Rectangle {
		id: inputPostView
		width: parent.width
		height: parent.height - 20
		anchors.top: parent.top
		anchors.topMargin: 10
		anchors.left: parent.left
		anchors.right: parent.right
		anchors.leftMargin: 20
		anchors.rightMargin: 20
		border.color: "#828282"
		border.width: 1



		ScrollView {
		    anchors.top: inputPostView.top
		    anchors.bottom: inputPostView.bottom
		    anchors.left: parent.left
		    width:parent.width
		    height: parent.height

		    ColumnLayout {
			width:parent.width
			anchors.top: parent.top
			spacing: 20

			Repeater {
			    model: 4
			    Rectangle {
				id: algoRec
				border.color: "#828282"
				border.width: 10
				Layout.preferredWidth: parent.width
				Layout.preferredHeight: 400

				property var algorithmIndex:  index
				property var memoryList: allocationController.getMemoryList(algorithmIndex)
				property var randomItemforRefresh: allocationController.memoryList[0]

				function refresh()
				{
				    parent.memoryList = allocationController.getMemoryList(algorithmIndex);
				}

				Text {
				    id: algorithmCompHeader
				    color: "#828282"
				    text: "Algorithm: "
				    font.family: "Ubuntu"
				    font.bold: true
				    font.pixelSize: 24
				    anchors.topMargin: 20
				}
				GridLayout {
				    id:  inputGrid
				    anchors.top: algorithmCompHeader.top
				    columns: 32
				    anchors.verticalCenter: parent.verticalCenter
				    rowSpacing: 10 
				    columnSpacing: 0
				    anchors.margins: 20
				    Repeater {
					id: inputRepeater
					model:  allocationController.memorySize

					Rectangle {
					    width: 58
					    height: 72
					    id: inputItem
					    property var used: memoryList[index] > 0
					    color: inputListArea.containsMouse ? "steelblue" : colorArray[memoryList[index] % colorArray.length];
					    MouseArea {
						id: inputListArea
						anchors.fill: parent
						cursorShape: Qt.PointingHandCursor
						acceptedButtons: Qt.LeftButton | Qt.RightButton
						hoverEnabled: true
						onClicked: {
						}
					    }
					    Text {
						text: index
						anchors.top: parent.top
						anchors.right: parent.right
						color: "#525252"
						font.family: "Ubuntu"
						font.pixelSize: 12 
						font.bold: true
					    }
					}
				    }
				}

			    }
			}
		    }
		}
	    }
	}
    }

    Rectangle {
	id: errorMessage
	height: parent.height/4 + 20
	width: parent.width
	color: "#E53935"
	anchors.verticalCenter: parent.verticalCenter
	visible: errorActive


	Text {
	    id: errorHeading
	    color: "#ffffff"
	    text: "Error"
	    font.family: "Ubuntu"
	    font.bold: true
	    font.pixelSize: 26
	    anchors.horizontalCenter: parent.horizontalCenter
	    anchors.top: parent.top
	    anchors.topMargin: 20
	}

	Rectangle {
	    height: 1
	    border.width: 4
	    border.color: "#ffffff"
	    anchors.left: errorHeading.left
	    anchors.right: errorHeading.right
	    anchors.top: errorHeading.bottom
	    anchors.topMargin: 2
	}

	Rectangle {
	    id: descriptionBox
	    width: parent.width - 60
	    height: parent.height/3
	    anchors.top: errorHeading.bottom
	    anchors.topMargin: 10
	    anchors.left: parent.left
	    anchors.leftMargin: 30
	    border.width: 1
	    border.color: "#ffffff"
	    color: "#E53935"
	}

	Text {
	    id: errorDescription
	    color: "#ffffff"
	    text: errorText
	    font.family: "Ubuntu"
	    font.pixelSize: 16
	    anchors.verticalCenter: descriptionBox.verticalCenter
	    anchors.left: descriptionBox.left
	    anchors.leftMargin: 10
	    anchors.right: descriptionBox.right
	    anchors.rightMargin: 10
	}

	Rectangle {
	    id: errorButton
	    height: 30
	    width: 80
	    color: errorButtonMouse.containsMouse ? "#ffffff" : "#E53935"
	    border.width: 1
	    border.color: "#ffffff"
	    anchors.top: descriptionBox.bottom
	    anchors.topMargin: 5
	    anchors.right: descriptionBox.right

	    Text {
		text: "OK"
		anchors.centerIn: parent
		color: errorButtonMouse.containsMouse ? "#E53935" : "#ffffff"
		font.family: "Ubuntu"
		font.pixelSize: 14
	    }

	    MouseArea {
		id: errorButtonMouse
		anchors.fill: parent
		hoverEnabled: true
		cursorShape: Qt.PointingHandCursor
		onClicked: {
		    errorActive = false
		}
	    }
	}
    }
}
