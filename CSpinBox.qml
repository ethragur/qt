import "./buttons"
import "./input"
import "./output"
import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    id: root


    signal incButtonPressed(real xPos, real yPos)
    signal decButtonPressed(real xPos, real yPos)
    signal onTextEdited(string text)


    property string textValue: ""

    ColumnLayout {

	anchors.fill: parent;

	RowLayout {
	    Layout.alignment: Qt.AlignCenter
	    spacing: 5

	    Rectangle {
		id: procMinusButton
		Layout.preferredWidth: 25
		Layout.preferredHeight: 40
		border.width: 1
		border.color: "#bbbbbb"

		OwnText {
		    text: "-"
		    anchors.centerIn: parent
		    font.pixelSize: 20
		}

		MouseArea {
		    anchors.fill: parent
		    hoverEnabled: true
		    cursorShape: Qt.PointingHandCursor
		    onClicked: {
			root.decButtonPressed(mouse.x, mouse.y)
		    }
		}
	    }

	    TextField {
		id: procNumInput
		Layout.preferredWidth: 40
		Layout.preferredHeight: 40
		horizontalAlignment: TextInput.AlignRight
		color: "#525252"
		selectByMouse: true
		maximumLength: 2
		inputMethodHints: Qt.ImhDigitsOnly
		validator: IntValidator{bottom: 1}
		font.family: "Ubuntu"
		font.pixelSize: 16
		placeholderText: "0"
		text: root.textValue

		onTextEdited: {
		    root.onTextEdited(text)
		}
	    }

	    Rectangle {
		id: procPlusButton
		Layout.preferredWidth: 25
		Layout.preferredHeight: 40
		border.width: 1
		border.color: "#bbbbbb"

		OwnText {
		    text: "+"
		    anchors.centerIn: parent
		    font.pixelSize: 20
		}

		MouseArea {
		    anchors.fill: parent
		    hoverEnabled: true
		    cursorShape: Qt.PointingHandCursor
		    onClicked: {
			root.incButtonPressed(mouse.x, mouse.y)
		    }
		}
	    }
	}
    }

}
