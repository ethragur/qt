/*
 * Author: Robert Schaffenrath
 * Supervisor: Dr. Thomas Fahringer
 *
 * University of Innsbruck, Institute of Computer Science, http://dps.uibk.ac.at
 */


#ifndef BANKER_PROCESS_H
#define BANKER_PROCESS_H

#include <QVector>

typedef struct banker_process
{
  int id;
  QVector<int> allocation;
  QVector<int> max;
  QVector<int> need;
} banker_process_t;

#endif
