import QtQuick 2.0
import QtQuick.Window 2.3

Rectangle {
    id: toolTip
    border.color: "#828282"
    border.width: 1
    radius: 1
    color: "#626262"

    property alias text: toolTipText.text

    OwnText {
        id: toolTipText
        color: "#ffffff"
        anchors.centerIn: parent
    }
}
