#include "paginglifo.h"

#include <QDebug>



LIFOAlgorithm::LIFOAlgorithm(int pageFramesCount, const QVector<int> & input) :
			PagingAlgorithm(pageFramesCount,input)
{ }

void LIFOAlgorithm::solve()
{
	int currentStep = 1;
	for(const auto &accessedPage : m_input)
	{
			PageFrame oldPageFrame = m_pageFrames.last();
			PageFrame newPageFrame(oldPageFrame);


			int currentMin = INT32_MAX;
			int minIndex = 0;
			
			for(int i = 0; i < oldPageFrame.getSize(); i++)
			{
				if(oldPageFrame.getContent(i) == accessedPage)
				{
					minIndex = -1;
					break;
				}
				if(oldPageFrame.getLastAccessed(i) <  currentMin)
				{
					currentMin = oldPageFrame.getLastAccessed(i);
					minIndex = i;
				}
			}

			if( minIndex != -1 )
			{
				newPageFrame.set(minIndex, accessedPage, currentStep);
				m_pageFaults.push_back(QVector<int> {minIndex, oldPageFrame.getContent(minIndex), accessedPage} );
				if(oldPageFrame.getContent(minIndex) != 0)
					m_pageFaultsCount++;
			}
			else
			{
				m_pageFaults.push_back(QVector<int>(3,0) );
			}

			currentStep++;
			m_pageFrames.push_back(newPageFrame);
	}
}

LRUAlgorithm::LRUAlgorithm(int pageFramesCount, const QVector<int> & input) :
			PagingAlgorithm(pageFramesCount,input)
{ }

void LRUAlgorithm::solve()
{
	int currentStep = 1;
	for(const auto &accessedPage : m_input)
	{
			PageFrame oldPageFrame = m_pageFrames.last();
			PageFrame newPageFrame(oldPageFrame);


			int currentMin = INT32_MAX;
			int minIndex = 0;
			
			for(int i = 0; i < oldPageFrame.getSize(); i++)
			{
				if(oldPageFrame.getContent(i) == accessedPage)
				{
					//Here is the LRU change (we update the page access)
					newPageFrame.set(i, accessedPage, currentStep);
					minIndex = -1;
					break;
				}
				if(oldPageFrame.getLastAccessed(i) <  currentMin)
				{
					currentMin = oldPageFrame.getLastAccessed(i);
					minIndex = i;
				}
			}

			if( minIndex != -1 )
			{
				newPageFrame.set(minIndex, accessedPage, currentStep);
				m_pageFaults.push_back(QVector<int> {minIndex, oldPageFrame.getContent(minIndex), accessedPage} );
				if(oldPageFrame.getContent(minIndex) != 0)
					m_pageFaultsCount++;
			}
			else
			{
				m_pageFaults.push_back(QVector<int>(3,0) );
			}

			currentStep++;
			m_pageFrames.push_back(newPageFrame);
	}
}

OptimalAlgorithm::OptimalAlgorithm(int pageFramesCount, const QVector<int> & input) :
			PagingAlgorithm(pageFramesCount,input)
{ }

void OptimalAlgorithm::solve()
{
	int currentStep = 1;
	for(const auto &accessedPage : m_input)
	{
			PageFrame oldPageFrame = m_pageFrames.last();
			PageFrame newPageFrame(oldPageFrame);

			int replaceEmpty = 0;
			if((replaceEmpty = oldPageFrame.contains(0)) != -1)
			{
					newPageFrame.set(replaceEmpty, accessedPage, currentStep);
					m_pageFaults.push_back(QVector<int> {replaceEmpty, 0, accessedPage} );

			}
			else if(oldPageFrame.contains(accessedPage) == -1)
			{
				int farthest = currentStep, res = -1;
				// go over all pageFrames
				for(int i = 0; i < oldPageFrame.getSize(); i++)
				{
					//go From current accessed Page to all accessedPages in the future
					int j;
					for(j = currentStep; j < m_input.size(); j++)
					{
						if(oldPageFrame.getContent(i) == m_input[j])
						{
							if(j > farthest)
							{
								farthest = j;
								res = i;
							}
							break;
						}
					}

					// if we the page will never be accessed return it instead
					if(j == m_input.size())
					{
						res = i;
						break;
					}
				}

				if(res != -1)
				{
					newPageFrame.set(res, accessedPage, currentStep);
					m_pageFaults.push_back(QVector<int> {res, oldPageFrame.getContent(res), accessedPage} );
					m_pageFaultsCount++;
				}
			}
			else
			{
				m_pageFaults.push_back(QVector<int>(3,0) );
			}

			currentStep++;
			m_pageFrames.push_back(newPageFrame);
	}
}

ClockAlgorithm::ClockAlgorithm(int pageFramesCount, const QVector<int> & input) :
			PagingAlgorithm(pageFramesCount,input)
{ 
	clock_pos = 0;
	m_clock_pos.push_back(0);
}

void ClockAlgorithm::solve() 
{
	int currentStep = 1;
	// for the clock algorithm we have to keep a pointer
	clock_pos = 0;
	for(const auto &accessedPage : m_input)
	{
		PageFrame oldPageFrame = m_pageFrames.last();
		PageFrame newPageFrame(oldPageFrame);

		int loc = oldPageFrame.contains(accessedPage);

		// pageFrame contains page, set access to 1
		if(loc != -1)
		{
			newPageFrame.set(loc, accessedPage, 1);
			m_pageFaults.push_back(QVector<int>(3,0) );
		}
		else
		{
			//search for replacement, start at clock_pos if refernced bit is 1 change it to 0, if reference bit is 0 replace it and set it to 1
			for(; ; clock_pos = (clock_pos + 1) % oldPageFrame.getSize())
			{
				// we now have to use the new pageFrame because we change all the refernced bits (so it don't get stuck in an endless loop
				if(newPageFrame.getLastAccessed(clock_pos) == 0)
				{
					//replace page
					newPageFrame.set(clock_pos, accessedPage, 1);
					m_pageFaults.push_back(QVector<int> {clock_pos, oldPageFrame.getContent(clock_pos), accessedPage} );
					if(oldPageFrame.getContent(clock_pos) != 0)
					{
						m_pageFaultsCount++;
					}
					clock_pos = (clock_pos + 1) % newPageFrame.getSize();
					break;
				}
				else
				{
					//update referenced
					newPageFrame.set(clock_pos, newPageFrame.getContent(clock_pos), 0);
				}
			}
		}

		currentStep++;
		m_pageFrames.push_back(newPageFrame);
		m_clock_pos.push_back(clock_pos);
	}
}

void ClockAlgorithm::reset(int pageFramesCount, const QVector<int> & input)
{
	// don't forget to reset the clock_pos
	clock_pos = 0;
	m_clock_pos.clear();
	m_clock_pos.push_back(0);
	PagingAlgorithm::reset(pageFramesCount, input);
}

int ClockAlgorithm::getPointerPos(int step) const 
{
	return m_clock_pos[step];
}
