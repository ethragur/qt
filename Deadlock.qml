import "./output"
import "./input"

import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import Process 1.0

Page {
    id: bankerView

    Hint {
        text: "Main menu"
        width: 80
        height: 20
        anchors.bottom: menuButton.top
        anchors.horizontalCenter: menuButton.horizontalCenter
        visible: menuMouseArea.containsMouse
    }

    //Menu button
    Rectangle {
        id: menuButton
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 20

        width: 40
        height: 40
        border.color: "#828282"
        border.width: 2

        Rectangle {
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 5

            width: 13
            height: 13
            color: "#828282"
        }

        Rectangle {
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 5

            width: 13
            height: 13
            color: "#828282"
        }

        Rectangle {
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5

            width: 13
            height: 13
            color: "#828282"
        }

        Rectangle {
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5

            width: 13
            height: 13
            color: "#828282"
        }

        MouseArea {
            id: menuMouseArea
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            hoverEnabled: true
            onClicked: stackView.pop("Deadlock.qml")
        }
    }


    //Heading
    OwnText {
        id: header
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20
        font.pixelSize: 28
        font.bold: true
        font.underline: true
        text: qsTr("Deadlock Avoidance")
    }

    //__Input__ site switcher
    Rectangle {
        id: inputSelection

        property bool onPage: swipeView.currentIndex == 0

        height: 30
        width: 100
        anchors.bottom: header.bottom
        anchors.bottomMargin: -20
        anchors.left: parent.left
        anchors.leftMargin: parent.width/4 - 50
        opacity: onPage ? 1 : 0.3

        OwnText {
            anchors.centerIn: parent
            font.pixelSize: 20
            font.bold: parent.onPage ? true : false
            font.underline: true
            text: qsTr("    Input    ")
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: swipeView.setCurrentIndex(0)
        }
    }

    //__Output__ site switcher
    Rectangle {
        id: outputSelection

        property bool onPage: swipeView.currentIndex == 1

        height: 30
        width: 100
        anchors.bottom: header.bottom
        anchors.bottomMargin: -20
        anchors.left: parent.left
        anchors.leftMargin: 3*(parent.width/4) - 50
        opacity: onPage ? 1 : 0.3

        OwnText {
            anchors.centerIn: parent
            font.pixelSize: 20
            font.bold: parent.onPage ? true : false
            font.underline: true
            text:qsTr("    Output    ")
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: swipeView.setCurrentIndex(1)
        }
    }

    //horizontal line between heading and body
    Rectangle {
        id: headerSeperator
        width: parent.width
        height: 1
        color: "#828282"
        anchors.top: parent.top
        anchors.topMargin: 75
    }

    SwipeView {
        id: swipeView
        currentIndex: 0
        width: parent.width
        height: parent.height - 80
        anchors.top: headerSeperator.bottom
        anchors.topMargin: 10

        //input view
        Item {
            id: inputPage
            width: swipeView.width - 15
            height: swipeView.height - 2

            Rectangle {
                id: resourceInputBorder
                border.color: "#bbbbbb"
                border.width: 0.5
                width: parent.width - 30
                height: 130
                anchors.horizontalCenter: parent.horizontalCenter

                OwnText {
                    id: procNumHeading
                    text: qsTr("Processes:")
                    color: "#525252"
                    font.pixelSize: 18
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.topMargin: 30
                    anchors.leftMargin: 30
                }

                Rectangle {
                    id: procMinusButton
                    width: 25
                    height: 40
                    anchors.right: procNumInput.left
                    anchors.rightMargin: 2
                    anchors.top: procNumInput.top
                    border.width: 1
                    border.color: "#bbbbbb"

                    OwnText {
                        text: "-"
                        anchors.centerIn: parent
                        font.pixelSize: 20
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        cursorShape: Qt.PointingHandCursor
                        onClicked: {
                            controller.popProc()
                            procNumInput.text = controller.getNumOfProcs()
                            resourceView.model = 0
                            resourceView.model = controller.getNumOfResources()
                        }
                    }
                }

                Connections {
                    target: controller
                    onDataChanged: procNumInput.text = controller.getNumOfProcs()
                }

                TextField {
                    id: procNumInput
                    width: 40
                    Layout.preferredHeight: 30
                    horizontalAlignment: TextInput.AlignRight
                    color: "#525252"
                    selectByMouse: true
                    maximumLength: 2
                    inputMethodHints: Qt.ImhDigitsOnly
                    validator: IntValidator{bottom: 0}
                    font.family: "Ubuntu"
                    font.pixelSize: 16
                    anchors.left: procNumHeading.left
                    anchors.leftMargin: 22
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 30
                    placeholderText: "0"
                    text: controller.getNumOfProcs()
                    onActiveFocusChanged: activeFocus ? selectAll() : deselect()

                    onTextEdited: {
                        controller.setNumOfProcs(text)
                        controller.setNumOfProcs(text)
                        resourceView.model = 0
                        resourceView.model = controller.getNumOfResources()
                    }
                }

                Rectangle {
                    id: procPlusButton
                    width: 25
                    height: 40
                    anchors.left: procNumInput.right
                    anchors.leftMargin: 2
                    anchors.top: procNumInput.top
                    border.width: 1
                    border.color: "#bbbbbb"

                    OwnText {
                        text: "+"
                        anchors.centerIn: parent
                        font.pixelSize: 20
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        cursorShape: Qt.PointingHandCursor
                        onClicked: {
                            controller.appendProc()
                            procNumInput.text = controller.getNumOfProcs()
                            resourceView.model = 0
                            resourceView.model = controller.getNumOfResources()
                        }
                    }
                }

                Item {
                    id: resourceInputItem
                    width: parent.width - 150
                    height: parent.height
                    anchors.left: procNumHeading.right
                    anchors.leftMargin: 25

                    OwnText {
                        id: resourceInputHeading
                        text: qsTr("Resources:")
                        color: "#525252"
                        font.pixelSize: 18
                        anchors.top: parent.top
                        //                        anchors.left: parent.left
                        anchors.topMargin: 5
                        //                        anchors.leftMargin: 20
                        anchors.horizontalCenter: parent.horizontalCenter
                    }

                    OwnText {
                        id: resourceInputType
                        text: qsTr("Type")
                        color: "#525252"
                        font.pixelSize: 15
                        anchors.bottom: resourceCenterLine.bottom
                        anchors.left: parent.left
                        anchors.bottomMargin: 10
                        anchors.leftMargin: 25
                        anchors.horizontalCenter: parent.horizontalCenter
                    }

                    Rectangle {
                        id: resourceCenterLine
                        height: 1
                        width: parent.width - 30
                        anchors.right: resourceAddButton.right
                        anchors.verticalCenter: parent.verticalCenter
                        color: "#828282"
                    }

                    OwnText {
                        id: resourceInputAmount
                        text: qsTr("Total Available \n   Resources")
                        color: "#525252"
                        font.pixelSize: 12
                        anchors.top: resourceCenterLine.top
                        anchors.left: parent.left
                        anchors.topMargin: 0
                        anchors.leftMargin: 20
                        anchors.horizontalCenter: parent.horizontalCenter
                    }

                    OwnText {
                        id: totalAllocAmount
                        text: qsTr("   Allocated \n  Resources")
                        color: "#525252"
                        font.pixelSize: 12
                        anchors.top: resourceCenterLine.top
                        anchors.left: parent.left
                        anchors.topMargin: 37
                        anchors.leftMargin: 20
                        anchors.horizontalCenter: parent.horizontalCenter
                    }

                    ListView {
                        id: resourceView
                        anchors.bottom:  parent.bottom
                        anchors.bottomMargin: -5
                        width: parent.width - 260
                        height: 105
                        anchors.left: parent.left
                        anchors.leftMargin: 130
                        clip: true

                        orientation: Qt.Horizontal

                        Connections {
                            target: controller
                            onDataChanged: {
                                resourceView.model = 0
                                resourceView.model = controller.getNumOfResources()
                                resourceScrollBar.policy = controller.getNumOfResources() * 60 < resourceView.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            }
                        }

                        Connections {
                            target: inputPage
                            onWidthChanged: resourceScrollBar.policy = controller.getNumOfResources() * 60 < resourceView.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                        }

                        model: controller.getNumOfResources()
                        spacing: 10
                        delegate:
                            Item {
                            id: resourceItem
                            property int resourceId: index
                            width: 50
                            height: parent.height

                            OwnText {
                                text: "R" +index
                                font.pixelSize: 15
                                color: controller.getResourceColor(resourceItem.resourceId)
                                anchors.top: parent.top
                                anchors.topMargin: 5
                                anchors.horizontalCenter: parent.horizontalCenter

                            }

                            TextField {
                                id: resourceInputField
                                width: 40
                                height: 30
                                //Layout.preferredHeight: 10
                                horizontalAlignment: TextInput.AlignRight
                                color: "#525252"
                                selectByMouse: true
                                maximumLength: 3
                                inputMethodHints: Qt.ImhDigitsOnly
                                validator: IntValidator{bottom: 0}
                                font.family: "Ubuntu"
                                font.pixelSize: 16
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.bottom: parent.bottom
                                anchors.bottomMargin: 35
                                placeholderText: "0"
                                text: controller.getAllocationTotalAt(resourceItem.resourceId)

                                onActiveFocusChanged: {
                                    resourceInputField.text = controller.getAllocationTotalAt(resourceItem.resourceId)
                                    invalidInputHint.visible = false
                                    activeFocus ? selectAll() : deselect()
                                }

                                onTextEdited: {
                                    if (controller.isNewAllocationTotalValid(resourceItem.resourceId, text)) {
                                        invalidInputHint.visible = false
                                        controller.setAllocationTotalAt(resourceItem.resourceId, text)
                                        availableResources.model = 0
                                        availableResources.model = controller.getNumOfResources()
                                    }
                                    else {
                                        invalidInputHint.visible = true
                                    }
                                }
                            }

                            OwnText {
                                text: controller.getAllocationSumAt(resourceItem.resourceId)
                                anchors.top: resourceInputField.bottom
                                anchors.topMargin: 10
                                anchors.right: resourceInputField.right
                                anchors.rightMargin: 5
                                //anchors.horizontalCenter: parent.horizontalCenter
                            }
                        }

                        Hint {
                            id: invalidInputHint
                            width: 380
                            height: 20
                            anchors.top: parent.top
                            anchors.topMargin: 30
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: "Invalid value, amount has to be greater than sum of allocation"
                            visible: false
                        }

                        ScrollBar.horizontal: ScrollBar {
                            id: resourceScrollBar
                            policy: controller.getNumOfResources() * 60 < parent.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                        }
                    }


                    Rectangle {
                        id: resourceAddButton
                        width: 100
                        height: 30
                        border.color: "#828282"
                        border.width: 1
                        anchors.right: parent.right
                        anchors.rightMargin: 10
                        anchors.bottom: resourceCenterLine.bottom
                        anchors.bottomMargin: 10

                        OwnText {
                            text: qsTr("+ Type")
                            anchors.centerIn: parent
                        }

                        MouseArea {
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            hoverEnabled: true
                            onClicked: controller.addResource()

                        }

                    }

                    Rectangle {
                        id: resourceRemoveButton
                        width: 100
                        height: 30
                        border.color: "#828282"
                        border.width: 1
                        anchors.right: parent.right
                        anchors.rightMargin: 10
                        anchors.top: resourceCenterLine.top
                        anchors.topMargin: 10

                        OwnText {
                            text: qsTr("- Type")
                            anchors.centerIn: parent
                        }

                        MouseArea {
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            hoverEnabled: true
                            onClicked: controller.getNumOfResources() > 1 ? controller.removeResource() : null
                        }

                    }
                }//resourceInputItem
            }//resourceBorder


            Item  {
                id: inputArea
                width: parent.width
                height: parent.height - 250
                anchors.top: resourceInputBorder.bottom
                anchors.topMargin: 20
                anchors.left: parent.left
                anchors.horizontalCenter: parent.horizontalCenter

                function getAttributeWidth()
                {
                    var numOfResources = controller.getNumOfResources()
                    if (numOfResources < 4)
                        return 3*50
                    else if (numOfResources * 50 < inputArea.width/4)
                        return numOfResources * 50
                    else
                        return inputArea.width/4
                }

                OwnText {
                    id: idHeading
                    text: qsTr("ID")
                    font.pixelSize: 20
                    anchors.top: parent.top
                    anchors.topMargin: 20
                    anchors.right: idBorder.left
                    anchors.rightMargin: 15

                }

                Rectangle {
                    id: idBorder
                    height: 60
                    width: 2
                    anchors.right: allocationArea.left
                    color: "#828282"
                }

                Item {
                    id: allocationArea
                    width: inputArea.getAttributeWidth()
                    height: 60
                    anchors.right: allocationBorder.left
                    anchors.top: parent.top

                    OwnText {
                        id: allocationHeading
                        text: qsTr("Allocation")
                        font.pixelSize: 20
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                    }

                    ListView {
                        id: allocationResources
                        anchors.left: parent.left
                        anchors.leftMargin: 15
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 0
                        orientation: Qt.Horizontal
                        width: parent.width - 15
                        height: 30
                        clip: true

                        Connections {
                            target: controller
                            onDataChanged:  {
                                allocationResources.model = controller.getNumOfResources()
                                allocationArea.width = inputArea.getAttributeWidth()
                                sba1.policy = controller.getNumOfResources() * 50 < inputArea.width/4 +20 ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            }
                        }

                        Connections {
                            target: inputPage
                            onWidthChanged: {
                                allocationArea.width = inputArea.getAttributeWidth()
                                sba1.policy = controller.getNumOfResources() * 50 < inputArea.width/4 +20? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            }
                        }

                        model: controller.getNumOfResources()
                        spacing: 0
                        delegate:
                            Rectangle{
                            width: 45
                            height: 15
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 10

                            OwnText {
                                text: "R" + index
                                color: controller.getResourceColor(index)
                                font.pixelSize: 15
                                anchors.left: parent.left
                                anchors.leftMargin: controller.getNumOfResources() > 0 && controller.getNumOfResources() < 3 ? 50/controller.getNumOfResources() : 3
                            }
                        }
                        ScrollBar.horizontal: ScrollBar {
                            id: sba1
                            policy: controller.getNumOfResources() * 50 < inputArea.width/4 +20 ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            anchors.bottom: parent.bottom
                        }
                    }
                }

                Rectangle {
                    id: allocationBorder
                    height: 60
                    width: 2
                    anchors.left: parent.left
                    anchors.leftMargin: resourceInputBorder.width/2 - maxArea.width/2
                    color: "#828282"
                }

                Item {
                    id: maxArea
                    width: inputArea.getAttributeWidth()
                    height: 60
                    anchors.left: allocationBorder.right

                    OwnText {
                        id: maxHeading
                        text: qsTr("Max")
                        font.pixelSize: 20
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                    }


                    ListView {
                        id: maxResources
                        anchors.left: parent.left
                        anchors.leftMargin: 15
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 0
                        orientation: Qt.Horizontal
                        width: parent.width - 15
                        height: 30
                        clip: true

                        Connections {
                            target: controller
                            onDataChanged:  {
                                maxResources.model = controller.getNumOfResources()
                                maxArea.width = inputArea.getAttributeWidth()//controller.getNumOfResources() > 3 ? controller.getNumOfResources() * 50 : 3 * 50
                                sbnm1.policy = controller.getNumOfResources() * 50 < inputArea.width/4+20 ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            }
                        }

                        Connections {
                            target: inputPage
                            onWidthChanged: {
                                maxArea.width = inputArea.getAttributeWidth()
                                sbnm1.policy = controller.getNumOfResources() * 50 < inputArea.width/4+20 ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            }
                        }

                        model: controller.getNumOfResources()
                        spacing: 0
                        delegate:
                            Rectangle{
                            width: 45
                            height: 15
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 10
                            OwnText {
                                text: "R" + index
                                color: controller.getResourceColor(index)
                                font.pixelSize: 15
                                anchors.left: parent.left
                                anchors.leftMargin: controller.getNumOfResources() > 0 && controller.getNumOfResources() < 3 ? 50/controller.getNumOfResources() : 3
                            }
                        }
                        ScrollBar.horizontal: ScrollBar {
                            id: sbnm1
                            policy: controller.getNumOfResources() * 50 < inputArea.width/4+20 ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            anchors.bottom: parent.bottom
                        }

                    }
                }//maxArea

                Rectangle {
                    id: maxBorder
                    height: 60
                    width: 2
                    anchors.left: maxArea.right
                    color: "#828282"
                }

                Item {
                    id: needArea
                    width: inputArea.getAttributeWidth()
                    height: 60
                    anchors.left: maxBorder.right
                    //anchors.leftMargin: inputArea.getAttributeWidth()
                    anchors.top: parent.top

                    OwnText {
                        id: needHeading
                        text: qsTr("Need")
                        font.pixelSize: 20
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                    }

                    ListView {
                        id: needResources
                        anchors.left: parent.left
                        anchors.leftMargin: 15
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 0
                        orientation: Qt.Horizontal
                        width: parent.width - 15
                        height: 30
                        clip: true

                        Connections {
                            target: controller
                            onDataChanged:  {
                                needResources.model = controller.getNumOfResources()
                                needArea.width = inputArea.getAttributeWidth()
                                needScroll.policy = controller.getNumOfResources() * 50 < inputArea.width/4+20 ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            }
                        }

                        Connections {
                            target: inputPage
                            onWidthChanged: {
                                needArea.width = inputArea.getAttributeWidth()
                                needScroll.policy = controller.getNumOfResources() * 50 < inputArea.width/4+20 ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            }
                        }

                        model: controller.getNumOfResources()
                        spacing: 0
                        delegate:
                            Rectangle{
                            width: 45
                            height: 15
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 10

                            OwnText {
                                text: "R" + index
                                color: controller.getResourceColor(index)
                                font.pixelSize: 15
                                anchors.left: parent.left
                                anchors.leftMargin: controller.getNumOfResources() > 0 && controller.getNumOfResources() < 3 ? 50/controller.getNumOfResources() : 3
                            }
                        }
                        ScrollBar.horizontal: ScrollBar {
                            id: needScroll
                            policy: controller.getNumOfResources() * 50 < inputArea.width/4+20 ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            anchors.bottom: parent.bottom
                        }
                    }
                }



                Rectangle {
                    id: seperationLine
                    height: 2

                    function getLineWidth()
                    {
                        var numOfResources = controller.getNumOfResources()
                        if(numOfResources < 3)
                            return  6 * 70 + 100
                        else if (2 * numOfResources * 70 < 2*(inputArea.width/3))
                            return 2 * numOfResources * 70 + 100
                        else
                            return 2 * (inputArea.width/3) + 150
                    }

                    width: getLineWidth() //controller.getNumOfResources() < 3 ? 6 * 50 + 100 :  2 * controller.getNumOfResources() * 50 + 100
                    color: "#828282"
                    anchors.top: allocationBorder.bottom
                    anchors.left: idHeading.left
                    anchors.leftMargin: -20

                    Connections {
                        target: controller
                        onDataChanged: seperationLine.width = seperationLine.getLineWidth()//controller.getNumOfResources() > 0 && controller.getNumOfResources() < 3 ? 6 * 50 + 100 :  2 * controller.getNumOfResources() * 50 + 100
                    }
                }

                Connections {
                    target: inputPage
                    onWidthChanged: {
                        seperationLine.width = seperationLine.getLineWidth()
                    }
                }


                ListView {
                    id: procView
                    height: parent.height - 100
                    width: seperationLine.width
                    anchors.top: seperationLine.bottom
                    anchors.left: seperationLine.left
                    clip: true

                    Connections {
                        target: controller
                        onDataChanged: {
                            //procView.model = 0
                            procView.model = controller.getNumOfProcs()+1
                            procView.width = seperationLine.getLineWidth()
                            procScrollBar.policy = controller.getNumOfProcs() * 50 > procView.height ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                        }
                    }

                    Connections {
                        target: inputPage
                        onWidthChanged: {
                            procView.width = seperationLine.getLineWidth()
                            procScrollBar.policy = controller.getNumOfProcs() * 50 > procView.height ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                        }
                    }

                    model: controller.getNumOfProcs()
                    spacing: 50
                    delegate: Rectangle {
                        id: procLine
                        property int procIndex: index
                        visible: index !== controller.getNumOfProcs()
                        anchors.left: parent.left

                        Rectangle {
                            width: 56
                            height: 50
                            anchors.left: procLine.left

                            OwnText {
                                id: procId
                                text: "P" + procLine.procIndex
                                font.pixelSize: 20
                                anchors.centerIn: parent
                            }
                        }

                        Rectangle {
                            id: procIdLine
                            height: 50
                            width: 2
                            color: "#828282"
                            anchors.left: procLine.left
                            anchors.leftMargin: 56
                        }

                        ListView {
                            id: procAllocation
                            width: inputArea.getAttributeWidth() - anchors.leftMargin//controller.getNumOfResources() > 3 ? controller.getNumOfResources() * 50 - anchors.leftMargin : 3 * 50 - anchors.leftMargin
                            height: 50
                            anchors.left: procIdLine.right
                            anchors.leftMargin: controller.getNumOfResources() === 2 ? 25 : 2
                            anchors.top: procAllocationBorder.top
                            anchors.topMargin: 5
                            clip: true

                            orientation: Qt.Horizontal

                            Connections {
                                target: controller
                                onDataChanged: {
                                    procAllocation.anchors.leftMargin = controller.getNumOfResources() === 2 ? 25 : 2
                                    procAllocation.width = inputArea.getAttributeWidth() - procAllocation.anchors.leftMargin
                                    procAllocation.model = 0
                                    procAllocation.model = controller.getNumOfResources()
                                }
                            }

                            Connections {
                                target: inputPage
                                onWidthChanged: procAllocation.width = inputArea.getAttributeWidth() - procAllocation.anchors.leftMargin
                            }

                            model: controller.getNumOfResources()
                            spacing: 0
                            delegate:
                                Rectangle {
                                id: allocationWindow
                                width: 45
                                height: 50
                                property int resourceIndex: index

                                TextField {
                                    width: 35
                                    height: 40
                                    horizontalAlignment: TextInput.AlignRight
                                    color: controller.getResourceColor(allocationWindow.resourceIndex)
                                    selectByMouse: true
                                    maximumLength: 2
                                    inputMethodHints: Qt.ImhDigitsOnly
                                    validator: IntValidator{bottom: 0}
                                    font.family: "Ubuntu"
                                    font.pixelSize: 16
                                    anchors.left: parent.left
                                    anchors.leftMargin: controller.getNumOfResources() === 1 ? 56 : 8
                                    placeholderText: "0"
                                    text: controller.getAllocationAt(procLine.procIndex, allocationWindow.resourceIndex)
                                    onActiveFocusChanged: activeFocus ? selectAll() : deselect()

                                    onTextEdited: {
                                        controller.setAllocationAt(procLine.procIndex, allocationWindow.resourceIndex, text)
                                        resourceView.model = 0
                                        resourceView.model = controller.getNumOfResources()
                                        procMax.model = 0
                                        procMax.model = controller.getNumOfResources()
                                    }
                                }
                            }

                            ScrollBar.horizontal: ScrollBar {
                                id: sba2
                                policy: ScrollBar.AlwaysOff
                                position: sba1.position

                                Connections {
                                    target: sba1
                                    onPositionChanged: sba2.position = sba1.position
                                }
                            }
                        }

                        Rectangle {
                            id: procAllocationBorder
                            width: 2
                            height: 50
                            color: "#828282"
                            anchors.left: procAllocation.right
                            anchors.top: parent.top
                        }

                        ListView {
                            id: procMax
                            width: inputArea.getAttributeWidth() - anchors.leftMargin
                            height: 50
                            anchors.left: procAllocationBorder.right
                            anchors.leftMargin: controller.getNumOfResources() === 2 ? 25 : 2
                            anchors.top: procAllocationBorder.top
                            anchors.topMargin: 5
                            clip: true

                            orientation: Qt.Horizontal

                            Connections {
                                target: controller
                                onDataChanged: {
                                    procMax.anchors.leftMargin = controller.getNumOfResources() === 2 ? 25 : 2
                                    procMax.width = inputArea.getAttributeWidth() - procMax.anchors.leftMargin
                                    procMax.model = 0
                                    procMax.model = controller.getNumOfResources()
                                }
                            }

                            Connections {
                                target: inputPage
                                onWidthChanged: procMax.width = inputArea.getAttributeWidth() - procMax.anchors.leftMargin
                            }

                            model: controller.getNumOfResources()
                            spacing: 5
                            delegate:
                                Rectangle {
                                id: maxWindow
                                width: 40
                                height: 50
                                property int resourceIndex: index

                                TextField {
                                    id: maxInputField
                                    width: 35
                                    height: 40
                                    horizontalAlignment: TextInput.AlignRight
                                    color: controller.getResourceColor(maxWindow.resourceIndex)
                                    selectByMouse: true
                                    maximumLength: 2
                                    inputMethodHints: Qt.ImhDigitsOnly
                                    validator: IntValidator{bottom: 0}
                                    font.family: "Ubuntu"
                                    font.pixelSize: 16
                                    anchors.left: parent.left
                                    anchors.leftMargin: controller.getNumOfResources() === 1 ? 56 : 8
                                    placeholderText: "0"
                                    text:  controller.getMaxAt(procLine.procIndex, maxWindow.resourceIndex)
                                    onActiveFocusChanged: {
                                        maxInputField.text = controller.getMaxAt(procLine.procIndex, maxWindow.resourceIndex)
                                        invalidMaxHint.visible = false
                                        activeFocus ? selectAll() : deselect()
                                    }

                                    onTextEdited: {
                                        if (controller.isMaxValid(procLine.procIndex, maxWindow.resourceIndex, text)) {
                                        controller.setMaxAt(procLine.procIndex, maxWindow.resourceIndex, text)
                                            invalidMaxHint.visible = false
                                        procNeed.model = 0
                                        procNeed.model = controller.getNumOfResources()
                                        }
                                        else {
                                            invalidMaxHint.visible = true
                                        }

                                    }
                                }

                            }

                            Hint {
                                id: invalidMaxHint
                                width: 100
                                height: 15
                                anchors.top: parent.top
                                anchors.topMargin: 30
                                anchors.horizontalCenter: parent.horizontalCenter
                                text: "Max > Allocation"
                                visible: false
                            }

                            ScrollBar.horizontal: ScrollBar {
                                id: sbnm2
                                policy: ScrollBar.AlwaysOff
                                position: sbnm1.position

                                Connections {
                                    target: sbnm1
                                    onPositionChanged: sbnm2.position = sbnm1.position
                                }
                            }
                        }

                        Rectangle {
                            id: procMaxBorder
                            width: 2
                            height: 50
                            color: "#828282"
                            anchors.left: procMax.right
                            anchors.top: parent.top
                        }

                        ListView {
                            id: procNeed
                            width: inputArea.getAttributeWidth() - anchors.leftMargin
                            height: 50
                            anchors.left: procMaxBorder.right
                            anchors.leftMargin: controller.getNumOfResources() === 2 ? 25 : 2
                            anchors.top: procMaxBorder.top
                            anchors.topMargin: 5
                            clip: true

                            orientation: Qt.Horizontal

                            Connections {
                                target: controller
                                onDataChanged: {
                                    procNeed.anchors.leftMargin = controller.getNumOfResources() === 2 ? 25 : 2
                                    procNeed.width = inputArea.getAttributeWidth() - procNeed.leftMargin
                                    procNeed.model = 0
                                    procNeed.model = controller.getNumOfResources()
                                }
                            }

                            Connections {
                                target: inputPage
                                onWidthChanged: procNeed.width = inputArea.getAttributeWidth() - procNeed.leftMargin
                            }

                            model: controller.getNumOfResources()
                            spacing: 5
                            delegate:
                                Rectangle {
                                id: needWindow
                                width: 40
                                height: 50
                                property int resourceIndex: index
                                TextField {
                                    width: 35
                                    height: 40
                                    horizontalAlignment: TextInput.AlignRight
                                    color: controller.getResourceColor(needWindow.resourceIndex)
                                    selectByMouse: true
                                    maximumLength: 2
                                    inputMethodHints: Qt.ImhDigitsOnly
                                    validator: IntValidator{bottom: 0}
                                    font.family: "Ubuntu"
                                    font.pixelSize: 16
                                    anchors.left: parent.left
                                    anchors.leftMargin: controller.getNumOfResources() === 1 ? 56 : 8
                                    placeholderText: "0"
                                    text: controller.getNeedAt(procLine.procIndex, needWindow.resourceIndex)
                                    onActiveFocusChanged: activeFocus ? selectAll() : deselect()

                                    onTextEdited: {
                                        controller.setNeedAt(procLine.procIndex, needWindow.resourceIndex, text)
                                        procMax.model = 0
                                        procMax.model = controller.getNumOfResources()

                                    }
                                }
                            }

                            ScrollBar.horizontal: ScrollBar {
                                id: sbnm3
                                policy: ScrollBar.AlwaysOff
                                position: needScroll.position

                                Connections {
                                    target: needScroll
                                    onPositionChanged: sbnm3.position = needScroll.position
                                }
                            }
                        }

                        Rectangle {
                            id: procLineSeperation
                            width: seperationLine.getLineWidth()

                            Connections {
                                target: seperationLine
                                onWidthChanged: procLineSeperation.width = seperationLine.getLineWidth()
                            }

                            height: 2
                            color: "#828282"
                            anchors.bottom: procAllocationBorder.bottom
                        }
                    }//procLine

                    ScrollBar.vertical: ScrollBar {
                        id: procScrollBar
                        anchors.right: parent.right
                        policy: controller.getNumOfProcs() * 50 > procView.height ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                    }
                }//procView

                Item {
                    id: availableArea

                    function getAvailableWidth()
                    {
                        var numOfResources = controller.getNumOfResources()
                        if (numOfResources < 3)
                            return 150;
                        else if  (numOfResources * 50 + 20 < inputArea.width/2)
                            return numOfResources * 50 + 20

                        return inputArea.width/2;
                    }

                    width: getAvailableWidth()

                    Connections {
                        target: inputArea
                        onWidthChanged: availableArea.width = availableArea.getAvailableWidth()
                    }

                    height: 100
                    anchors.top: parent.bottom
                    anchors.topMargin: -20
                    anchors.horizontalCenter: parent.horizontalCenter

                    Rectangle {
                        anchors.fill: parent
                        border.color: "#828282"
                        border.width: 2
                    }

                    OwnText {
                        id: availableHeading
                        text: qsTr("Available")
                        font.pixelSize: 20
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                    }

                    ListView {
                        id: availableResources
                        anchors.left: availableArea.left
                        anchors.leftMargin: 15
                        anchors.bottom: availableArea.bottom
                        anchors.bottomMargin: 5
                        orientation: Qt.Horizontal
                        width: availableArea.getAvailableWidth()
                        height: 65
                        clip: true


                        Connections {
                            target: controller
                            onDataChanged:  {
                                availableResources.model = 0
                                availableResources.model = controller.getNumOfResources()
                                availableResources.width = availableArea.getAvailableWidth() - 40
                                availableArea.width = availableArea.getAvailableWidth()
                                availableScrollBar.policy =  controller.getNumOfResources() * 55 > inputArea.width/2 ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                            }
                        }

                        Connections {
                            target: inputPage
                            onWidthChanged: {
                                availableResources.width = availableArea.getAvailableWidth() - 40
                                availableArea.width = availableArea.getAvailableWidth()
                                availableScrollBar.policy =  controller.getNumOfResources() * 55 > inputArea.width/2 ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                            }
                        }

                        model: controller.getNumOfResources()
                        spacing: 12
                        delegate:
                            Rectangle{
                            id: availableWindow
                            property int availableIndex: index
                            width: 35
                            height: 65
                            OwnText {
                                id: availableResourceText
                                text: "R" + index
                                color: controller.getResourceColor(index)
                                font.pixelSize: 15
                                anchors.left: parent.left
                                anchors.leftMargin: controller.getNumOfResources() < 3 ? 45/controller.getNumOfResources() : 10
                                anchors.top: parent.top
                            }

                            TextField {
                                width: 35
                                height: 40
                                anchors.bottom: parent.bottom

                                anchors.left: parent.left
                                anchors.leftMargin: controller.getNumOfResources() > 2 ? 0 : 65 - (controller.getNumOfResources() * 27)
                                horizontalAlignment: TextInput.AlignRight
                                color: controller.getResourceColor(availableWindow.availableIndex)
                                selectByMouse: true
                                maximumLength: 2
                                inputMethodHints: Qt.ImhDigitsOnly
                                validator: IntValidator{bottom: 0}
                                font.family: "Ubuntu"
                                font.pixelSize: 16
                                placeholderText: "0"
                                text: controller.getAvailableAt(availableWindow.availableIndex)
                                onActiveFocusChanged: activeFocus ? selectAll() : deselect()

                                onTextEdited: {
                                    controller.setAvailableAt(availableWindow.availableIndex, text)
                                    resourceView.model = 0
                                    resourceView.model = controller.getNumOfResources()
                                }
                            }
                        }
                        ScrollBar.horizontal: ScrollBar {
                            id: availableScrollBar
                            policy: controller.getNumOfResources() * 55 > inputArea.width/2 ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                            anchors.top: parent.top
                            anchors.topMargin: 15
                        }
                    }
                }//availableArea

            }//inputArea

            Rectangle {
                id: findButton
               width: 160
               height: 40
               border.color: "#828282"
               border.width: 1
               anchors.bottom: calcButton.top
               anchors.bottomMargin: 5
               anchors.right: parent.right
               anchors.rightMargin: 30

               OwnText {
                   anchors.verticalCenter: parent.verticalCenter
                   anchors.left: parent.left
                   anchors.leftMargin: 12
                   text: qsTr("Find Min Available")
                   color: "#525252"
                   font.pixelSize: 14
               }

               MouseArea {
                   id: calcMinAvailMouse
                   anchors.fill: parent
                   cursorShape: Qt.PointingHandCursor
                   hoverEnabled: true
                   onClicked: {
                       controller.findMinAvail()
                       availableResources.model = 0
                       availableResources.model = controller.getNumOfResources()

                   }
               }

               Rectangle {
                   id: findAvailableHelp
                   width: 14
                   height: 14
                   radius: 7
                   border.width: 1
                   border.color: "#828282"
                   anchors.right: findButton.right
                   anchors.rightMargin: 5
                   anchors.bottom: findButton.bottom
                   anchors.bottomMargin: findButton.height/2 - findAvailableHelp.height/2

                   OwnText {
                       anchors.centerIn: parent
                       text: "?"
                       font.pixelSize: 8
                   }

                   MouseArea {
                       id: findAvailableMouse
                       anchors.fill: parent
                       hoverEnabled: true
                   }

                   Hint {
                       width: 230
                       height: 20
                       anchors.bottom: findAvailableHelp.top
                       anchors.right: findAvailableHelp.right
                       text: "Find lowest needed Available values"
                       visible: findAvailableMouse.containsMouse
                   }
               }

            }

            Rectangle {
               id: calcButton
               width: 200
               height: 40
               border.color: "#828282"
               border.width: 1
               anchors.bottom: parent.bottom
               anchors.bottomMargin: 25
               anchors.right: parent.right
               anchors.rightMargin: 30

               OwnText {
                   anchors.centerIn: parent
                   text: qsTr("Calculate")
                   color: "#525252"
                   font.pixelSize: 16
               }

               MouseArea {
                   id: calcButtonMouse
                   anchors.fill: parent
                   cursorShape: Qt.PointingHandCursor
                   hoverEnabled: true
                   onClicked: {
                       stepSequences.model = 0
                       controller.calcSequences()
                       controller.setSequenceIndex(-1)
                       stepHint.visible = true
                       stepHeading.visible = false
                       stepResourceSeperator.visible = false
                       swipeView.incrementCurrentIndex()
                   }
               }
            }


            Rectangle {
               width: 160
               height: 35
               border.color: "#828282"
               border.width: 1
               anchors.bottom: randButton.top
               anchors.bottomMargin: 5
               anchors.left: parent.left
               anchors.leftMargin: 30

               OwnText {
                   anchors.centerIn: parent
                   text: qsTr("Reset Input")
                   color: "#525252"
                   font.pixelSize: 16
               }

               MouseArea {
                   id: clearButtonMouse
                   anchors.fill: parent
                   cursorShape: Qt.PointingHandCursor
                   hoverEnabled: true
                   onClicked: {
                       controller.clearInput()
                       procView.model = 0
                       procView.model = controller.getNumOfProcs()
                   }
               }
            }



            //generate random button
            Rectangle {
                id: randButton
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 25
                anchors.left: parent.left
                anchors.leftMargin: 30
                border.color: "#828282"
                border.width: 1
                color: randButtonArea.containsMouse ? "#dddddd" : "#ffffff"
                width: 200
                height: 35

                Text {
                    text: qsTr("Random Processes")
                    anchors.centerIn: parent
                    color: "#525252"
                    font.family: "Ubuntu"
                    font.pixelSize: 16
                    //font.bold: true
                }

                MouseArea {
                    id: randButtonArea
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    hoverEnabled: true
                    onClicked: {
                        randButton.focus = true
                        randInputView.visible = true
                        randInputDialog.visible = true
                    }
                }

                Rectangle {
                    id: forceInterruptHelp
                    width: 14
                    height: 14
                    radius: 7
                    border.width: 1
                    border.color: "#828282"
                    anchors.right: randButton.right
                    anchors.rightMargin: 5
                    anchors.bottom: randButton.bottom
                    anchors.bottomMargin: randButton.height/2 - forceInterruptHelp.height/2

                    OwnText {
                        anchors.centerIn: parent
                        text: "?"
                        font.pixelSize: 8
                    }

                    MouseArea {
                        id: forceInterruptMouse
                        anchors.fill: parent
                        hoverEnabled: true
                    }

                    Hint {
                        width: 300
                        height: 20
                        anchors.bottom: forceInterruptHelp.top
                        //anchors.right: forceInterruptHelp.right
                        anchors.horizontalCenter: forceInterruptHelp.horizontalCenter
                        text: "Generating input data, which leads to 1 safe sequence"
                        visible: forceInterruptMouse.containsMouse
                    }
                }
            }

            //random attribute dialog
            Dialog {
                id: randInputDialog
                visible: false
                title: "Attributes"

                modal: true

                x: (swipeView.width - width) / 2
                y: (swipeView.height - height) / 2

                //generate random processes window
                Rectangle {
                    id: randInputView
                    implicitWidth: 300
                    implicitHeight: 100
                    anchors.bottomMargin: 10
                    color: "#eeeeee"
                    border.color: "#828282"
                    border.width: 1

                    ProcessGenerateAttributes {
                        id: randProcAmount
                        anchors.left: randInputView.left
                        anchors.leftMargin: 50
                        maxValue: 2
                        headerText: qsTr("#Processes")
                        valueText: controller.getGenerateProcAmount()
                        onEditingFinished: controller.setGenerateProcAmount(valueText)
                    }

                    ProcessGenerateAttributes {
                        id: randProcMaxValue
                        anchors.right: randInputView.right
                        anchors.rightMargin: 50
                        maxValue: 2
                        headerText: qsTr("#Resources")
                        valueText: controller.getGenerateResourceAmount()
                        onEditingFinished: controller.setGenerateResourceAmount(valueText)
                    }

                    Rectangle {
                        id: randGenerateButton
                        width: randInputView.width - 40
                        height: 20
                        anchors.horizontalCenter: randInputView.horizontalCenter
                        anchors.bottom: randInputView.bottom
                        anchors.bottomMargin: 5
                        border.color: "#828282"
                        border.width: 1
                        color: randGenerateButtonMouse.containsMouse ? "#e2e2e2" : "#ffffff"

                        Text {
                            anchors.centerIn: parent
                            text: qsTr("Generate")
                            color: "#525252"
                            font.family: "Ubuntu"
                            font.pixelSize: 12
                        }

                        MouseArea {
                            id: randGenerateButtonMouse
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            hoverEnabled: true
                            onClicked: {
                                randGenerateButton.focus = true
                                if (!controller.isGenerationAttributesValid()) {
                                  controller.setErrorHeading("Invalid Values");
                                    controller.setErrorDescription(controller.getGenerateErrorMessage());
                                    randInputDialog.visible = false;
                                    controller.setError(true);
                                    errorMessage.visible = true;
                                }
                                else {
                                    controller.generateProcs()
                                    procView.model = 0
                                    procView.model = controller.getNumOfProcs()
                                }
                                    randInputDialog.visible = false
                                }
                            }
                        }
                    }
                }


            Rectangle {
                id: errorMessage
                height: parent.height/4 + 20
                width: parent.width
                color: "#E53935"
                anchors.verticalCenter: parent.verticalCenter
                visible: controller.isErrorActive()

                Connections {
                    target: controller
                    onDataChanged: {
                        errorMessage.visible = controller.isErrorActive()
                        errorHeading.text = controller.getErrorHeading()
                        errorDescription.text = controller.getErrorDescription()
                    }
                }


                Text {
                    id: errorHeading
                    color: "#ffffff"
                    text: "Error"
                    font.family: "Ubuntu"
                    font.bold: true
                    font.pixelSize: 26
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.topMargin: 20
                }

                Rectangle {
                    height: 1
                    border.width: 4
                    border.color: "#ffffff"
                    anchors.left: errorHeading.left
                    anchors.right: errorHeading.right
                    anchors.top: errorHeading.bottom
                    anchors.topMargin: 2
                }

                Rectangle {
                    id: descriptionBox
                    width: parent.width - 60
                    height: parent.height/3
                    anchors.top: errorHeading.bottom
                    anchors.topMargin: 10
                    anchors.left: parent.left
                    anchors.leftMargin: 30
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#E53935"
                }

                Text {
                    id: errorDescription
                    color: "#ffffff"
                    text: "If you see this text, something went wrong!"
                    font.family: "Ubuntu"
                    font.pixelSize: 16
                    anchors.verticalCenter: descriptionBox.verticalCenter
                    anchors.left: descriptionBox.left
                    anchors.leftMargin: 10
                    anchors.right: descriptionBox.right
                    anchors.rightMargin: 10
                }

                Rectangle {
                    id: errorButton
                    height: 30
                    width: 80
                    color: errorButtonMouse.containsMouse ? "#ffffff" : "#E53935"
                    border.width: 1
                    border.color: "#ffffff"
                    anchors.top: descriptionBox.bottom
                    anchors.topMargin: 5
                    anchors.right: descriptionBox.right

                    Text {
                        text: "OK"
                        anchors.centerIn: parent
                        color: errorButtonMouse.containsMouse ? "#E53935" : "#ffffff"
                        font.family: "Ubuntu"
                        font.pixelSize: 14
                    }

                    MouseArea {
                        id: errorButtonMouse
                        anchors.fill: parent
                        hoverEnabled: true
                        cursorShape: Qt.PointingHandCursor
                        onClicked: {
                            controller.setError(false)
                            errorMessage.visible = false
                        }
                    }
                }
            }
        }//inputPage

        Item {
            id: outputPage

            Rectangle {
                id: inputPostView
                width: parent.width/2
                height: parent.height/2 - 50
                anchors.top: parent.top
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 20
                border.color: "#828282"
                border.width: 1

                OwnText {
                    text: qsTr("ID")
                    font.pixelSize: 15
                    anchors.left: parent.left
                    anchors.leftMargin: 12
                    anchors.top: parent.top
                    anchors.topMargin: 15
                }

                Rectangle {
                    id: inputPostIdSeperator
                    height: 50
                    width: 1
                    color: "#828282"
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.leftMargin: 40
                }

                Item {
                    id: inputPostAllocation
                    width: (inputPostView.width-44)/3
                    height: 50
                    anchors.left: inputPostIdSeperator.right
                    anchors.top: parent.top

                    OwnText {
                        text: qsTr("Allocation")
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 5
                    }

                    ListView {
                        id: inputPostAllocationResources
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                        anchors.top: parent.top
                        anchors.topMargin: 30
                        orientation: Qt.Horizontal
                        width: parent.width - 15
                        height: 21
                        clip: true

                        Connections {
                            target: controller
                            onDataChanged:  {
                                inputPostAllocationResources.model = controller.getNumOfResources()
                                inputPostAllocationResources.spacing = (inputPostAllocationResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostAllocationResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                                sb1.policy = controller.getNumOfResources() * 30 < inputPostAllocationResources.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            }
                        }

                        Connections {
                            target: inputPostAllocationResources
                            onWidthChanged: {
                                inputPostAllocationResources.spacing = (inputPostAllocationResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostAllocationResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1//controller.getNumOfResources() < 4 ? 15 : 5
                                sb1.policy = controller.getNumOfResources() * 30 < inputPostAllocationResources.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn

                            }
                        }

                        model: controller.getNumOfResources()
                        spacing: (width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1//controller.getNumOfResources() < 4 ? 15 : 4
                        delegate:
                            Rectangle{
                            width: 30
                            height: 15

                            OwnText {
                                text: "R" + index
                                color: controller.getResourceColor(index)
                                font.pixelSize: 12
                                anchors.left: parent.left
                                anchors.leftMargin: controller.getNumOfResources() > 0 && controller.getNumOfResources() < 3 ? 50/controller.getNumOfResources() : 5
                            }
                        }

                        ScrollBar.horizontal: ScrollBar {
                            id: sb1
                            anchors.bottom: inputPostAllocationResources.bottom
                            policy: controller.getNumOfResources() * 30 < inputPostAllocationResources.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                        }
                    }

                }

                Rectangle {
                    id: inputPostAllocationSeperator
                    height: 50
                    width: 1
                    color: "#828282"
                    anchors.top: parent.top
                    anchors.left: inputPostAllocation.right
                }



                Item {
                    id: inputPostMax
                    width: (inputPostView.width-44)/3
                    height: 50
                    anchors.left: inputPostAllocationSeperator.right
                    anchors.top: parent.top

                    OwnText {
                        text: qsTr("Max")
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 5
                    }

                    ListView {
                        id: inputPostMaxResources
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                        anchors.top: parent.top
                        anchors.topMargin: 30
                        orientation: Qt.Horizontal
                        width: parent.width - 15
                        height: 21
                        clip: true

                        Connections {
                            target: controller
                            onDataChanged:  {
                                inputPostMaxResources.model = controller.getNumOfResources()
                                inputPostMaxResources.spacing = (inputPostMaxResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostMaxResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                                sb3.policy = controller.getNumOfResources() * 30 < inputPostMaxResources.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            }
                        }

                        Connections {
                            target: inputPostMaxResources
                            onWidthChanged: {

                                inputPostMaxResources.spacing = (inputPostMaxResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostMaxResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                                sb3.policy = controller.getNumOfResources() * 30 < inputPostMaxResources.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            }
                        }

                        model: controller.getNumOfResources()
                        spacing: (inputPostMaxResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostMaxResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                        delegate:
                            Rectangle{
                            width: 30
                            height: 15

                            OwnText {
                                text: "R" + index
                                color: controller.getResourceColor(index)
                                font.pixelSize: 12
                                anchors.left: parent.left
                                anchors.leftMargin: controller.getNumOfResources() > 0 && controller.getNumOfResources() < 3 ? 50/controller.getNumOfResources() : 5
                            }
                        }
                        ScrollBar.horizontal: ScrollBar {
                            id: sb3
                            anchors.bottom: inputPostMaxResources.bottom
                            policy: controller.getNumOfResources() * 30 < inputPostMaxResources.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                        }
                    }
                }

                Rectangle {
                    id: inputPostMaxSeperator
                    height: 50
                    width: 1
                    color: "#828282"
                    anchors.top: parent.top
                    anchors.left: inputPostMax.right
                }

                Item {
                    id: inputPostNeed
                    width: (inputPostView.width-44)/3
                    height: 50
                    anchors.left: inputPostMaxSeperator.right
                    anchors.top: parent.top

                    OwnText {
                        text: qsTr("Need")
                        font.pixelSize: 15
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 5
                    }

                    ListView {
                        id: inputPostNeedResources
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                        anchors.top: parent.top
                        anchors.topMargin: 30
                        orientation: Qt.Horizontal
                        width: parent.width - 15
                        height: 21
                        clip: true

                        Connections {
                            target: controller
                            onDataChanged:  {
                                inputPostNeedResources.model = controller.getNumOfResources()
                                inputPostNeedResources.spacing = (inputPostNeedResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostNeedResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                                sb2.policy = controller.getNumOfResources() * 30 < inputPostNeedResources.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            }
                        }

                        Connections {
                            target: inputPostNeedResources
                            onWidthChanged: {
                                inputPostNeedResources.spacing = (inputPostNeedResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostNeedResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                                sb2.policy = controller.getNumOfResources() * 30 < inputPostNeedResources.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn

                            }
                        }

                        model: controller.getNumOfResources()
                        spacing: (inputPostNeedResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostNeedResources.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                        delegate:
                            Rectangle{
                            width: 30
                            height: 15

                            OwnText {
                                text: "R" + index
                                color: controller.getResourceColor(index)
                                font.pixelSize: 12
                                anchors.left: parent.left
                                anchors.leftMargin: controller.getNumOfResources() > 0 && controller.getNumOfResources() < 3 ? 50/controller.getNumOfResources() : 5
                            }
                        }

                        ScrollBar.horizontal: ScrollBar {
                            id: sb2
                            anchors.bottom: inputPostNeedResources.bottom
                            policy: controller.getNumOfResources() * 30 < inputPostNeedResources.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                        }
                    }
                }


                Rectangle {
                    id: inputPostHeaderLine
                    height: 1
                    width: parent.width
                    color: "#828282"
                    anchors.bottom: inputPostAllocation.bottom
                    anchors.left: parent.left
                }

                ListView {
                    id: inputPostProcs
                    width: parent.width - 6
                    height: parent.height - 50
                    anchors.top: inputPostHeaderLine.bottom
                    anchors.left: inputPostHeaderLine.left
                    anchors.leftMargin: 3
                    clip: true

                    Connections {
                        target: controller
                        onDataChanged: {
                            inputPostProcs.model = controller.getNumOfProcs()
                            inputPostProcScrollbar.policy = inputPostProcs.height < controller.getNumOfProcs() * 30 ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                        }
                    }

                   model: controller.getNumOfProcs()
                   delegate: Rectangle {
                       id: inputPostLine
                       property int lineIndex: index
                       width: parent.width
                       height: 30

                       OwnText {
                           id: inputPostLineId
                           text: "P" + inputPostLine.lineIndex
                           anchors.verticalCenter: parent.verticalCenter
                           anchors.left: parent.left
                           anchors.leftMargin: 15
                       }

                       Rectangle {
                           id: inputPostProcIdSeperator
                           width: 1
                           height: parent.height
                           color: "#828282"
                           anchors.left: parent.left
                           anchors.leftMargin: 37
                       }

                       ListView {
                           id: inputPostProcAllocation
                           width: inputPostAllocation.width - 12
                           height: 30
                           anchors.left: inputPostProcIdSeperator.right
                           anchors.leftMargin: 12
                           anchors.top: inputPostProcIdSeperator.top
                           clip: true

                           orientation: Qt.Horizontal

                           Connections {
                               target: controller
                               onDataChanged: {
                                   inputPostProcAllocation.spacing = (inputPostProcAllocation.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostProcAllocation.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                                   inputPostProcAllocation.model = 0
                                   inputPostProcAllocation.model = controller.getNumOfResources()
                               }
                           }

                           Connections {
                               target: inputPostProcAllocation
                               onWidthChanged: inputPostProcAllocation.spacing = (inputPostProcAllocation.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostProcAllocation.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                           }

                           model: controller.getNumOfResources()
                           spacing: (inputPostProcAllocation.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostProcAllocation.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                           delegate:
                               Rectangle {
                               id: inputPostAllocationWindow
                               width: 30
                               height: 30
                               property int resourceIndex: index

                               OwnText {
                                   anchors.verticalCenter: parent.verticalCenter
                                   anchors.left: parent.left
                                   anchors.leftMargin: controller.getNumOfResources() > 0 && controller.getNumOfResources() < 3 ? 50/controller.getNumOfResources() : 5
                                   text: controller.getAllocationAt(inputPostLine.lineIndex, inputPostAllocationWindow.resourceIndex)
                                   color: controller.getResourceColor(inputPostAllocationWindow.resourceIndex)
                               }
                           }

                           ScrollBar.horizontal: ScrollBar {
                               id: sb11
                               position: sb1.position
                               policy: ScrollBar.AlwaysOff

                               Connections {
                                   target: sb1
                                   onPositionChanged: sb11.position = sb1.position
                               }
                           }
                       }

                       Rectangle {
                           id: inputPostProcAllocationSeperator
                           width: 1
                           height: parent.height
                           color: "#828282"
                           anchors.left: inputPostProcAllocation.right
                       }


                       ListView {
                           id: inputPostProcMax
                           width: inputPostMax.width - 12 //(inputPostView.width-44)/3
                           height: 30
                           anchors.left: inputPostProcAllocationSeperator.right
                           anchors.leftMargin: 12
                           anchors.top: inputPostProcAllocationSeperator.top
                           clip: true

                           orientation: Qt.Horizontal

                           Connections {
                               target: controller
                               onDataChanged: {
                                   inputPostProcMax.spacing = (inputPostProcMax.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostProcMax.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                                   inputPostProcMax.model = 0
                                   inputPostProcMax.model = controller.getNumOfResources()
                               }
                           }

                           Connections {
                               target: inputPostProcMax
                               onWidthChanged: inputPostProcMax.spacing = (inputPostProcMax.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostProcMax.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                           }

                           model: controller.getNumOfResources()
                           spacing: (inputPostProcMax.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostProcMax.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                           delegate:
                               Rectangle {
                               id: inputPostProcMaxWindow
                               width: 30
                               height: 30
                               property int resourceIndex: index

                               OwnText {
                                   anchors.verticalCenter: parent.verticalCenter
                                   anchors.left: parent.left
                                   anchors.leftMargin: controller.getNumOfResources() > 0 && controller.getNumOfResources() < 3 ? 50/controller.getNumOfResources() : 5
                                   text: controller.getMaxAt(inputPostLine.lineIndex, inputPostProcMaxWindow.resourceIndex)
                                   color: controller.getResourceColor(inputPostProcMaxWindow.resourceIndex)
                               }
                           }
                           ScrollBar.horizontal: ScrollBar {
                               id: sb31
                               position: sb3.position
                               policy: ScrollBar.AlwaysOff

                               Connections {
                                   target: sb3
                                   onPositionChanged: sb31.position = sb3.position
                               }
                           }
                       }

                       Rectangle {
                           id: inputPostProcMaxSeperator
                           width: 1
                           height: parent.height
                           color: "#828282"
                           anchors.left: inputPostProcMax.right
                       }


                       ListView {
                           id: inputPostProcNeed
                           width: inputPostNeed.width - 12//(inputPostView.width-44)/3
                           height: 30
                           anchors.left: inputPostProcMaxSeperator.right
                           anchors.leftMargin: 12
                           anchors.top: inputPostProcMaxSeperator.top
                           clip: true

                           orientation: Qt.Horizontal

                           Connections {
                               target: controller
                               onDataChanged: {
                                   inputPostProcNeed.spacing = (inputPostProcNeed.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostProcNeed.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                                   inputPostProcNeed.model = 0
                                   inputPostProcNeed.model = controller.getNumOfResources()
                               }
                           }

                           Connections {
                               target: inputPostProcNeed
                               onWidthChanged: inputPostProcNeed.spacing = (inputPostProcNeed.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostProcNeed.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                           }

                           model: controller.getNumOfResources()
                           spacing: (inputPostProcNeed.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() > 1 ? (inputPostProcNeed.width - controller.getNumOfResources() * 30)/controller.getNumOfResources() : 1
                           delegate:
                               Rectangle {
                               id: inputPostProcNeedWindow
                               width: 30
                               height: 30
                               property int resourceIndex: index

                               OwnText {
                                   anchors.verticalCenter: parent.verticalCenter
                                   anchors.left: parent.left
                                   anchors.leftMargin: controller.getNumOfResources() > 0 && controller.getNumOfResources() < 3 ? 50/controller.getNumOfResources() : 5
                                   text: controller.getNeedAt(inputPostLine.lineIndex, inputPostProcNeedWindow.resourceIndex)
                                   color: controller.getResourceColor(inputPostProcNeedWindow.resourceIndex)
                               }
                           }

                           ScrollBar.horizontal: ScrollBar {
                               id: sb21
                               position: sb2.position
                               policy: ScrollBar.AlwaysOff

                               Connections {
                                   target: sb2
                                   onPositionChanged: sb21.position = sb2.position
                               }
                           }
                       }//inputPostProcNeed


                       //horizontalLine
                       Rectangle {
                           width: parent.width
                           height: 1
                           color: "#828282"
                           anchors.bottom: parent.bottom
                       }
                    }

                   ScrollBar.vertical: ScrollBar {
                       id: inputPostProcScrollbar
                       policy: inputPostProcs.height < controller.getNumOfProcs() * 30 ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                   }
                }

            }//inputPostView

            Rectangle {
                id: resultList
                width: parent.width/2
                height: parent.height/2 - 50
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 50
                anchors.left: parent.left
                anchors.leftMargin: 20
                border.color: "#828282"
                border.width: 1

                OwnText {
                    id: sequenceHeading
                    text: qsTr("Safe Sequences")
                    //font.bold: true
                    font.pixelSize: 20
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    anchors.top: parent.top
                    anchors.topMargin: 20
                }

                OwnText {
                    id: sequenceCounter
                    text: qsTr("#Sequences: ") + controller.getNumOfSequences()
                    anchors.bottom: sequenceHeading.bottom
                    anchors.right: parent.right
                    anchors.rightMargin: 20
                    visible: controller.getNumOfSequences() !== 0
                }

                Rectangle {
                    id: sequenceSeperator
                    width: parent.width-40
                    height: 1
                    color: "#828282"
                    anchors.top: parent.top
                    anchors.topMargin: 50
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                }

                ListView {
                    id: resultSequences
                    width: parent.width - 6
                    height: parent.height - 60
                    anchors.left: parent.left
                    anchors.leftMargin: 2
                    anchors.top: parent.top
                    anchors.topMargin: 55
                    clip: true

                    Connections {
                        target: controller
                        onDataChanged: {
                            noResultsText.visible = controller.getNumOfSequences() === 0
                            resultSequences.model = controller.getNumOfSequences()
                            sequenceCounter.visible = controller.getNumOfSequences() !== 0
                            sequenceCounter.text = qsTr("#Sequences: ") + controller.getNumOfSequences()
                            sequenceScrollBar.policy = resultSequences.height < controller.getNumOfSequences() * 40 ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                        }
                    }

                    model: controller.getNumOfSequences()

                    delegate: Rectangle {
                        id: sequenceLine
                        property int lineIndex: index
                        height: 40
                        width: parent.width

                        OwnText {
                            id: lineId
                            text: (sequenceLine.lineIndex + 1) + "."
                            font.pixelSize: 15
                            anchors.left: parent.left
                            anchors.leftMargin: 10
                            anchors.verticalCenter: parent.verticalCenter
                        }

                        ListView {
                            id: procSequence
                            anchors.left: lineId.right
                            anchors.leftMargin: 10
                            //anchors.top: parent.top
                            anchors.verticalCenter: parent.verticalCenter
                            width: sequenceLine.width - 15
                            height: parent.height
                            clip: true

                            orientation: Qt.Horizontal

                            Connections {
                                target: controller
                                onDataChanged: {
                                    procSequence.model = 0
                                    procSequence.model = controller.getNumOfProcs()+1
                                    noResultsText.visible = controller.getNumOfSequences() === 0
                                    stepHint.visible = controller.getSequenceIndex() === -1
                                    sequenceHorScroll.policy = controller.getNumOfProcs() * 30 < procSequence.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn

                                }
                            }

                            model: controller.getNumOfProcs()+1
                            //spacing: 0
                            delegate: Rectangle {
                                id: seqProcWindow
                                property int posIndex: index
                                width: 30
                                height: 30
                                anchors.verticalCenter: parent.verticalCenter

                                OwnText {
                                    anchors.centerIn: parent
                                    font.pixelSize: 15
                                    text: controller.getNumOfProcs() === index ? "" : "P" + controller.getSeqResultAt(sequenceLine.lineIndex, seqProcWindow.posIndex)
                                }

                            }

                            ScrollBar.horizontal: ScrollBar {
                                id: sequenceHorScroll
                                policy: controller.getNumOfProcs() * 30 < procSequence.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                                anchors.bottom: parent.bottom
                            }

                        }

                        MouseArea {
                            //anchors.fill: parent
                            height: parent.height - 20
                            width: parent.width - 10
                            anchors.top: parent.top
                            anchors.topMargin: 10
                            hoverEnabled: true
                            cursorShape: Qt.PointingHandCursor
                            onClicked: {
                                controller.setSequenceIndex(sequenceLine.lineIndex)
                                controller.calcSteps()
                                stepSequences.model = 0
                                stepSequences.model = controller.getNumOfProcs()
                                stepHint.visible = controller.getSequenceIndex() === -1
                                stepHeading.visible = controller.getSequenceIndex() !== -1
                                stepResourceSeperator.visible = stepHeading.visible
                                stepNeedHeading.width = controller.getNumOfResources() * 35 < stepHeading.width/3 ? controller.getNumOfResources() * 35 : stepHeading.width/3
                                stepNeedHeading.model = controller.getNumOfResources()
                                stepAvailableHeading.width = controller.getNumOfResources() * 35 < stepHeading.width/3 ? controller.getNumOfResources() * 35 : stepHeading.width/3
                                stepAvailableHeading.model = controller.getNumOfResources()
                                stepNeedScroll.policy = controller.getNumOfResources() * 30 < (stepNeedHeading.width + 10) ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                                stepAvailableScroll.policy = controller.getNumOfResources() * 30 < (stepNeedHeading.width + 10) ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                                stepScroll.policy = controller.getNumOfProcs() * 25 > stepSequences.height ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                            }
                        }

                        Rectangle {
                            width: parent.width
                            height: 1
                            color: "#828282"
                            anchors.bottom: parent.bottom
                            anchors.left: parent.left
                            anchors.leftMargin: 2
                        }
                    }

                    ScrollBar.vertical: ScrollBar {
                        id: sequenceScrollBar
                        policy: resultSequences.height < controller.getNumOfSequences() * 40 ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                        anchors.right: parent.right
                    }

                }//resultSequences

                OwnText {
                    id: noResultsText
                    text: qsTr("No safe sequences found")
                    font.pixelSize: 20
                    font.bold: true
                    anchors.centerIn: parent
                    visible: controller.getNumOfSequences() === 0
                }
            }//resultList

            Rectangle {
                id: stepView
                width: parent.width/2 - 100
                height: parent.height -100
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 50
                border.color: "#828282"
                border.width: 1

                OwnText {
                    text: qsTr("Steps")
                    //font.bold: true
                    font.pixelSize: 20
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    anchors.top: parent.top
                    anchors.topMargin: 20
                }

                Rectangle {
                    id: stepHeadingSeperator
                    width: parent.width-40
                    height: 1
                    color: "#828282"
                    anchors.top: parent.top
                    anchors.topMargin: 50
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                }

                Rectangle {
                    id: stepHeading
                    width: parent.width - 6
                    height: 50
                    anchors.top: stepHeadingSeperator.top
                    anchors.topMargin: 10
                    anchors.left: parent.left
                    anchors.leftMargin: 3
                    visible: controller.getSequenceIndex() !== -1

                    OwnText {
                        text: qsTr("ID")
                        font.pixelSize: 15
                        anchors.top: parent.top
                        anchors.topMargin: 5
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                    }

                    OwnText {
                        text:  qsTr("Need")
                        font.pixelSize: 15
                        anchors.top: parent.top
                        anchors.topMargin: 5
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.horizontalCenterOffset: -40
                    }

                    OwnText {
                        text: qsTr("Available")
                        font.pixelSize: 15
                       anchors.top: parent.top
                       anchors.topMargin: 5
                       anchors.horizontalCenter: parent.horizontalCenter
                       anchors.horizontalCenterOffset: 70
                    }

                    Item {
                        id: stepHeadingCenter
                        anchors.left: stepHeading.left
                        anchors.leftMargin: stepHeading.width/2 + 10
                    }

                    ListView {
                        id: stepNeedHeading
                        width: controller.getNumOfResources() * 35 < stepHeading.width/3 ? controller.getNumOfResources() * 35 : stepHeading.width/3
                        height: 30
                        anchors.right: stepHeadingCenter.left
                        anchors.rightMargin: 27
                        anchors.top: parent.top
                        anchors.topMargin: 20
                        orientation: Qt.Horizontal
                        clip: true

                        Connections {
                            target: stepHeading
                            onWidthChanged: {
                                stepNeedHeading.width = controller.getNumOfResources() * 35 < stepHeading.width/3 ? controller.getNumOfResources() * 35 : stepHeading.width/3
                                stepAvailableHeading.width = controller.getNumOfResources() * 35 < stepHeading.width/3 ? controller.getNumOfResources() * 35 : stepHeading.width/3
                                stepNeedScroll.policy = controller.getNumOfResources() * 30 < stepNeedHeading.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                                stepAvailableScroll.policy = controller.getNumOfResources() * 30 < stepNeedHeading.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                            }

                        }

                        model: controller.getNumOfResources()
                        spacing: 0
                        delegate: Rectangle {
                            id: stepNeedHeader
                            property int needPos: index
                            anchors.top: parent.top
                            width: 30
                            height: 25

                            OwnText {
                                text: "R" + stepNeedHeader.needPos
                                color: controller.getResourceColor(stepNeedHeader.needPos)
                                font.pixelSize: 13
                                anchors.centerIn: parent
                            }
                        }
                        ScrollBar.horizontal: ScrollBar {
                            id: stepNeedScroll
                            policy: controller.getNumOfResources() * 30 < (stepNeedHeading.width + 10) ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                        }
                    }//stepNeed

                    ListView {
                        id: stepAvailableHeading
                        width: controller.getNumOfResources() * 35 < stepNeedHeading.width ? controller.getNumOfResources() * 35 : stepHeading.width/3
                        height: 30
                        anchors.left: stepHeadingCenter.right
                        anchors.leftMargin: 25
                        anchors.top: parent.top
                        anchors.topMargin: 20
                        orientation: Qt.Horizontal
                        clip: true

                        model: controller.getNumOfResources()
                        spacing: 0
                        delegate: Rectangle {
                            id: stepAvailableHeader
                            property int needPos: index
                            anchors.top: parent.top
                            width: 30
                            height: 25

                            OwnText {
                                text: "R" + stepAvailableHeader.needPos
                                color: controller.getResourceColor(stepAvailableHeader.needPos)
                                font.pixelSize: 13
                                anchors.centerIn: parent
                            }
                        }
                        ScrollBar.horizontal: ScrollBar {
                            id: stepAvailableScroll
                            policy: controller.getNumOfResources() * 30 < (stepNeedHeading.width + 10) ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                        }
                    }//stepNeed

                }

                Rectangle {
                    id: stepResourceSeperator
                    visible: controller.getSequenceIndex() !== -1
                    width: parent.width
                    height: 1
                    color: "#828282"
                    anchors.bottom: stepResultBox.top
                    anchors.bottomMargin: 5
                }

                Rectangle {
                    id: stepResultBox
                    width: parent.width - 6
                    height: parent.height - 150
                    anchors.left: parent.left
                    anchors.leftMargin: 3
                    anchors.top: stepHeadingSeperator.bottom
                    anchors.topMargin: 65

                    OwnText {
                        id: stepHint
                        visible: controller.getSequenceIndex() === -1
                        text: qsTr("<- Select a safe sequence, if available")
                        anchors.centerIn: parent
                        font.bold: true
                        font.pixelSize: 16
                    }

                    ListView {
                        id: stepSequences
                        width: parent.width
                        height: parent.height - 6
                        anchors.left: parent.left
                        anchors.top: parent.top
                        clip: true

                        model: controller.getSequenceIndex() === -1 ? 0 : controller.getNumOfProcs()
                        spacing: 5
                        delegate: Rectangle {
                            id: stepLine
                            property int lineIndex: index
                            width: parent.width - 20
                            height: 25

                            OwnText {
                                id: stepProcId
                                text: "P"+controller.getStepId(stepLine.lineIndex)
                                font.pixelSize: 12
                                anchors.left: parent.left
                                anchors.leftMargin: 10
                                anchors.verticalCenter: parent.verticalCenter
                            }

                            ListView {
                                id: stepNeed
                                width: controller.getNumOfResources() * 35 < stepHeading.width/3 ? controller.getNumOfResources() * 35 : stepHeading.width/3//controller.getNumOfResources() <= 4 ? controller.getNumOfResources() * 35 : 130
                                height: 30
                                anchors.right: needSeperator.left
                                anchors.rightMargin: 20
                                anchors.top: parent.top
                                orientation: Qt.Horizontal
                                clip: true

                                model: controller.getNumOfResources()
                                spacing: 0
                                delegate: Rectangle {
                                    id: stepNeedWindow
                                    property int needPos: index
                                    anchors.top: parent.top
                                    width: 30
                                    height: 25

                                    OwnText {
                                        text: controller.getStepNeed(stepLine.lineIndex, stepNeedWindow.needPos)
                                        color: controller.getResourceColor(stepNeedWindow.needPos)
                                        font.pixelSize: 13
                                        anchors.centerIn: parent
                                    }
                                }

                                ScrollBar.horizontal: ScrollBar {
                                    id: stepNeedScroll2
                                    policy: ScrollBar.AlwaysOff
                                    position: stepNeedScroll.position

                                    Connections {
                                        target: stepNeedScroll
                                        onPositionChanged: stepNeedScroll2.position = stepNeedScroll.position
                                    }
                                }
                            }//stepNeed

                            OwnText {
                                id: needSeperator
                                text: "≤"
                                font.pixelSize: 20
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.left: stepLine.left
                                anchors.leftMargin: stepLine.width/2 + 10
                            }

                            ListView {
                                id: stepAvailable
                                width: controller.getNumOfResources() * 35 < stepHeading.width/3 ? controller.getNumOfResources() * 35 : stepHeading.width/3
                                height: 30
                                anchors.left: needSeperator.right
                                anchors.leftMargin: 20
                                anchors.top: parent.top
                                orientation: Qt.Horizontal
                                clip: true


                                model: controller.getNumOfResources()
                                spacing: 0
                                delegate: Rectangle {
                                    id: stepAvailableWindow
                                    property int availablePos: index
                                    anchors.top: parent.top
                                    width: 30
                                    height: 25


                                    OwnText {
                                        text: controller.getStepAt(stepLine.lineIndex, stepAvailableWindow.availablePos)
                                        color: controller.getResourceColor(stepAvailableWindow.availablePos)
                                        font.pixelSize: 13
                                        anchors.centerIn: parent
                                    }
                                }

                                ScrollBar.horizontal: ScrollBar {
                                    id: stepAvailableScroll2
                                    policy: ScrollBar.AlwaysOff
                                    position: stepAvailableScroll.position

                                    Connections {
                                        target: stepAvailableScroll
                                        onPositionChanged: stepAvailableScroll2.position = stepAvailableScroll.position
                                    }
                                }
                            }//stepNeed


                            Rectangle {
                                width: parent.width + 20
                                height: 1
                                color: "#828282"
                                anchors.bottom: parent.bottom
                                anchors.bottomMargin: -3
                            }
                        }//stepLine
                        ScrollBar.vertical: ScrollBar {
                            id: stepScroll
                            policy: controller.getNumOfProcs() * 25 > stepSequences.height ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                        }
                    }//stepSequence
                }//stepResultBox
            }//stepView
        } //outputPage
    }//swipeView
}//page
