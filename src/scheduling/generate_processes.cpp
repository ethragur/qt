#include <ctime>
#include <cmath>
#include <climits>
#include <algorithm>
#include <random>
#include <chrono>
#include "generate_processes.h"
#include "generate_scheduling.h"

#include <QDebug>

using namespace std;


/*
 *	This file contains the functions to create random processes with a specific amount of conflicts. The attributes for the processes are
 *  generated here and the verify-functions from generate_scheduling.cpp are called.
 */

mt19937 gen(chrono::system_clock::now().time_since_epoch().count());

bool is_smaller(unsigned short x, unsigned short y)
{
    return x < y;
}

unsigned short generate_unique_id(std::vector<process_t> *proc_list, unsigned short amount)
{
    uniform_real_distribution<double> random_id(0.0,(double) amount);
    //the first selected id can be completely random, because no other id has been used so far
    if (proc_list->empty())
        return (unsigned short) random_id(gen);

    bool exists_already = false;
    /* vector which holds all numbers from 0 to n, where n is the amount of process that
     * should be generated
     */
    std::vector<unsigned short> possible_ids;

    //for loop that adds all possible id values to the list
    for (size_t i=0; i < amount; i++)
    {
        //every id, that has been used already, can not be added
        for (process_t proc : *proc_list)
        {
            if (proc.id == i)
                exists_already = true;
        }
        if (!exists_already)
            possible_ids.push_back(i);
        exists_already = false;
    }

    /* construct to generate random number between 0 and number of elements in
     * the possible id list
     */
    uniform_real_distribution<double> random_id_index(0.0,(double) possible_ids.size());

    //return a random number from the id list
    return possible_ids[(size_t) random_id_index(gen)];
}


/* A conflicts is a situation where the execution of a scheduling algorithm can produce
 * two possible schedules, because at a time slice more than one process can be selected.
 * Two conflicts can produce three possible schedules.
 */
unsigned short generate_arrival(std::vector<process_t> *proc_list, generation_process_queue_t *rr_queue ,
                                std::vector<unsigned short> *should_conflict,
                                unsigned short amount, unsigned short max, unsigned short created,
                                unsigned short quantum, unsigned short *arrival_helper,
                                unsigned short *tolerance)
{
    /* if some processes are already created and values leading to multiple schedules
     * should be created
     */
    if (!proc_list->empty() && !should_conflict->empty())
    {
        //if the last process is generated
        if (created == amount - 1)
        {
            should_conflict->erase(should_conflict->begin());
            return proc_list->back().arrival;
        }

        //if a conflict should be added at the given position
        if (should_conflict->front() ==  proc_list->back().arrival)
        {
            should_conflict->erase(should_conflict->begin());
            return proc_list->back().arrival;
        }

        //if process should conflict in the current area
        else if (proc_list->size() >= 2 && proc_list->at(proc_list->size() - 2).arrival < should_conflict->front() &&
                 proc_list->back().arrival > should_conflict->front())
        {
            should_conflict->erase(should_conflict->begin());
            return proc_list->back().arrival;
        }

        //if conflict should have already been inserted and one process has been generated
        else if (proc_list->size() == 1 && proc_list->back().arrival >= should_conflict->front())
        {
            should_conflict->erase(should_conflict->begin());
            return proc_list->back().arrival;
        }

        //if conflict should have already been inserted
        else if (proc_list->back().arrival > should_conflict->front())
        {
            should_conflict->erase(should_conflict->begin());
            return proc_list->back().arrival;
        }
    }

    //if only 1 process should be generated, very little logic is necessary
    if (amount == 1)
    {
        //generate a random value between 0 and max
        uniform_real_distribution<double> random_value(0.0,(double) max);
        return (unsigned short) random_value(gen);
    }

    bool found_valid_values = false;
    std::vector<unsigned short> possible_arrivals;
    std::vector<unsigned short> conflict_arrivals;
    unsigned short selected = 0, apply_random_tolerance = 0;

    uniform_real_distribution<double> random_tolerance_probability(1.0, (double) 100.0);

    //if quantum is not 0, the rr algorithm has to be considered
    if (quantum != 0)
    {
        //calculate all values that would lead to multiple schedules
        check_rr(proc_list, rr_queue, quantum, &conflict_arrivals);
        //sort list accending
        std::sort(conflict_arrivals.begin(), conflict_arrivals.end(), is_smaller);
        //remove duplicates
        conflict_arrivals.erase(unique(conflict_arrivals.begin(), conflict_arrivals.end()), conflict_arrivals.end());
    }

    //if no process has been generated yet
    if (proc_list->empty())
    {
        found_valid_values = true;
        //the tolerance describes the difference between the possible max and possible min value
        *tolerance = max - amount;
        //if a tolerance exists
        if (*tolerance > 0)
        {
            //the tolerance probability is used to add more randomness to the gerated value
            unsigned short tolerance_prop = (((double)*tolerance/max)*200.0);
            //a random value descides how many possible arrival values are available
            if ((unsigned short)random_tolerance_probability(gen) <= tolerance_prop)
                apply_random_tolerance = *tolerance/amount + (unsigned short) ceil(((double)*tolerance/amount)*((double)tolerance_prop/100.0));
        }

        for (int i=0; i <= apply_random_tolerance; i++)
            possible_arrivals.push_back(i);
    }

    else
    {
        if (*tolerance > 0)
        {
            unsigned short tolerance_prop = (((double)*tolerance/max)*200.0);

            if ((unsigned short)random_tolerance_probability(gen) <= tolerance_prop)
                apply_random_tolerance = (unsigned short)ceil((double)*tolerance/amount) + (unsigned short)floor(((double)*tolerance/amount)*((double)tolerance_prop/100.0));
        }

        bool found_conflict = false;
        /* arrival_helper saves the arrival value of the latest generated proess
         * in the loop every possible value that is the tolerance area is checked
         */
        for (int i = *arrival_helper; i <= *arrival_helper + apply_random_tolerance && i <= max; i++)
        {
            //iterate over all values that would lead to multiple schedules
            for (size_t j = 0; j < conflict_arrivals.size() && conflict_arrivals[j] <= i; j++)
            {
                //if multiple schedules should be produced, set flag that one is found
                if (i ==  conflict_arrivals[j])
                    found_conflict = true;
            }

            //if multiple schedules should not be produced, add safe value to list
            if (!found_conflict)
            {
                possible_arrivals.push_back(i);
                found_valid_values = true;
            }
            found_conflict = false;
        }
    }

    //if no valid value was found, return the max possible unsigned short value, indicating failure
    if (!found_valid_values || possible_arrivals.empty())
    {
        return USHRT_MAX;
    }

    uniform_real_distribution<double> rando(0.0,(double) possible_arrivals.size());
    //select random value from possible list
    selected = possible_arrivals[(size_t) rando(gen)];
    *arrival_helper = selected+1;
    return selected;

}



unsigned short generate_duration(std::vector<process_t> *proc_list, generation_process_queue_t *sjf_queue,
                                 generation_process_queue_t *srtf_queue, process_t *new_proc, unsigned short should_conflict,
                                 unsigned short max, unsigned short *sjf_conflict_counter, unsigned short *srtf_conflict_counter)
{
    std::vector<unsigned short> possible_remaining;
    std::vector<unsigned short> conflict_sjf;
    std::vector<unsigned short> conflict_srtf;
    std::vector<unsigned short> common_conflicts;
    std::vector<unsigned short> conflict_remaining;

    //calculate process queues for both SJF and SRTF
    check_sjf(proc_list, sjf_queue, new_proc, &conflict_sjf);
    check_srtf(proc_list, srtf_queue, new_proc, &conflict_srtf);

    std::sort(conflict_sjf.begin(), conflict_sjf.end());
    std::sort(conflict_srtf.begin(), conflict_srtf.end());

    //intersect both lists
    std::set_intersection(conflict_sjf.begin(), conflict_sjf.end(),
                          conflict_srtf.begin(), conflict_srtf.end(),
                          std::back_inserter(common_conflicts));

    if (should_conflict != 0 && (*sjf_conflict_counter < should_conflict || *srtf_conflict_counter < should_conflict))
    {
        if (!common_conflicts.empty() && *sjf_conflict_counter < should_conflict && *srtf_conflict_counter < should_conflict)
        {
            *sjf_conflict_counter = *sjf_conflict_counter + 1;
            *srtf_conflict_counter = *srtf_conflict_counter + 1;
            uniform_int_distribution<int> random_idx(0, common_conflicts.size()-1);
            return common_conflicts[random_idx(gen)];
        }

     if (!conflict_sjf.empty() && *sjf_conflict_counter < should_conflict && *sjf_conflict_counter <= *srtf_conflict_counter)
        {
            uniform_int_distribution<int> random_idx(0, conflict_sjf.size()-1);
            *sjf_conflict_counter = *sjf_conflict_counter + 1;
            return conflict_sjf[random_idx(gen)];
        }

        else if (!conflict_srtf.empty() && *srtf_conflict_counter < should_conflict)
        {
            uniform_int_distribution<int> random_idx(0, conflict_srtf.size()-1);
            *srtf_conflict_counter = *srtf_conflict_counter + 1;
            return conflict_srtf[random_idx(gen)];
        }
    }

    std::set_union(conflict_sjf.begin(), conflict_sjf.end(),
                   conflict_srtf.begin(), conflict_srtf.end(),
                   std::back_inserter(conflict_remaining));

    //remove duplicates from list of conflict values
    std::sort(conflict_remaining.begin(), conflict_remaining.end(), is_smaller);
    conflict_remaining.erase(unique(conflict_remaining.begin(), conflict_remaining.end()), conflict_remaining.end());

    bool should_add = true;
    for (size_t i=1; i < max; i++)
    {
        for (size_t j=0; j < conflict_remaining.size(); j++)
        {
            if (i == conflict_remaining[j])
                should_add = false;
        }

        if (should_add)
            possible_remaining.push_back(i);
        should_add = true;
    }

    if (possible_remaining.empty())
        return 0;

    uniform_real_distribution<double> random_possible_duration_idx(0.0,(double) possible_remaining.size());
    return possible_remaining[(size_t) random_possible_duration_idx(gen)];
}


unsigned short generate_priority(std::vector<process_t> *proc_list, generation_process_queue_t *ps_non_queue,
                                 generation_process_queue_t *ps_pre_queue ,process_t *new_proc, unsigned short should_conflict,
                                 unsigned short max, unsigned short *ps_non_conflict_counter, unsigned short *ps_pre_conflict_counter)
{
    std::vector<unsigned short> possible_priority;
    std::vector<unsigned short> conflict_ps_non;
    std::vector<unsigned short> conflict_ps_pre;
    std::vector<unsigned short> common_conflicts;
    std::vector<unsigned short> conflict_priority;


    check_ps_non(proc_list, ps_non_queue, new_proc, &conflict_ps_non);
    check_ps_pre(proc_list, ps_pre_queue, new_proc, &conflict_ps_pre);

    std::sort(conflict_ps_non.begin(), conflict_ps_non.end());
    std::sort(conflict_ps_pre.begin(), conflict_ps_pre.end());

    std::set_intersection(conflict_ps_non.begin(), conflict_ps_non.end(),
                          conflict_ps_pre.begin(), conflict_ps_pre.end(),
                          std::back_inserter(common_conflicts));


    if (should_conflict != 0 && (*ps_non_conflict_counter < should_conflict || *ps_pre_conflict_counter < should_conflict))
    {
        if (!common_conflicts.empty() && *ps_non_conflict_counter < should_conflict && *ps_pre_conflict_counter < should_conflict)
        {
            *ps_non_conflict_counter = *ps_non_conflict_counter + 1;
            *ps_pre_conflict_counter = *ps_pre_conflict_counter + 1;
            uniform_int_distribution<int> random_idx(0, common_conflicts.size()-1);
            return common_conflicts[random_idx(gen)];
        }

     if (!conflict_ps_non.empty() && *ps_non_conflict_counter < should_conflict && *ps_non_conflict_counter <= *ps_pre_conflict_counter)
        {
            uniform_int_distribution<int> random_idx(0, conflict_ps_non.size()-1);
            *ps_non_conflict_counter = *ps_non_conflict_counter + 1;
            return conflict_ps_non[random_idx(gen)];
        }

        else if (!conflict_ps_pre.empty() && *ps_pre_conflict_counter < should_conflict)
        {
            uniform_int_distribution<int> random_idx(0, conflict_ps_pre.size()-1);
            *ps_pre_conflict_counter = *ps_pre_conflict_counter + 1;
            return conflict_ps_pre[random_idx(gen)];
        }
    }

    std::set_union(conflict_ps_non.begin(), conflict_ps_non.end(),
                   conflict_ps_pre.begin(), conflict_ps_pre.end(),
                   std::back_inserter(conflict_priority));

    //remove duplicates from list of conflict values
    std::sort(conflict_priority.begin(), conflict_priority.end(), is_smaller);
    conflict_priority.erase(unique(conflict_priority.begin(), conflict_priority.end()), conflict_priority.end());

    bool should_add = true;
    for (size_t i=1; i < max; i++)
    {
        for (size_t j=0; j < conflict_priority.size(); j++)
        {
            if (i == conflict_priority[j])
                should_add = false;
        }
        if (should_add)
            possible_priority.push_back(i);
        should_add = true;
    }


    if (possible_priority.empty())
        return 0;

    uniform_real_distribution<double> random_possible_priority_idx(0.0,(double) possible_priority.size());
    return possible_priority[(size_t) random_possible_priority_idx(gen)];

}

unsigned short generate_tickets(std::vector<process_t> *proc_list, generation_process_queue_t *ls_schedule,
                                unsigned short should_conflict, unsigned short max, unsigned short *ls_counter, unsigned short quantum)
{
    std::vector<unsigned short> possible_tickets;
    std::vector<unsigned short> conflict_ls;

    check_ls(proc_list, ls_schedule, &conflict_ls, quantum);

    std::sort(conflict_ls.begin(), conflict_ls.end());
    conflict_ls.erase(unique(conflict_ls.begin(), conflict_ls.end()), conflict_ls.end());

    if (should_conflict && *ls_counter < should_conflict && !conflict_ls.empty())
    {
        *ls_counter = *ls_counter + 1;
        uniform_int_distribution<int> random_idx(0, conflict_ls.size()-1);
        int idx = random_idx(gen);
        return conflict_ls[idx];
    }

    bool should_add = true;
    for (size_t i=1; i < max; i++)
    {
        for (size_t j=1; j < conflict_ls.size(); j++)
        {
            if (i == conflict_ls[j])
                should_add = false;
        }
        if (should_add)
            possible_tickets.push_back(i);
        should_add = true;
    }

    if (possible_tickets.empty())
        return 0;

    uniform_int_distribution<int> random_possible_idx(0, possible_tickets.size()-1);
    int idx = random_possible_idx(gen);

    return possible_tickets[idx];
}


std::vector<process_t> generate_procs(unsigned short amount, unsigned short max,
                                      unsigned short quantum, unsigned short lsQuantum, unsigned short alt_solutions)
{
    bool done = false;
    unsigned short created = 0, tries = 0, tolerance = 0, arrival_helper = 0;
    std::vector<process_t> new_proc_list;
    uniform_int_distribution<int> random_value(1, max);
    uniform_int_distribution<int> random_usage(1, lsQuantum);

    if (alt_solutions == 0)
    {
        while (!done)
        {
            process_t new_proc;
            new_proc.id = generate_unique_id(&new_proc_list, amount);
            new_proc.arrival = random_value(gen) - 1;
            new_proc.duration = random_value(gen);
            new_proc.remaining_time = new_proc.duration;
            new_proc.priority = random_value(gen);
            new_proc.tickets = random_value(gen);
            new_proc.quantum_usage = random_usage(gen);

            new_proc_list.push_back(new_proc);
            created++;
            done = created >= amount;
        }
        return new_proc_list;
    }

    /* the algorithm was designed to create every specific amount of conflicts,
     * because this feature is not necessary for the application, the argument,
     * defining the amount of conflicts is set to 0
     */

    alt_solutions = 0;


    unsigned short sjf_conflict_counter= 0, srtf_conflict_counter = 0, ps_non_conflict_counter = 0, ps_pre_conflict_counter = 0,
            ls_conflict_counter = 0;
    std::vector<unsigned short> arrival_conflict_positions;
    generation_process_queue_t sjf_queue, srtf_queue, rr_queue, ps_non_queue, ps_pre_queue, ls_queue;
    srtf_queue.timestamp = 0;

    uniform_real_distribution<double> random_tickets(1.0,(double) max);

    // random selection of positions for arrival conflicts
    uniform_real_distribution<double> random_arrival(0.0, (double) amount);
    for (int i=0; i < alt_solutions; i++)
        arrival_conflict_positions.push_back(random_arrival(gen));

    std::sort(arrival_conflict_positions.begin(), arrival_conflict_positions.end());

    while (!done)
    {
        process_t new_proc;
        new_proc.id = generate_unique_id(&new_proc_list, amount);
        new_proc.arrival = generate_arrival(&new_proc_list, &rr_queue, &arrival_conflict_positions, amount, max, created, quantum,
                                            &arrival_helper, &tolerance);
        if (new_proc.arrival != USHRT_MAX)
            new_proc.duration = generate_duration(&new_proc_list, &sjf_queue, &srtf_queue, &new_proc, alt_solutions,
                                                  max, &sjf_conflict_counter, &srtf_conflict_counter);
        new_proc.remaining_time = new_proc.duration;

        if (new_proc.arrival != USHRT_MAX || new_proc.duration != 0)
            new_proc.priority = generate_priority(&new_proc_list, &ps_non_queue, &ps_pre_queue, &new_proc,
                                                  alt_solutions, max, &ps_non_conflict_counter, &ps_pre_conflict_counter);

        new_proc.quantum_usage = random_usage(gen);
        new_proc.tickets = generate_tickets(&new_proc_list, &ls_queue, alt_solutions, max, &ls_conflict_counter, lsQuantum);

        if(new_proc.arrival == USHRT_MAX || new_proc.priority == 0 || new_proc.duration == 0 || new_proc.tickets == 0)
        {
            tries++;
            created = 0;
            sjf_conflict_counter = 0;
            srtf_conflict_counter = 0;
            ps_non_conflict_counter = 0;
            ps_pre_conflict_counter = 0;
            tolerance = 0;
            arrival_helper = 0;
            new_proc_list.clear();
            sjf_queue.timestamp = 0;
            sjf_queue.process_queue.clear();
            srtf_queue.timestamp = 0;
            srtf_queue.prev_timestamp = 0;
            srtf_queue.process_queue.clear();
            rr_queue.timestamp = 0;
            rr_queue.process_queue.clear();
            ps_non_queue.timestamp = 0;
            ps_non_queue.process_queue.clear();
            ps_pre_queue.timestamp = 0;
            ps_pre_queue.process_queue.clear();
            if (tries >= 20)
                return new_proc_list;
            continue;
        }

        new_proc_list.push_back(new_proc);
        created++;

        done = created >= amount;
    }

    return new_proc_list;
}
