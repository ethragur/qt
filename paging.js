


var pageSettings = { pageNr:  8, pageFrames: 8, inputLength: 12, algorithm: 'FIFO' };
var input = new Array(pageSettings.inputLength);

var eventsNotifier = Qt.createQmlObject('import QtQuick 2.0; QtObject { signal dataChanged }', Qt.application, 'EventsNotifier')


function fillInput() {
	for(var i = 0; i < pageSettings.inputLength; i++)
	{
		input[i] = Math.floor(Math.random()*pageSettings.pageNr + 1);
	}
	console.log(input);
}

function itemAt(i) {
	return input[i];
}

function addInput() {
}

function removeInput() {
}


function clear() {
	input = new Array(pageSettings.inputLength);
	fillInput();
	eventsNotifier.dataChanged();
}





function solve() {
	console.log("solving");
	pageFrames = new Array(pageSettings.pageFrames);

	for(var i = 0; i < pageSettings.pageFrames; i++) {
		pageFrames[i] = { step: 0, item: 0 };
	}

	var currentStep = 1;

	input.forEach(function(e) {
		var frameIndex = findFirstOrEmpty(e, pageFrames);

		if( frameIndex !== -1 )
		{
			pageFrames[frameIndex] = { step: currentStep, item: e};
		}

		printFrames(pageFrames);
		currentStep++;
	});
}

function printFrames(frames) {
	var i = 0;
	frames.forEach(function(frame) {
		console.log('Frame: ' + i + '; item: ' + frame.item + '; step: ' + frame.step );
		i++;
	});

	console.log('\n');
}


function findFirstOrEmpty(element, frames) {
	var currentMin = Number.MAX_SAFE_INTEGER;
	var minIndex = 0;



	for(var i = 0; i < pageSettings.pageFrames; i++) {
		if(frames[i].item == element)
		{
			return -1;
		}
		if(frames[i].step < currentMin)
		{
			currentMin = frames[i].step;
			minIndex = i;
		}
	}

	return minIndex;
}

fillInput();
