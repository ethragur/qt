#pragma once


#include <QObject>
#include <QVector>
#include <QPair>
#include <QDebug>


class AllocationAlgo
{

	public:
		//Overwrite this function for the different Algorithms
		virtual void solve() = 0;

		void print() const 
		{
			for(const auto & i : m_memoryList)
			{
				qDebug() << i << " ";
			}
		}

		QVector<int> getMemoryList()
		{
			return m_memoryList;
		}

		void addInput(QPair<int, int> input)
		{
			m_inputList.push_back(input);
		}

	protected:
		//Protected constructor, should never be instanced directly
		AllocationAlgo(const int memorySize, const QVector<int> & memoryList, const QVector<QPair<int,int>> & input)  :
			m_memorySize(memorySize), m_memoryList(memoryList), m_inputList(input)
		{
		}

		int m_memorySize;
		QVector<int>  m_memoryList;
		QVector<QPair<int, int>> m_inputList;

};

class FirstFitAlgo : public AllocationAlgo
{
	public:
		FirstFitAlgo(const int memorySize, const QVector<int> & memoryList, const QVector<QPair<int,int>> & input) ;
		void solve();

};

class NextFitAlgo : public AllocationAlgo
{
	public:
		NextFitAlgo(const int memorySize, const QVector<int> & memoryList, const QVector<QPair<int,int>> & input) ;
		void solve();
protected: 
		int m_pointerPos;

};

class BestFitAlgo : public AllocationAlgo
{
	public:
		BestFitAlgo(const int memorySize, const QVector<int> & memoryList, const QVector<QPair<int,int>> & input) ;
		void solve();

};

class WorstFitAlgo : public AllocationAlgo
{
	public:
		WorstFitAlgo(const int memorySize, const QVector<int> & memoryList, const QVector<QPair<int,int>> & input) ;
		void solve();

};
