import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3


ListView {
    id: pagingOutputView
    width: parent.width
    height: 800
    anchors.topMargin: 5
    clip: true

    model: 4

    property int itemHeight : pagingController.pageFramesCount * 22 + 160
    spacing: 10
    delegate:
    Item {
        id: rsultItem2
        width: pagingOutputView.width
        height: pagingOutputView.itemHeight

        Rectangle {
            id: resultArea
            property int scheduleIndex: index
            width: parent.width - 20
            height:parent.height - 50
            anchors.horizontalCenter: parent.horizontalCenter
            border.color: "#828282"
            border.width: 1

            Text {
                id: scheduleHeading
                text: "FIFO"
                anchors.horizontalCenter: parent.horizontalCenter
                font.family: "Ubuntu"
                font.pixelSize: 20
                color: "#525252"
                anchors.top: parent.top
                anchors.topMargin: 5
            }
            //
            // Diagram Ids
            Rectangle {
                id: idsBorder
                height: pagingController.pageFramesCount * 20 + 2 * pagingController.pageFramesCount
                width: 50
                anchors.top: resultArea.top
                anchors.topMargin: 50
                anchors.left: resultArea.left
                anchors.leftMargin: 5
                border.color: "#888888"
                border.width: 2

                ListView {
                    id: diagramIds
                    anchors.fill: parent

                    clip: true
                    orientation: Qt.Vertical

                    model: pagingController.pageFramesCount 
                    spacing: 2
                    delegate:

                    Rectangle {
                        id: idWindow
                        property int rowId: index
                        width: diagramIds.width
                        height: 20
                        color: "#000000"

                        OwnText {
                            anchors.centerIn: parent
                            text: "F"+idWindow.rowId
                            color: "#ffffff"
                        }
                    }

                }
            }

            ListView{
                id: diagramData
                implicitHeight: pagingController.pageFramesCount * 20 + 2 * pagingController.pageFramesCount + 66
                implicitWidth: parent.width - 70
                anchors.top: idsBorder.top
                anchors.left: idsBorder.right
                anchors.leftMargin: 10
                clip: true
                orientation: Qt.Horizontal

                model: pagingController.inputLength
                delegate:
                //time slice
                Rectangle {
                    id: processFrame
                    property int processIndex: index
                    width: 40
                    height: parent.height - 50
                    //border.color: "#828282"

                    ListView {
                        id: diagramPos
                        anchors.fill: parent
                        clip: true
                        orientation: Qt.Vertical

                        model:  pagingController.pageFramesCount
                        delegate:
                        Rectangle {
                            id: singleFrame
                            width: 40
                            height: 22
                            border.color: "#ff0000"
                            border.width:  1
                            color: "#aaaaaa"

                        }
                    }
                    OwnText {
                        text: index
                        color: "#323232"
                        font.pixelSize: 10
                        anchors.top: parent.bottom
                        anchors.topMargin: -15
                        anchors.left: processFrame.left
                    }

                }

            }
        }
    }
    ScrollBar.vertical: ScrollBar {
        policy: ScrollBar.AlwaysOn
    }
}
