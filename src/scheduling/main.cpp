#include <iostream>
#include <algorithm>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <random>
#include <cstring>
#include <ctype.h>
#include <unistd.h>
#include "process.h"
#include "queue.h"
#include "scheduling.h"
#include "generate_processes.h"
#include "metric.h"

using namespace std;

void show_help();
process_list_t *user_input(process_list_t *head);
process_list_t *test_data(process_list_t *head);
process_list_t *random_input(process_list_t *head, int max);
process_list_t *random_generated(process_list_t *head);
process_list_t *error_data(process_list_t *head);

int main(int argc, char **argv)
{
  bool valid_parameter = false;
  process_list_t *new_proc_list = NULL;
  if (argc == 2)
    {
      if (strcmp(argv[1], "-h") == 0)
	{	 
	  show_help();
	  return 0;
	}

      if (strcmp(argv[1], "-t") == 0)
	{
	  new_proc_list = test_data(new_proc_list);
	  valid_parameter = true;
	}

      if (strcmp(argv[1], "-a") == 0)
	{
	  printf("Enter max value: ");
	  int max = 0;
	  cin>>max;
	  while (max <= 0 && max >= 60000)
	    {
	      printf("The entered value is invalid!\nPlease enter a other max value: ");
	      cin>>max;
	    }
	  new_proc_list = random_input(new_proc_list, max);
	  valid_parameter = true;
	}

      
      if (strcmp(argv[1], "-e") == 0)
	{
	  new_proc_list = error_data(new_proc_list);
	  valid_parameter = true;
	}

      
      if (strcmp(argv[1], "-g") == 0)
	{
	  new_proc_list = random_generated(new_proc_list);
	  if (new_proc_list == NULL)
	    {
	      cout<<"Failed to create random processes!\n";
	      return 1;
	    }
	  valid_parameter = true;
	}

      if (!valid_parameter)
	{
	  printf("Parameter not defined!\n");
	  show_help();
	  return 0;
	}
    }

  else if (argc == 1)
    {
      new_proc_list = user_input(new_proc_list);
    }

  else
    {
      printf("Invalid starting parameters!\n");
      show_help();
      return 0;
    }

  proc_print(new_proc_list);

  scheduling(FCFS, new_proc_list); 
  scheduling(SJF, new_proc_list); 
  scheduling(SRTF, new_proc_list); 
  scheduling(RR, new_proc_list, 1); 
  scheduling(RR, new_proc_list, 4);
  scheduling(PS, new_proc_list, NON_PREEMTIVE);
  // scheduling(PS, new_proc_list, PREEMTIVE); 
  // scheduling(LS, new_proc_list, 2);
  proc_clear(new_proc_list);
  
  return 0;
}


void show_help()
{
  printf("\nSimple scheduling program\n*************************\n\n");
  printf("Parameters: \n^^^^^^^^^^ \n -t \t use test data \n -h \t show help\n -g \t generate random processes\n");
}


process_list_t *user_input(process_list_t *head)
{
  std::string attribute_name[] = {"Insertion point", "       Duration", "       Priority"};
  unsigned short id=0, attribute_value[3];
  bool creating_processes = true, successful_input = false;  
  
  while(creating_processes)
    {
      printf("Values for Process %d\n",id);
					    
      for(int i=0; i<3; i++)
	{
	  std::cout<<attribute_name[i]<<": ";	  

	  while(std::cin>>attribute_value[i])
	    {
	      successful_input = true;
	      break;
	    }
	  if(!successful_input)
	    {
	      std::cout<<"\n\n\nExited because esc!"<<std::endl;
	      creating_processes = false;	  
	      std::cout<<"Finished input with "<<id<<" processes!"<<std::endl;
	      std::cout<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n"<<std::endl;
	      break;
	    }
	  successful_input = false;
	}

      if(creating_processes)
	{
	  process_t new_process;
	  new_process.id = id;
	  new_process.insertion_time = attribute_value[0];
	  new_process.remaining_time = attribute_value[1];
	  new_process.priority = attribute_value[2];
	  new_process.quantum_usage = 0;
	  new_process.tickets = 0;
	  proc_insert(&head, &new_process);
	  id++;
	     
	  std::cout<<"Process successfully created"<<std::endl;
	  std::cout<<"----------------------------\n"<<std::endl;
	}
    }
  
  return head;
}

process_list_t *test_data(process_list_t *head)
  {
    process_t new_process[6];
    
    new_process[0].id = 0;
    new_process[0].insertion_time = 0;
    new_process[0].remaining_time = 3;
    new_process[0].priority = 3;
    new_process[0].tickets = 10;
    new_process[0].quantum_usage = 0;
  
    new_process[1].id = 1;
    new_process[1].insertion_time = 2;
    new_process[1].remaining_time = 6;
    new_process[1].priority = 3;
    new_process[1].tickets = 21;
    new_process[1].quantum_usage = 0;
  
    new_process[2].id = 2;
    new_process[2].insertion_time = 4;
    new_process[2].remaining_time = 4;
    new_process[2].priority = 2;
    new_process[2].tickets = 7;
    new_process[2].quantum_usage = 0;

    new_process[3].id = 3;
    new_process[3].insertion_time = 6;
    new_process[3].remaining_time = 5;
    new_process[3].priority = 2;
    new_process[3].tickets = 7;
    new_process[3].quantum_usage = 0;
  
    new_process[4].id = 4;
    new_process[4].insertion_time = 8;
    new_process[4].remaining_time = 2;
    new_process[4].priority = 1;
    new_process[4].tickets = 7;
    new_process[4].quantum_usage = 1;


    // ADDITIONAL TEST DATA
    new_process[5].id=5;
    new_process[5].insertion_time = 21;
    new_process[5].remaining_time = 1;
    new_process[5].priority = 3;
    new_process[5].tickets = 14;
    new_process[5].quantum_usage = 1;

    
    proc_insert(&head, &new_process[0]);
    proc_insert(&head, &new_process[1]);
    proc_insert(&head, &new_process[2]);
    proc_insert(&head, &new_process[3]);
    proc_insert(&head, &new_process[4]);
    //proc_insert(&head, &new_process[5]); 

    return head;
  }


process_list_t *random_input(process_list_t *head, int max_value)
{
  random_device rd;
  uniform_real_distribution<double> rando(0.0, (double)max_value);
  uniform_real_distribution<double> rando_restricted(1.0, 10.0);
  unsigned short amount_of_processes = (unsigned short) rando(rd) + 1;
  unsigned short id_counter = 0, new_insertion_time;
  vector<unsigned short> insertion_list;
  vector<unsigned short> remaining_list;
  
  for (int i=0; i<amount_of_processes; i++)
    {
      process_t new_proc;
      new_proc.id = id_counter;

      /* find unique insertion time, because same value would lead to multiple possible
       * solution for first-come first-served
       */
      unsigned short tries = 0;
      bool found = false;
      while (!found)
	{
	  new_insertion_time = rando(rd);
	  if (find(insertion_list.begin(), insertion_list.end(), new_insertion_time)
	      == insertion_list.end())
	    {
	      insertion_list.push_back(new_insertion_time);
	      found = true;
	    }

	  else
	    {
	      tries++;
	      if (tries >= 50) {
		max_value += 20;
		tries = 0;
	      }
	    }
	}
      
      new_proc.insertion_time = new_insertion_time;
      
      new_proc.remaining_time = (unsigned short)rando(rd) + 1;
      new_proc.priority = (unsigned short)rando_restricted(rd);
      new_proc.tickets = (unsigned short)rando(rd)+1;
      new_proc.quantum_usage = (unsigned short) rando_restricted(rd);
      id_counter++;

      proc_insert(&head, &new_proc);
    }
  

  
  return head;
}


process_list_t *random_generated(process_list_t *head)
{
  unsigned short amount, max, alt_solutions;
  std::cout<<"\nRandom generated processes:\n--------------------------\n"<<std::endl;
  std::cout<<"Amount of processes: ";
  std::cin>>amount;

  if (amount > 60000)
    {
      std::cout<<"Amount was too big, value under 60000 is required\n\n";
      return NULL;
    }

  std::cout<<"Max value: ";
  std::cin>>max;
  while (max < amount)
    {
      std::cout<<"\nMax value can not be smaller than the amount of processes!\n";
      std::cout<<"Please enter new max value: ";
      std::cin>>max;
    }
  
  std::cout<<"Amount of alternative Solutions: ";
  std::cin>>alt_solutions;
  while (alt_solutions > amount)
    {
      std::cout<<"\nThere can't be more solutions than processes!\n";
      std::cout<<"Please enter new amount of solutions: ";
      std::cin>>alt_solutions;
    }

  generate_procs(head, amount, max, alt_solutions);

  cout<<"\ngenerated all procs:\n";
  process_list_t *it = head;
  while (it != NULL)
    {
      cout<<"id: "<<it->process.id<<endl;
      cout<<"insertion: "<<it->process.insertion_time<<endl;
      cout<<"remaing: "<<it->process.remaining_time<<"\n"<<endl;
      it = it->next;
      
    }
  
  return head;
}


process_list_t *error_data(process_list_t *head)
  {
    process_t new_process[4];
    
    new_process[0].id = 5;
    new_process[0].insertion_time = 1;
    new_process[0].remaining_time = 4;
    new_process[0].priority = 5;
    new_process[0].tickets = 10;
    new_process[0].quantum_usage = 0;
  
    new_process[1].id = 4;
    new_process[1].insertion_time = 2;
    new_process[1].remaining_time = 2;
    new_process[1].priority = 5;
    new_process[1].tickets = 10;
    new_process[1].quantum_usage = 0;

    new_process[2].id = 3;
    new_process[2].insertion_time = 2;
    new_process[2].remaining_time = 2;
    new_process[2].priority = 5;
    new_process[2].tickets = 10;
    new_process[2].quantum_usage = 0;

    new_process[3].id = 1;
    new_process[3].insertion_time = 5;
    new_process[3].remaining_time = 5;
    new_process[3].priority = 5;
    new_process[3].tickets = 10;
    new_process[3].quantum_usage = 0;

    proc_insert(&head, &new_process[0]);
    proc_insert(&head, &new_process[1]);
    proc_insert(&head, &new_process[2]);
    proc_insert(&head, &new_process[3]);
  
    return head;
  }

