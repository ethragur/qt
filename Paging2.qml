import "./buttons"
import "./input"
import "./output"
import QtQuick 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.0


import Paging 1.0

Page {
    id: pagingView

    property int iCurrentStep: 0
    property var colorArray: ["lightgrey", "aqua", "chartreuse", "crimson", "dodgerblue", "fuchsia", "gold", "lightgreen", "orange", "pink", "teal", "peru", "navy", "lightsteelblue", "yellow", "red"]

    PagingController {
        id: pagingController
    }

    //Menu button
    Rectangle {
        id: menuButton
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 20

        width: 40
        height: 40
        border.color: "#828282"
        border.width: 2

        Rectangle {
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 5

            width: 13
            height: 13
            color: "#828282"
        }

        Rectangle {
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 5

            width: 13
            height: 13
            color: "#828282"
        }

        Rectangle {
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5

            width: 13
            height: 13
            color: "#828282"
        }

        Rectangle {
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5

            width: 13
            height: 13
            color: "#828282"
        }

        MouseArea {
            id: menuMouseArea
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            hoverEnabled: true
            onClicked: stackView.pop("Paging2.qml")
        }
    }


    //Heading
    OwnText {
        id: header
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20
        font.pixelSize: 28
        font.bold: true
        font.underline: true
        text: qsTr(" Paging ")
    }

    //__Input__ site switcher
    Rectangle {
        id: inputSelection

        property bool onPage: swipeView.currentIndex == 0

        height: 30
        width: 100
        anchors.bottom: header.bottom
        anchors.bottomMargin: -20
        anchors.left: parent.left
        anchors.leftMargin: parent.width/4 - 50
        opacity: onPage ? 1 : 0.3

        OwnText {
            anchors.centerIn: parent
            font.pixelSize: 20
            font.bold: parent.onPage ? true : false
            font.underline: true
            text: qsTr("    Input    ")
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: swipeView.setCurrentIndex(0)
        }
    }

    //__Output__ site switcher
    Rectangle {
        id: outputSelection

        property bool onPage: swipeView.currentIndex == 1

        height: 30
        width: 100
        anchors.bottom: header.bottom
        anchors.bottomMargin: -20
        anchors.left: parent.left
        anchors.leftMargin: 3*(parent.width/4) - 50
        opacity: onPage ? 1 : 0.3

        OwnText {
            anchors.centerIn: parent
            font.pixelSize: 20
            font.bold: parent.onPage ? true : false
            font.underline: true
            text:qsTr("    Output    ")
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: swipeView.setCurrentIndex(1)
        }
    }

    //horizontal line between heading and body
    Rectangle {
        id: headerSeperator
        width: parent.width
        height: 1
        color: "#828282"
        anchors.top: parent.top
        anchors.topMargin: 75
    }

    SwipeView {
        id: swipeView
        currentIndex: 0
        width: parent.width
        height: parent.height - 80
        anchors.top: headerSeperator.bottom
        anchors.topMargin: 10

        //input view
        Item {
            id: inputPage
            width: swipeView.width - 15
            height: swipeView.height - 2

            Rectangle {
                id: resourceInputBorder
                border.color: "#bbbbbb"
                border.width: 0.5
                width: parent.width - 15
                anchors.horizontalCenter: parent.horizontalCenter
                height: 130

                CSpinBox {
                    id: pageNrSpin
                    //textHeader: "# of Pages:"
                    //                    border.color: "#bbbbbb"
                    //                    border.width: 0.5
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.right: pageFramesSpin.left
                    width: 150
                    height: 130
                    textValue: pagingController.pageNr

                    onIncButtonPressed: {
                        pagingController.pageNr = pagingController.pageNr + 1
                    }

                    onDecButtonPressed: {
                        pagingController.pageNr = pagingController.pageNr - 1
                    }
                }

                CSpinBox {
                    id: pageFramesSpin
                    //textHeader: "# of Frames:"
                    //    border.color: "#bbbbbb"
                    //    border.width: 0.5
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: pageNrSpin.right
                    width: 150
                    height: 130
                    textValue: pagingController.pageFramesCount

                    onIncButtonPressed: {
                        pagingController.pageFramesCount = pagingController.pageFramesCount + 1
                    }

                    onDecButtonPressed: {
                        pagingController.pageFramesCount = pagingController.pageFramesCount - 1
                    }
                }

                CSpinBox {
                    id: pageInputSpin
                    //textHeader: "# of Acceses:"
                    //    border.color: "#bbbbbb"
                    //    border.width: 0.5
                    anchors.left: pageFramesSpin.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    width: 150
                    height: 130
                    textValue: pagingController.inputLength

                    onIncButtonPressed: {
                        pagingController.inputLength = pagingController.inputLength + 1
                    }

                    onDecButtonPressed: {
                        pagingController.inputLength = pagingController.inputLength - 1
                    }
                }
                GridLayout {
                    id: algoSelect
                    anchors.left: pageInputSpin.right
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    height: 130
                    anchors.leftMargin: 50
                    anchors.rightMargin: 50
                    anchors.topMargin: 20
                    anchors.bottomMargin: 20
                    rows: 2
                    columns: 2

                    AlgoSelectButton {            
                        text: "LIFO"
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        active: false
                        onToggled: {
                            //TODO:
                            active = !active
                        }
                    }
                    AlgoSelectButton {            
                        text: "LRU"
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        active: true
                        onToggled: {
                            //TODO:
                            active = !active
                        }
                    }
                    AlgoSelectButton {            
                        text: "Optimal"
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        active: true
                        onToggled: {
                            //TODO:
                            active = !active
                        }
                    }
                    AlgoSelectButton {            
                        text: "Clock"
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        active: true
                        onToggled: {
                            //TODO:
                            active = !active
                        }
                    }

                }

            }

            Item  {
                id: inputArea
                width: parent.width
                height: parent.height - 250
                anchors.top: resourceInputBorder.bottom
                anchors.topMargin: 15 
                anchors.left: parent.left
                anchors.horizontalCenter: parent.horizontalCenter

                ColumnLayout {
                    visible: true
                    width: parent.width
                    height: parent.height
                    anchors.horizontalCenter: parent.horizontalCenter
                    Layout.fillWidth: true

                    spacing: 40

                    Rectangle {
                        id: pageInputArea
                        Layout.alignment: Qt.AlignTop
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        GridLayout {
                            id:  inputGrid
                            anchors.top: pageInputArea.top
                            columns: 32
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            rowSpacing: 36
                            columnSpacing: 4
                            anchors.margins: 20
                            Repeater {
                                id: inputRepeater
                                model: pagingController.inputLength 

                                Rectangle {
                                    width: 50
                                    height: 50
                                    id: inputItem
                                    property var itemIndex: pagingController.input[index]
                                    color: inputListArea.containsMouse ? "steelblue" : colorArray[itemIndex % colorArray.length];
                                    border { width: 1; color: "black" }
                                    MouseArea {
                                        id: inputListArea
                                        anchors.fill: parent
                                        cursorShape: Qt.PointingHandCursor
                                        onClicked: {
                                        }
                                    }
                                    TextInput {
                                        id: textEdit
                                        text: parent.itemIndex
                                        anchors.centerIn: parent
                                        color: "#000000"
                                        font.family: "Ubuntu"
                                        font.pixelSize: 20
                                        inputMethodHints: Qt.ImhDigitsOnly
                                        validator: IntValidator { bottom: 1; top: pagingController.pageNr }
                                        Keys.onPressed: {
                                            if (event.key == Qt.Key_Tab) {
                                                event.accepted = true;
                                                textEdit.deselect();
                                                textEdit.focus = false;
                                                if( index + 1 < pagingController.inputLength && textEdit.acceptableInput) {
                                                    inputRepeater.itemAt(index + 1).children[1].selectAll();
                                                    inputRepeater.itemAt(index + 1).children[1].focus = true;
                                                } 
                                            }
                                        }
                                        onEditingFinished: {
                                            if(textEdit.acceptableInput) {
                                                pagingController.input[index] = textEdit.text;
                                            }
                                        }
                                    }
                                    Arrow {
                                        id: inputArrow
                                        anchors.top: parent.bottom
                                        visible: (index == iCurrentStep) ? true : false
                                    }
                                }
                            }
                        }
                    }

                    RowLayout {
                        id: pageFramesArea
                        Layout.fillWidth: true

                        Rectangle {
                            Layout.fillWidth: true
                            Layout.fillHeight: true

                            OwnText {
                                id: pageFramesHeading
                                text: qsTr("Page Frames")
                                font.pixelSize: 30
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.top: parent.top
                            }


                            ColumnLayout {
                                anchors.top: pageFramesHeading.bottom
                                anchors.topMargin: 15
                                anchors.horizontalCenter: parent.horizontalCenter
                                spacing: 5

                                RowLayout {
                                    Layout.alignment: Qt.AlignCenter
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true
                                    spacing: 20

                                    OwnText {
                                        id: frameHeading
                                        text: qsTr("Frame")
                                        font.pixelSize: 20
                                        Layout.preferredWidth: 60
                                    }

                                    Rectangle {
                                        id: headingBorder
                                        height: 60
                                        width: 2
                                        color: "#828282"
                                    }

                                    OwnText {
                                        id: currentPageHeading
                                        text: qsTr("Current")
                                        font.pixelSize: 20
                                            Layout.preferredWidth: 100
                                    }

                                    Rectangle {
                                        id: currentPageBorder
                                        height: 60
                                        width: 2
                                        color: "#828282"
                                    }

                                    OwnText {
                                        id: lastaccessheading
                                        text: qsTr("Last Access")
                                        font.pixelSize: 20
                                            Layout.preferredWidth: 100
                                    }
                                }

                                Repeater {

                                    model: pagingController.pageFramesCount
                                    RowLayout {

                                        Layout.fillWidth: true
                                        Layout.fillHeight: true
                                        spacing: 20

                                        OwnText {
                                            Layout.alignment: Qt.AlignLeft
                                            id: frameName
                                            text: qsTr(String.fromCharCode(65+ index))
                                            font.pixelSize: 20
                                        Layout.preferredWidth: 60
                                        }

                                        Rectangle {
                                            Layout.alignment: Qt.AlignLeft
                                            id: frameNameBorder
                                            height: 60
                                            width: 2
                                            color: "#828282"
                                        }

                                        CSpinBox {
                                            Layout.alignment: Qt.AlignLeft

                                            Layout.fillHeight: true
                                            id: frameValueSpin
                                            textValue: pagingController.getFrame(0, index).y

                                            Layout.preferredWidth: 100
                                            onIncButtonPressed: {
                                            }

                                            onDecButtonPressed: {
                                            }
                                        }

                                        Rectangle {
                                            Layout.alignment: Qt.AlignLeft
                                            id: frameValueBorder
                                            height: 60
                                            width: 2
                                            color: "#828282"
                                        }

                                        CSpinBox {
                                            Layout.alignment: Qt.AlignLeft
                                            id: lastAccessValue
                                            Layout.fillWidth: true
                                            //    border.color: "#bbbbbb"
                                            //    border.width: 0.5
                                            textValue: pagingController.getFrame(0, index).x

                                            Layout.preferredWidth: 100
                                            Layout.fillHeight: true
                                            onIncButtonPressed: {
                                            }

                                            onDecButtonPressed: {
                                            }
                                        }


                                    }
                                }
                            }
                        }
                    }

                }


            }//inputArea


            Rectangle {
                id: calcButton
                width: 200
                height: 40
                border.color: "#828282"
                border.width: 1
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 25
                anchors.right: parent.right
                anchors.rightMargin: 30

                OwnText {
                    anchors.centerIn: parent
                    text: qsTr("Calculate")
                    color: "#525252"
                    font.pixelSize: 16
                }

                MouseArea {
                    id: calcButtonMouse
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    hoverEnabled: true
                    onClicked: {
                        swipeView.incrementCurrentIndex()
                    }
                }
            }


            Rectangle {
                width: 160
                height: 35
                border.color: "#828282"
                border.width: 1
                anchors.bottom: randButton.top
                anchors.bottomMargin: 5
                anchors.left: parent.left
                anchors.leftMargin: 30

                OwnText {
                    anchors.centerIn: parent
                    text: qsTr("Reset Input")
                    color: "#525252"
                    font.pixelSize: 16
                }

                MouseArea {
                    id: clearButtonMouse
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    hoverEnabled: true
                    onClicked: {
                        controller.clearInput()
                        procView.model = 0
                        procView.model = controller.getNumOfProcs()
                    }
                }
            }



            //generate random button
            Rectangle {
                id: randButton
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 25
                anchors.left: parent.left
                anchors.leftMargin: 30
                border.color: "#828282"
                border.width: 1
                color: randButtonArea.containsMouse ? "#dddddd" : "#ffffff"
                width: 200
                height: 35

                Text {
                    text: qsTr("Random Processes")
                    anchors.centerIn: parent
                    color: "#525252"
                    font.family: "Ubuntu"
                    font.pixelSize: 16
                    //font.bold: true
                }

                MouseArea {
                    id: randButtonArea
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    hoverEnabled: true
                    onClicked: {
                        randButton.focus = true
                        randInputView.visible = true
                        randInputDialog.visible = true
                    }
                }

                Rectangle {
                    id: forceInterruptHelp
                    width: 14
                    height: 14
                    radius: 7
                    border.width: 1
                    border.color: "#828282"
                    anchors.right: randButton.right
                    anchors.rightMargin: 5
                    anchors.bottom: randButton.bottom
                    anchors.bottomMargin: randButton.height/2 - forceInterruptHelp.height/2

                    OwnText {
                        anchors.centerIn: parent
                        text: "?"
                        font.pixelSize: 8
                    }

                    MouseArea {
                        id: forceInterruptMouse
                        anchors.fill: parent
                        hoverEnabled: true
                    }

                    Hint {
                        width: 300
                        height: 20
                        anchors.bottom: forceInterruptHelp.top
                        //anchors.right: forceInterruptHelp.right
                        anchors.horizontalCenter: forceInterruptHelp.horizontalCenter
                        text: "Generating input data, which leads to 1 safe sequence"
                        visible: forceInterruptMouse.containsMouse
                    }
                }
            }

            Rectangle {
                id: errorMessage
                height: parent.height/4 + 20
                width: parent.width
                color: "#E53935"
                anchors.verticalCenter: parent.verticalCenter
                visible: controller.isErrorActive()

                Connections {
                    target: controller
                    onDataChanged: {
                        errorMessage.visible = controller.isErrorActive()
                        errorHeading.text = controller.getErrorHeading()
                        errorDescription.text = controller.getErrorDescription()
                    }
                }


                Text {
                    id: errorHeading
                    color: "#ffffff"
                    text: "Error"
                    font.family: "Ubuntu"
                    font.bold: true
                    font.pixelSize: 26
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.topMargin: 20
                }

                Rectangle {
                    height: 1
                    border.width: 4
                    border.color: "#ffffff"
                    anchors.left: errorHeading.left
                    anchors.right: errorHeading.right
                    anchors.top: errorHeading.bottom
                    anchors.topMargin: 2
                }

                Rectangle {
                    id: descriptionBox
                    width: parent.width - 60
                    height: parent.height/3
                    anchors.top: errorHeading.bottom
                    anchors.topMargin: 10
                    anchors.left: parent.left
                    anchors.leftMargin: 30
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#E53935"
                }

                Text {
                    id: errorDescription
                    color: "#ffffff"
                    text: "If you see this text, something went wrong!"
                    font.family: "Ubuntu"
                    font.pixelSize: 16
                    anchors.verticalCenter: descriptionBox.verticalCenter
                    anchors.left: descriptionBox.left
                    anchors.leftMargin: 10
                    anchors.right: descriptionBox.right
                    anchors.rightMargin: 10
                }

                Rectangle {
                    id: errorButton
                    height: 30
                    width: 80
                    color: errorButtonMouse.containsMouse ? "#ffffff" : "#E53935"
                    border.width: 1
                    border.color: "#ffffff"
                    anchors.top: descriptionBox.bottom
                    anchors.topMargin: 5
                    anchors.right: descriptionBox.right

                    Text {
                        text: "OK"
                        anchors.centerIn: parent
                        color: errorButtonMouse.containsMouse ? "#E53935" : "#ffffff"
                        font.family: "Ubuntu"
                        font.pixelSize: 14
                    }

                    MouseArea {
                        id: errorButtonMouse
                        anchors.fill: parent
                        hoverEnabled: true
                        cursorShape: Qt.PointingHandCursor
                        onClicked: {
                            controller.setError(false)
                            errorMessage.visible = false
                        }
                    }
                }
            }
        }//inputPage

        Item {
            id: outputPage

            Rectangle {
                id: inputPostView
                width: parent.width
                height: parent.height
                anchors.fill: parent
                anchors.margins: 10
                border.color: "#828282"
                border.width: 1

                PagingOutputView {
                    id: pagingOutputView
                    anchors.fill: parent
                }

            }//inputPostView
        } //outputPage
    }//swipeView
}
