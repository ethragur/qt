#pragma once


#include "pagingalgo.h"
#include "pageframe.h"
#include <QVector>


class LIFOAlgorithm : public PagingAlgorithm
{

	public:
		LIFOAlgorithm(int pageFramesCount, const QVector<int> & input); 
		void solve();
};


class LRUAlgorithm : public PagingAlgorithm
{

	public:
		LRUAlgorithm(int pageFramesCount, const QVector<int> & input); 
		void solve();
};


class ClockAlgorithm : public PagingAlgorithm
{

	public:
		ClockAlgorithm(int pageFramesCount, const QVector<int> & input); 
		void solve();
		
		//Overwrite those 2 functions 
		void reset(int pageFramesCount, const QVector<int> & input) ;
		int getPointerPos(int step) const ;
	protected:
		//save all the old clock pos for UI
		QVector<int> m_clock_pos;
	private:
		// stores the position the clock pointer is pointing at
		int clock_pos;
};


class OptimalAlgorithm : public PagingAlgorithm
{

	public:
		OptimalAlgorithm(int pageFramesCount, const QVector<int> & input); 
		void solve();
};
