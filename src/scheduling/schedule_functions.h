/*
 * Author: Robert Schaffenrath
 * Supervisor: Dr. Thomas Fahringer
 *
 * University of Innsbruck, Institute of Computer Science, http://dps.uibk.ac.at
 */

#ifndef SCHEDULE_FUNCTIONS_H
#define SCHEDULE_FUNCTIONS_H

#include "process.h"
#include "event_list.h"
#include "scheduling.h"

unsigned short find_event_timestamp(process_list_t *proc_list, event_list_head_tail_t *event_list);
event_list_head_tail_t *add_equal_insertion_proc (process_list_t *proc_list, event_list_head_tail_t *event_list,
                                       short *copied_list);
event_list_head_tail_t *add_event_list_node (process_list_t *proc_list, event_list_head_tail_t *event_list,
                             unsigned short new_position);
void interrupt_running_process (event_list_head_tail_t **event_list);
process_list_t *find_shortest (process_list_t *process_list);
process_list_t *find_highest_priority (process_list_t *process_list);
process_list_t *find_lottery_winner(process_list_t *process_list, unsigned short total_tickets);
process_list_t *find_different_lottery_winner(process_list_t *process_list, unsigned short total_tickets, unsigned short avoid_id);
process_list_t *find_most_tickets(process_list_t *process_list, unsigned short total_tickets);
process_list_t *find_different_most_tickets(process_list_t *process_list, unsigned short total_tickets, unsigned short avoid_id);
void update_timestamp (event_list_head_tail_t **event_list);
short check_tickets(process_list_t *proc_list_head);
short did_use_quantum(event_list_head_tail_t *event_list, unsigned short quantum);

#endif
