/*
 * Author: Robert Schaffenrath
 * Supervisor: Dr. Thomas Fahringer
 *
 * University of Innsbruck, Institute of Computer Science, http://dps.uibk.ac.at
 */

#ifndef GENERATE_SCHEDULING_H
#define GENERATE_SCHEDULING_H

#include "process.h"

typedef struct generation_process_queue
{
    std::vector<process_t> process_queue;
    unsigned short timestamp = 0;
    unsigned short prev_timestamp = 0;
    process_t running_process;
}generation_process_queue_t;

void check_sjf(std::vector<process_t> *proc_list, generation_process_queue_t *queue, process_t *new_process, std::vector<unsigned short> *conflict_values);
void check_srtf(std::vector<process_t> *proc_list, generation_process_queue_t *queue, process_t *new_process, std::vector<unsigned short> *conflict_values);
void check_rr(std::vector<process_t> *proc_list, generation_process_queue_t *queue, const unsigned short  quantum,
                        std::vector<unsigned short> *conflict_values);
void check_ps_non(std::vector<process_t> *proc_list, generation_process_queue_t *queue, process_t *new_process,
                            std::vector<unsigned short> *conflict_values);
void check_ps_pre(std::vector<process_t> *proc_list, generation_process_queue_t *queue, process_t *new_process,
                            std::vector<unsigned short> *conflict_values);
void check_ls(std::vector<process_t> *proc_list, generation_process_queue_t *queue,
              std::vector<unsigned short> *conflict_ls, unsigned short quantum);
#endif
