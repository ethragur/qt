/*
 * Author: Robert Schaffenrath
 * Supervisor: Dr. Thomas Fahringer
 *
 * University of Innsbruck, Institute of Computer Science, http://dps.uibk.ac.at
 */

#ifndef PROCESSCONTROLLER_H
#define PROCESSCONTROLLER_H

#include <QObject>
#include <QVector>
#include <QMap>

#include "src/scheduling/process.h"
#include "src/scheduling/event_list.h"
#include "src/scheduling/scheduling.h"
#include "src/scheduling/schedule_functions.h"
#include "src/scheduling/generate_processes.h"
#include "src/scheduling/generate_scheduling.h"
#include "src/scheduling/metric.h"

#define SCHEDULE_NUM 8

struct event_gap_list_t
{
    int timestamp;
    bool existsRunningProc;
    process_t *runningProc;
    QVector<process_t*> event_list;
};

struct proc_result_t
{
    int scheduleAlgorithm;
    QVector<event_list_t> resultList;
    QVector<event_gap_list_t> eventList;
    QVector<proc_metric_t> procMetric;
    float avg_turnaround_time;
    float avg_response_time;
    float avg_wait_time;
};

class ProcessController : public QObject
{
    Q_OBJECT
public:


    explicit ProcessController(QObject *parent = nullptr);

    QVector<process_t> getProcList() const;
    QVector<proc_result_t> getProcResultList() const;

    bool setProcAt(int index, const process_t &proc);

signals:
    void preProcAppend();
    void postProcAppend();

    void preProcRemove(int index);
    void postProcRemove();

    void preClearList();
    void postClearList();

    void dataChanged();

public slots:
    void appendProc();
    void removeProc(int index);
    void resetInternalData() {procList.clear();}

    QString getCharId(unsigned short id);
    void setAllButtons();
    void clearAllButtons();
    void changeButtonState(int algorithm);
    bool getButtonState(int algorithm);
    int  getMaxTime();
    int getProcAmountAtQueue();
    int getProcIdFromQueueAt(int index);
    int  getProcAmountAtPos(int index);
    int  getProcId(int positionIndex, int itemIndex);
    QString getColorById(int index);
    void assignColor(int index, QString color);
    int getRRQuantum();
    void setRRQuantum(int quantum);
    void setLSQuantum(int quantum);
    int getLSQuantum();

    void calcScheduling();
    int getNumScheduleResults();
    QString getScheduleType(int index);
    bool isLS(int index);
    bool isLST(int index);
    bool isForceInterruption();
    void setFoceInterruption(bool state);
    int getMaxSchedulePos(int index);
    int getProcAmountAtResultPos(int scheduleIndex, int posIndex);
    int getProcIdAtResultPos(int scheduleIndex, int posIndex);
    void clearProcResultList();
    void clearProcList();
    bool isSchedulingValid();
    QString getSchedulingErrorMessage();
    void setClickPosition(float pos);
    float getClickPosition();

    void generateRandomList();
    QString getAvgMetric(int metricType, int scheduleIndex);
    void setMetricAttributes(int scheduleIndex, int id, int pos);
    int getMetricId();
    int getMetricPos();
    int getMetricResponse();
    int getMetricWait();
    int getMetricTurnaround();
    int getMetricAt(int index);
    int getNumQueue();

    int getGenerateAmount();
    int getGenerateMax();
    int getGenerateLSQuantum();
    int getGenerateRRQuantum();
    int getGenerateAlternatives();
    bool getGenerateIncludePS();
    void setGenerateAmount(int amount);
    void setGenerateMax(int max);
    void setGenerateLSQuantum(int quantum);
    void setGenerateRRQuantum(int quantum);
    void setGenerateAlternatives(int alternatives);
    void setGenerateIncludePS(bool status);
    bool isGenerateAttributesValid();
    QString getGenerateErrorMessage();
    bool isGeneratedListValid();
    bool isSingleSolution();
    void setSingleSolution(bool state);

    bool isErrorActive();
    QString getErrorHeading();
    QString getErrorDescription();
    void setError(bool state);
    void setErrorHeading(QString heading);
    void setErrorDescription(QString description);


private:
    QVector<process_t> procList;
    QVector<process_t> calcList;
    QVector<proc_result_t> procResultList;
    QVector<QString> colorPalett;
    QMap<unsigned short, QString> colorMap;
    bool selectedAlgorithms[SCHEDULE_NUM] = {false};
    int alternativeSolutions;
    int generateAttributes[5] = {0};
    bool singleSolution = false;
    bool generateIncludePS = false;
    bool forceInterruption = false;
    float clickPosition = 0.0f;
    int rrQuantum = 0;
    int lsQuantum = 0;
    int metricId = -1;
    int metricPos = -1;
    int metricScheduleIdx = -1;
    bool errorActive = false;
    QString errorHeading;
    QString errorDescription;
};

#endif // PROCESSCONTROLLER_H
