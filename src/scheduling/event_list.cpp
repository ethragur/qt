#include <cstdlib>
#include "event_list.h"


/*
 *  This file contains the functions to handle the event_list. The event_list should represent a event_list from a real process schedular, where
 *  one node represents the state of the event_list for a given time. All nodes are part of a double linked list.
 */


void event_list_append(event_list_head_tail_t **event_list, process_list_t *process_list)
{
    event_list_head_tail_t *new_event_list = *event_list;
    event_list_t *new_node = (event_list_t*) malloc(sizeof(event_list_t));
    new_node->process_queue = process_list;
    new_node->timestamp = 0;
    new_node->running_process = NULL;
    new_node->proc_list_pointer = NULL;
    new_node->next = NULL;
    new_node->prev = NULL;

    if (new_event_list->tail == NULL)
    {
        new_node->next = NULL;
        new_node->prev = NULL;
        new_event_list->head = new_node;
        new_event_list->tail = new_node;
        *event_list = new_event_list;
    }
    else
    {
        new_event_list->tail->next = new_node;
        new_node->prev = new_event_list->tail;
        new_event_list->tail = new_node;
        *event_list = new_event_list;
    }
}


event_list_t *event_list_copy_last_node(event_list_t *event_list)
{
    if (event_list == NULL)
        return NULL;

    event_list_t *new_node = (event_list_t*) malloc(sizeof(event_list_t));
    new_node->timestamp = event_list->timestamp;
    new_node->total_tickets = event_list->total_tickets;
    new_node->running_process = event_list->running_process;
    new_node->proc_list_pointer = event_list->proc_list_pointer;
    new_node->process_queue = proc_copy(event_list->process_queue, &new_node->running_process);
    new_node->prev = event_list;
    new_node->next = NULL;
    event_list->next = new_node;

    return new_node;
}


void event_list_clear(event_list_head_tail_t *event_list)
{
    if (event_list == NULL)
        return;

    if (event_list->head->next == NULL)
    {
        proc_clear(event_list->head->process_queue);
        free(event_list->head);
        return;
    }

    event_list->head = event_list->head->next;

    while (event_list->head->next != NULL)
    {
        proc_clear(event_list->head->prev->process_queue);
        free(event_list->head->prev);
        event_list->head = event_list->head->next;
    }

    proc_clear(event_list->head->prev->process_queue);
    free(event_list->head->prev);

    proc_clear(event_list->head->process_queue);
    free(event_list->head);
}
