import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import Process 1.0

ListView {
    id: processResultView
    width: parent.width
    height: 800
    anchors.topMargin: 5
    clip: true

    property int itemHeight : processController.getProcAmountAtQueue() * 22 + 160

    Connections {
        target: calcButton
        onClicked: {
            processResultView.model = processController.getNumScheduleResults()
            processResultView.itemHeight = processController.getProcAmountAtQueue() * 22 + 160
        }
    }

    signal clicked()

    model: processController.getNumScheduleResults()

    spacing: 10
    delegate:
        Item {
        id: resultItem2
        width: processResultView.width
        height: processResultView.itemHeight

        Rectangle {
            id: resultArea
            property int scheduleIndex: index
            width: parent.width - 20
            height:parent.height - 50
            anchors.horizontalCenter: parent.horizontalCenter
            border.color: "#828282"
            border.width: 1

            Text {
                id: scheduleHeading
                text: processController.getScheduleType(index)
                anchors.horizontalCenter: parent.horizontalCenter
                font.family: "Ubuntu"
                font.pixelSize: 20
                color: "#525252"
                anchors.top: parent.top
                anchors.topMargin: 5
            }

            Rectangle {
                id: lsHelp
                width: 14
                height: 14
                radius: 7
                border.width: 1
                border.color: "#828282"
                anchors.left: scheduleHeading.right
                anchors.leftMargin: 2
                anchors.top: scheduleHeading.top
                anchors.topMargin: 5
                visible: processController.isLS(index)

                Text {
                    id: lsHelpText
                    anchors.centerIn: parent
                    text: "?"
                    font.pixelSize: 8
                    color: "#828282"
                }

                MouseArea {
                    id: lsHelpMouse
                    anchors.fill: parent
                    hoverEnabled: true
                }

                Hint {
                    width: 280
                    height: 20
                    anchors.bottom: lsHelp.bottom
                    anchors.bottomMargin: -2
                    anchors.left: lsHelpText.right
                    anchors.leftMargin: 5
                    text: processController.isLST(index) ? "Process selection by amount of tickets" : "Process selection by random value"
                    visible: lsHelpMouse.containsMouse
                }
            }

            // Diagram Ids
            Rectangle {
                id: idsBorder
                height: processController.getProcAmountAtQueue() * 20 + 2 * processController.getProcAmountAtQueue()
                width: 50
                anchors.top: resultArea.top
                anchors.topMargin: 50
                anchors.left: resultArea.left
                anchors.leftMargin: 5
                //border.color: "#888888"
                //border.width: 2

                ListView {
                    id: diagramIds
                    anchors.fill: parent


                    clip: true
                    orientation: Qt.Vertical

                    Connections {
                        target: calcButton
                        onClicked: {
                            idsBorder.height = processController.getProcAmountAtQueue() * 20 + 2 * processController.getProcAmountAtQueue()
                            diagramIds.model = processController.getProcAmountAtQueue()

                        }
                    }

                    model: processController.getProcAmountAtQueue()
                    spacing: 2
                    delegate:

                        Rectangle {
                        id: idWindow
                        property int rowId: processController.getProcIdFromQueueAt(index)
                        width: diagramIds.width
                        height: 20
                        color: processController.getColorById(rowId)

                        OwnText {
                            anchors.centerIn: parent
                            text: "P"+idWindow.rowId
                            color: "#ffffff"
                        }
                    }
                }
            }

                ListView {
                    id: diagramData
                    implicitHeight: processController.getProcAmountAtQueue() * 20 + 2 * processController.getProcAmountAtQueue() + 66
                    implicitWidth: parent.width - 70
                    anchors.top: idsBorder.top
                    anchors.left: idsBorder.right
                    anchors.leftMargin: 10
                    clip: true
                    orientation: Qt.Horizontal

                    Connections {
                        target: calcButton
                        onClicked: {
                            diagramData.implicitHeight = processController.getProcAmountAtQueue() * 20 + 2 * processController.getProcAmountAtQueue() + 66
                            diagramData.model = processController.getMaxSchedulePos(resultArea.scheduleIndex)
                        }
                    }

                    model: processController.getMaxSchedulePos(resultArea.scheduleIndex)
                    delegate:
                        //time slice
                        Rectangle {
                        id: processFrame
                        property int processIndex: index
                        width: 40
                        height: parent.height - 50
                        //border.color: "#828282"

                        ListView {
                            id: diagramPos
                            anchors.fill: parent
                            clip: true
                            orientation: Qt.Vertical

                            model: processController.getProcAmountAtQueue()
                            delegate:
                                Rectangle {
                                id: singleFrame
                                width: 40
                                height: 22
                                border.color: processFrame.processIndex !== processController.getMaxSchedulePos(resultArea.scheduleIndex)-1 ? "#828282" : "#ffffff"
                                border.width:  1
                                color: processController.getProcAmountAtResultPos(resultArea.scheduleIndex, processFrame.processIndex) && processController.getProcIdAtResultPos(resultArea.scheduleIndex, processFrame.processIndex) === index ? processController.getColorById(index) : "#ffffff"

                                MouseArea {
                                    id: singleFrameMouse
                                    anchors.fill: parent
                                    cursorShape:processController.getProcIdAtResultPos(resultArea.scheduleIndex, processFrame.processIndex) === index ? Qt.PointingHandCursor : Qt.ArrowCursor
                                    enabled: processController.getProcIdAtResultPos(resultArea.scheduleIndex, processFrame.processIndex) === index
                                    onClicked: {
                                        processController.setMetricAttributes(resultArea.scheduleIndex, processController.getProcIdAtResultPos(resultArea.scheduleIndex, processFrame.processIndex), processFrame.processIndex)
                                        processController.setClickPosition(resultItem.y)
                                        processResultView.clicked()
                                    }
                                }
                            }
                        }
                        OwnText {
                            text: index
                            color: "#323232"
                            font.pixelSize: 10
                            anchors.top: parent.bottom
                            anchors.topMargin: -15
                            anchors.left: processFrame.left
                        }

                    }

                    ScrollBar.horizontal: ScrollBar {
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 1
                        policy: processController.getMaxSchedulePos(resultArea.scheduleIndex) * 40 <parent.width ? ScrollBar.AlwaysOff : ScrollBar.AlwaysOn
                    }

                }
        }

        //Metrics
        Item {
            id: metricColumn1
            width: resultArea.width/3
            height: 20
            anchors.bottom: resultArea.bottom
            anchors.bottomMargin: 5

            Connections {
                target: calcButton
                onClicked: {
                    avgResponseTime.text = "Ø" + qsTr(" Response time: ") + processController.getAvgMetric(ProcessModel.RESPONSE_TIME, resultArea.scheduleIndex)
                    avgWaitTime.text = "Ø" + qsTr(" Waiting time: ") + processController.getAvgMetric(ProcessModel.WAIT_TIME, resultArea.scheduleIndex)
                    avgTurnaroundTime.text = "Ø" + qsTr(" Turnaround time: ") + processController.getAvgMetric(ProcessModel.TURNAROUND_TIME, resultArea.scheduleIndex)
                }
            }

            Text {
                id: avgResponseTime
                text: "Ø" + qsTr(" Response time: ") + processController.getAvgMetric(ProcessModel.RESPONSE_TIME, resultArea.scheduleIndex)
                color: "#525252"
                font.family: "Ubuntu"
                font.pixelSize: 13
                anchors.centerIn: parent
            }

            Rectangle {
                id: responseHelp
                width: 14
                height: 14
                radius: 7
                border.width: 1
                border.color: "#828282"
                anchors.left: avgResponseTime.right
                anchors.leftMargin: 5
                anchors.bottom: avgResponseTime.bottom
                anchors.bottomMargin: 1

                Text {
                    anchors.centerIn: parent
                    text: "?"
                    font.pixelSize: 8
                    color: "#828282"
                }

                MouseArea {
                    id: responseHelpMouse
                    anchors.fill: parent
                    hoverEnabled: true
                }

                Hint {
                    width: 250
                    height: 20
                    anchors.bottom:  responseHelp.top
                    anchors.horizontalCenter: responseHelp.horizontalCenter
                    text: "Average time from arrival until start"
                    visible: responseHelpMouse.containsMouse
                }
            }

        }

        Item {
            id: metricColumn2
            width: resultArea.width/3
            height: 20
            anchors.bottom: resultArea.bottom
            anchors.bottomMargin: 5
            anchors.left: metricColumn1.right

            Text {
                id: avgWaitTime
                text: "Ø" + qsTr(" Waiting time: ") + processController.getAvgMetric(ProcessModel.WAIT_TIME, resultArea.scheduleIndex)
                color: "#525252"
                font.family: "Ubuntu"
                font.pixelSize: 13
                anchors.centerIn: parent
            }

            Rectangle {
                id: waitHelp
                width: 14
                height: 14
                radius: 7
                border.width: 1
                border.color: "#828282"
                anchors.left: avgWaitTime.right
                anchors.leftMargin: 5
                anchors.bottom: avgWaitTime.bottom
                anchors.bottomMargin: 1

                Text {
                    anchors.centerIn: parent
                    text: "?"
                    font.pixelSize: 8
                    color: "#828282"
                }

                MouseArea {
                    id: waitHelpMouse
                    anchors.fill: parent
                    hoverEnabled: true
                }

                Hint {
                    width: 200
                    height: 20
                    anchors.bottom:  waitHelp.top
                    anchors.horizontalCenter: waitHelp.horizontalCenter
                    text: "Average time in the queue"
                    visible: waitHelpMouse.containsMouse
                }
            }
        }

        Item {
            id: metricColumn3
            width: resultArea.width/3
            height: 20
            anchors.bottom: resultArea.bottom
            anchors.bottomMargin: 5
            anchors.left: metricColumn2.right

            Text {
                id: avgTurnaroundTime
                text: "Ø" + qsTr(" Turnaround time: ") + processController.getAvgMetric(ProcessModel.TURNAROUND_TIME, resultArea.scheduleIndex)
                color: "#525252"
                font.family: "Ubuntu"
                font.pixelSize: 13
                anchors.centerIn: parent
            }

            Rectangle {
                id: turnaroundHelp
                width: 14
                height: 14
                radius: 7
                border.width: 1
                border.color: "#828282"
                anchors.left: avgTurnaroundTime.right
                anchors.leftMargin: 5
                anchors.bottom: avgTurnaroundTime.bottom
                anchors.bottomMargin: 1

                Text {
                    anchors.centerIn: parent
                    text: "?"
                    font.pixelSize: 8
                    color: "#828282"
                }

                MouseArea {
                    id: turnaroundHelpMouse
                    anchors.fill: parent
                    hoverEnabled: true
                }

                Hint {
                    width: 250
                    height: 20
                    anchors.bottom:  turnaroundHelp.top
                    anchors.right: parent.right
                    text: "Average time from arrival until completion"
                    visible: turnaroundHelpMouse.containsMouse
                }
            }
        }
    }
    ScrollBar.vertical: ScrollBar {
        policy: ScrollBar.AlwaysOn
    }
}
