#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "src/scheduling/processcontroller.h"
#include "src/scheduling/processmodel.h"
#include "src/deadlock/controller.h"
#include "src/paging/pagingcontroller.h"
#include "src/paging/pageframe.h"
#include "src/allocation/allocationcontroller.h"

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    qmlRegisterType<ProcessModel>("Process", 1, 0, "ProcessModel");
    qmlRegisterType<PagingController>("Paging", 1, 0, "PagingController");
    qmlRegisterType<AllocationController>("Allocation", 1, 0, "AllocationController");
    qmlRegisterUncreatableType<ProcessController>("Process", 1, 0, "ProcessController",
                                                  QStringLiteral("ProcessController should not be created in QML"));    

    ProcessController processController;
    PagingController  pagingController;
    AllocationController allocationController;
    Controller controller;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(QStringLiteral("processController"), &processController);
    engine.rootContext()->setContextProperty(QStringLiteral("controller"), &controller);
    engine.rootContext()->setContextProperty(QStringLiteral("pagingController"), &pagingController);
    engine.rootContext()->setContextProperty(QStringLiteral("allocationController"), &allocationController);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
