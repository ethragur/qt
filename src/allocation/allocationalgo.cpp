
#include "allocationalgo.h"

#include <QDebug>

FirstFitAlgo::FirstFitAlgo(const int memorySize, const QVector<int> & memoryList, const QVector<QPair<int,int>> & input) :
	AllocationAlgo(memorySize,memoryList,input)
{ }

void FirstFitAlgo::solve()
{
	for(const auto & newBlock : m_inputList)
	{
		for(int i = 0; i <= m_memorySize-newBlock.first; i++)
		{
			if(m_memoryList[i] != 0)
				continue;
			else
			{
				bool fit = true;
				for(int j = 0; j < newBlock.first; j++)
				{
					if(m_memoryList[i+j] != 0)
					{
						i = i+j;
						fit = false;
						break;
					}
				}

				if(fit)
				{
					for(int j = 0; j < newBlock.first; j++)
					{
						m_memoryList[i+j]  = newBlock.second;
					}
				}
			}
		}
	}
}

NextFitAlgo::NextFitAlgo(const int memorySize, const QVector<int> & memoryList, const QVector<QPair<int,int>> & input) :
	AllocationAlgo(memorySize,memoryList, input)
{
	m_pointerPos = 0;
}

void NextFitAlgo::solve()
{
	for(const auto & newBlock : m_inputList)
	{
		for(int i = m_pointerPos; (i + m_pointerPos) <= (m_memorySize + m_pointerPos - newBlock.first)  ; i = (i+1) % m_memorySize)
		{
			if(m_memoryList[i] != 0)
				continue;
			else
			{
				bool fit = true;
				for(int j = 0; j < newBlock.first; j++)
				{
					if(m_memoryList[i+j] != 0)
					{
						i = i+j;
						fit = false;
						break;
					}
				}

				if(fit)
				{
					m_pointerPos = i;
					for(int j = 0; j < newBlock.first; j++)
					{
						m_memoryList[i+j]  = newBlock.second;
					}

					m_pointerPos += newBlock.first;
				}
			}
		}
	}
}

BestFitAlgo::BestFitAlgo(const int memorySize, const QVector<int> & memoryList, const QVector<QPair<int,int>> & input) :
	AllocationAlgo(memorySize,memoryList, input)
{ }

void BestFitAlgo::solve()
{
	for(const auto & newBlock : m_inputList)
	{
		int min_free = INT_MAX;
		int min_free_loc = -1;
		for(int i = 0; i <= m_memorySize - newBlock.first; i++)
		{
			if(m_memoryList[i] != 0)
				continue;
			else
			{
				int j;	
				for(j = 0; j <= m_memorySize; j++)
				{
					if(m_memoryList[i+j] != 0)
					{
						i = i+j;
						break;
					}
				}

				if(j == newBlock.first)
				{
					//perfect fit, no need to search any further
					min_free = j;
					min_free_loc = i -j;
					break;
				}
				else if((j >= newBlock.first && j < min_free))
				{
					//we found a block that is smaller than our current min_size
					min_free = j;
					min_free_loc = i -j;
				}
			}
		}

		if( min_free_loc != -1 )
		{
			for(int j = min_free_loc; j < min_free_loc + newBlock.first; j++)
			{
				m_memoryList[j]  = newBlock.second;
			}
		}
	}
}

WorstFitAlgo::WorstFitAlgo(const int memorySize, const QVector<int> & memoryList, const QVector<QPair<int,int>> & input) :
	AllocationAlgo(memorySize,memoryList, input)
{ }

void WorstFitAlgo::solve()
{
	for(const auto & newBlock : m_inputList)
	{
		int max_free = newBlock.first;
		int max_free_loc = -1;
		for(int i = 0; i <= m_memorySize - newBlock.first; i++)
		{
			if(m_memoryList[i] != 0)
				continue;
			else
			{
				int j;
				for(j = 0; j <= m_memorySize; j++)
				{
					if(m_memoryList[i+j] != 0)
					{
						i = i+j;
						break;
					}
				}

				//if we don't have a block already update, otherwise only update if we found a bigger one
				if((j >= newBlock.first && (j > max_free || max_free_loc == -1)))
				{
					max_free = j;
					max_free_loc = i -j;
				}
			}
		}

		if( max_free_loc != -1 )
		{
			for(int j = max_free_loc; j < max_free_loc + newBlock.first; j++)
			{
				m_memoryList[j]  = newBlock.second;
			}
		}
	}
}

