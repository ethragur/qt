#include "pagingcontroller.h"

#include "paginglifo.h"
#include <iostream>
#include <random>
#include <QDebug>


PagingController::PagingController(QObject* parent)
	: QObject(parent)
{
	m_inputLength = 32;
	m_pageFramesCount = 3;
	m_pageNr = 8;
	m_pagingAlgo = LIFO;
	m_input = QVector<int>(m_inputLength, 1);

	std::random_device device;
	std::mt19937 generator(device());
	std::uniform_int_distribution<int> distribution(1, m_pageNr);

	for (int i = 0; i < m_inputLength; i++) {
		m_input[i] = distribution(generator);
	}

	std::shared_ptr<PagingAlgorithm> lifo		= std::make_shared<LIFOAlgorithm>(LIFOAlgorithm(m_pageFramesCount, m_input ));
	std::shared_ptr<PagingAlgorithm> lru		= std::make_shared<LRUAlgorithm>(LRUAlgorithm(m_pageFramesCount, m_input ));
	std::shared_ptr<PagingAlgorithm> optimal	= std::make_shared<OptimalAlgorithm>(OptimalAlgorithm(m_pageFramesCount, m_input ));
	std::shared_ptr<PagingAlgorithm> clock		= std::make_shared<ClockAlgorithm>(ClockAlgorithm(m_pageFramesCount, m_input ));
	m_AlgoImplementation.insert(LIFO,	 AlgoPair(true, lifo));
	m_AlgoImplementation.insert(LRU,	 AlgoPair(true, lru));
	m_AlgoImplementation.insert(CLOCK,	 AlgoPair(true, clock));
	m_AlgoImplementation.insert(OPTIMAL, AlgoPair(true, optimal));

	emit inputChanged();
}

void PagingController::setInputLength(const int inputLength)
{
	if (inputLength <= 64) 
	{
		if (m_inputLength > inputLength && inputLength >= 0) 
		{
			m_input.removeLast();
		} 
		else 
		{
			std::random_device device;
			std::mt19937 generator(device());
			std::uniform_int_distribution<int> distribution(1, m_pageNr);
			m_input.append(distribution(generator));
		}

		resolveAll();

		m_inputLength = inputLength;
		emit inputLengthChanged();
		emit inputChanged();
	} 
	else 
	{
		this->setInputLength(64);
	}
}

void PagingController::setPageNr(const int pageNr)
{
	m_pageNr = pageNr;
	std::random_device device;
	std::mt19937 generator(device());
	std::uniform_int_distribution<int> distribution(1, m_pageNr);

	for (int i = 0; i < m_inputLength; i++) 
	{
		m_input[i] = distribution(generator);
	}

	resolveAll();

	emit inputLengthChanged();
	emit inputChanged();
	emit pageNrChanged();
	emit inputChanged();
}

void PagingController::setPageFramesCount(const int pageFramesCount)
{
	m_pageFramesCount = pageFramesCount;


	resolveAll();

	emit pageFramesCountChanged();
}

void PagingController::setPagingAlgo(const int pagingAlgo)
{
	m_pagingAlgo = (Algorithm)pagingAlgo;
	emit pagingAlgoChanged();
}

void PagingController::setInput(const QVector<int> input)
{
	m_input = input;

	resolveAll();

	emit inputChanged();
}


int PagingController::inputLength() const
{
	return m_inputLength;
}

int PagingController::pageNr() const
{
	return m_pageNr;
}

int PagingController::pageFramesCount() const
{
	return m_pageFramesCount;
}

int PagingController::pagingAlgo() const
{
	return (int)m_pagingAlgo;
}

QVector<int> PagingController::input() const
{
	return m_input;
}

/*
 *  Returns  the  page Frame with index  i  on step step
 * */
QPoint PagingController::getFrame(int step, int i)
{
	return getFrame(step, i, (int)m_pagingAlgo);
}

/*
 *  Returns  the  page Frame with index  i  on step step
 * */
QPoint PagingController::getFrame(int step, int i, int algorithm)
{
	AlgoPair *pagingStruct = &m_AlgoImplementation.find((Algorithm)algorithm).value();
	if(pagingStruct->solve)
	{
		pagingStruct->pagingAlgorithm->reset(m_pageFramesCount, m_input );
		pagingStruct->pagingAlgorithm->solve();
		pagingStruct->solve = false;
	}
	return pagingStruct->pagingAlgorithm->getPageFrames()[step].getPair(i);
}

int PagingController::getPageFaultsCount()
{
	return getPageFaultsCount((int)m_pagingAlgo);
}

int PagingController::getPageFaultsCount(int algorithm)
{
	AlgoPair *pagingStruct = &m_AlgoImplementation.find((Algorithm)algorithm).value();
	if(pagingStruct->solve)
	{
		pagingStruct->pagingAlgorithm->reset(m_pageFramesCount, m_input );
		pagingStruct->pagingAlgorithm->solve();
		pagingStruct->solve = false;
	}
	qDebug() << "Page Fault counts for algo: " << algorithm << " " << m_AlgoImplementation.find((Algorithm)algorithm).value().pagingAlgorithm->getPageFaultsCount();
	return m_AlgoImplementation.find((Algorithm)algorithm).value().pagingAlgorithm->getPageFaultsCount();
}

/*
 *  Removes the last Step (prevStep Button)
 * */
void PagingController::prevStep()
{
}

/*
 * Compares pageFrame from step and step -1 to get the pagefault
 * TODO: when variables change Page Faults do not update in frontend
 * */
QVector<int> PagingController::getPageFault(int step) 
{
	return getPageFault(step, (int)m_pagingAlgo);
}

/*
 * Compares pageFrame from step and step -1 to get the pagefault
 * TODO: when variables change Page Faults do not update in frontend
 * */
QVector<int> PagingController::getPageFault(int step, int algorithm) 
{
	AlgoPair *pagingStruct = &m_AlgoImplementation.find((Algorithm)algorithm).value();
	if(pagingStruct->solve)
	{
		pagingStruct->pagingAlgorithm->reset(m_pageFramesCount, m_input );
		pagingStruct->pagingAlgorithm->solve();
		pagingStruct->solve = false;
	}
	return pagingStruct->pagingAlgorithm->getPageFaults()[step];
}

/*
 *	Returns the position of the pointer for the clock algorithm
 *
 * */
int PagingController::getPointerPos(int step)
{
	return getPointerPos(step, (int)m_pagingAlgo);
}

/*
 *	Returns the position of the pointer for the clock algorithm
 *
 * */
int PagingController::getPointerPos(int step, int algorithm)
{
	if((Algorithm)algorithm != CLOCK)
	{
		return -1;
	}
	AlgoPair *pagingStruct = &m_AlgoImplementation.find(CLOCK).value();
	if(pagingStruct->solve)
	{
		//we haven't solved this algorithm yet
		return -1;
	}
	
	return pagingStruct->pagingAlgorithm->getPointerPos(step);
}

void PagingController::refresh()
{
	resolveAll();
	getFrame(0,0);
	getFrame(0,1);
	getFrame(0,2);
	getFrame(0,3);
	emit inputLengthChanged();
	emit inputChanged();
	emit pageNrChanged();
	emit inputChanged();

}

void PagingController::resolveAll()
{
	for(auto & s : m_AlgoImplementation)
	{
		s.solve = true;
	}
}
