import "./buttons"
import "./input"
import "./output"
import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0

import Process 1.0

Page {
    id: schedulingView

    Hint {
        text: "Main menu"
        width: 80
        height: 20
        anchors.bottom: menuButton.top
        anchors.horizontalCenter: menuButton.horizontalCenter
        visible: menuMouseArea.containsMouse
    }

    //Menu button
    Rectangle {
        id: menuButton
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 20

        width: 40
        height: 40
        border.color: "#828282"
        border.width: 2

        Rectangle {
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 5

            width: 13
            height: 13
            color: "#828282"
        }

        Rectangle {
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 5

            width: 13
            height: 13
            color: "#828282"
        }

        Rectangle {
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5

            width: 13
            height: 13
            color: "#828282"
        }

        Rectangle {
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5

            width: 13
            height: 13
            color: "#828282"
        }

        MouseArea {
            id: menuMouseArea
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            hoverEnabled: true
            onClicked: stackView.pop("Scheduling.qml")
        }
    }


    //Heading
    OwnText {
        id: header
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20
        font.pixelSize: 28
        font.bold: true
        font.underline: true
        text: qsTr("Process Scheduling")
    }

    //__Input__ site switcher
    Rectangle {
        id: inputSelection

        property bool onPage: swipeView.currentIndex == 0

        height: 30
        width: 100
        anchors.bottom: header.bottom
        anchors.bottomMargin: -20
        anchors.left: parent.left
        anchors.leftMargin: parent.width/4 - 50
        opacity: onPage ? 1 : 0.3

        OwnText {
            anchors.centerIn: parent
            font.pixelSize: 20
            font.bold: parent.onPage ? true : false
            font.underline: true
            text: qsTr("    Input    ")
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: swipeView.setCurrentIndex(0)
        }
    }

    //__Output__ site switcher
    Rectangle {
        id: outputSelection

        property bool onPage: swipeView.currentIndex == 1

        height: 30
        width: 100
        anchors.bottom: header.bottom
        anchors.bottomMargin: -20
        anchors.left: parent.left
        anchors.leftMargin: 3*(parent.width/4) - 50
        opacity: onPage ? 1 : 0.3

        OwnText {
            anchors.centerIn: parent
            font.pixelSize: 20
            font.bold: parent.onPage ? true : false
            font.underline: true
            text:qsTr("    Output    ")
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: swipeView.setCurrentIndex(1)
        }
    }

    //horizontal line between heading and body
    Rectangle {
        id: headerSeperator
        width: parent.width
        height: 1
        color: "#828282"
        anchors.top: parent.top
        anchors.topMargin: 75
    }

    //input view
    SwipeView {
        id: swipeView
        currentIndex: 0
        width: parent.width
        height: parent.height - 80
        anchors.top: headerSeperator.bottom
        anchors.topMargin: 10


        ScrollView {
            id: scrollView
            contentHeight: 450 + processInput.height
            clip: true

            signal triggered()

            property int numResults : 0

            onTriggered: {
                numResults = processController.getNumScheduleResults()
                scrollDown.start()
                arrowUp.visible = scrollView.ScrollBar.vertical.visible
            }

            ScrollBar.vertical.policy: (numResults != 0) || (scrollView.contentHeight > swipeView.height) ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff

            NumberAnimation {
                id: scrollDown
                target: scrollView.ScrollBar.vertical
                property: "position"
                to:((450 + processInput.height) / scrollView.contentHeight)
                duration: 400
            }


            Item {
                id: arrowUp
                width: 50
                height: 50
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 20
                anchors.right: parent.right
                visible: scrollView.ScrollBar.vertical.visible

                OwnText {
                    text: "↑"
                    font.pixelSize: 35
                    font.bold: true
                    anchors.centerIn: parent
                }

                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    cursorShape: Qt.PointingHandCursor
                    onClicked: scrollUp.start()
                }

                NumberAnimation {
                    id: scrollUp
                    target: scrollView.ScrollBar.vertical
                    property: "position"
                    to: 0
                    duration: 400
                }
            }


            Item {
                id: inputWithResultPage
                property bool showResult: false

                width: swipeView.width - 20
                height: swipeView.height - 2

                Item {
                    id: inputPage
                    width: swipeView.width - 15
                    height: swipeView.height - 2

                    // Schedule-Algorithms selection buttons
                    Rectangle {
                        id: typeSelectionBorder
                        border.color: "#bbbbbb"
                        border.width: 0.5
                        width: parent.width - 30
                        height: 130
                        anchors.horizontalCenter: parent.horizontalCenter

                        GridLayout {
                            id: scheduleSelectionGrid
                            anchors.fill: parent
                            anchors.centerIn: parent
                            anchors.leftMargin: 10
                            anchors.topMargin: 5
                            rows: 3
                            columns: 3

                            SchedulingButton {
                                id: fcfsButton
                                Layout.preferredWidth: parent.width/3 - 10
                                Layout.preferredHeight: parent.height/4
                                text: "First-come First-served (FCFS)"
                                active: processController.getButtonState(ProcessModel.FCFS)
                                onToggled: {
                                    processController.changeButtonState(ProcessModel.FCFS)
                                    active = processController.getButtonState(ProcessModel.FCFS)
                                }
                            }

                            SchedulingButton {
                                id: sjfButton
                                Layout.preferredWidth: parent.width/3 - 10
                                Layout.preferredHeight: parent.height/4
                                text: "Shortest Job First (SJF)"
                                active: processController.getButtonState(ProcessModel.SJF)
                                onToggled: {
                                    processController.changeButtonState(ProcessModel.SJF)
                                    active = processController.getButtonState(ProcessModel.SJF)
                                }
                            }

                            SchedulingButton {
                                id: srtfButton
                                Layout.preferredWidth: parent.width/3 - 10
                                Layout.preferredHeight: parent.height/4
                                text: "Shortest Remaining Time First (SRTF)"
                                active: processController.getButtonState(ProcessModel.SRTF)
                                onToggled: {
                                    processController.changeButtonState(ProcessModel.SRTF)
                                    active = processController.getButtonState(ProcessModel.SRTF)
                                }
                            }

                            SchedulingButton {
                                id: rrButton
                                Layout.preferredWidth: parent.width/3 - 10
                                Layout.preferredHeight: parent.height/4
                                text: "Round Robin (RR)"
                                active: processController.getButtonState(ProcessModel.RR)
                                onToggled: {
                                    processController.changeButtonState(ProcessModel.RR)
                                    active = processController.getButtonState(ProcessModel.RR)
                                    rrQuantumInput.visible = processController.getButtonState(ProcessModel.RR)
                                }
                            }

                            SchedulingButton {
                                id: psButton
                                property var psButtonState
                                Layout.preferredWidth: parent.width/3 - 10
                                Layout.preferredHeight: parent.height/4
                                text: "Priority Scheduling (PS)"
                                active: processController.getButtonState(ProcessModel.PSnon)
                                onToggled: {
                                    processController.changeButtonState(ProcessModel.PSnon)
                                    processController.changeButtonState(ProcessModel.PSpre)
                                    psButtonState = processController.getButtonState(ProcessModel.PSnon)
                                    active = psButtonState
                                    priorityHeading.visible = psButtonState
                                    processInput.psActive = psButtonState
                                }
                            }

                            SchedulingButton {
                                id: lsButton
                                property var lsButtonState
                                Layout.preferredWidth: parent.width/3 - 10
                                Layout.preferredHeight: parent.height/4
                                text: "Lottery Scheduling (LS)"
                                active: processController.getButtonState(ProcessModel.LS)
                                onToggled: {
                                    processController.changeButtonState(ProcessModel.LS)
                                    processController.changeButtonState(ProcessModel.LS_theoretical)
                                    lsButtonState = processController.getButtonState(ProcessModel.LS)
                                    active = lsButtonState
                                    removeHeading.activeLS = lsButtonState
                                    ticketsHeading.visible = lsButtonState
                                    quantumHeading.visible = lsButtonState
                                    processInput.lsActive = lsButtonState
                                    lsQuantumInput.visible = lsButtonState
                                    forceInterruptText.color = lsButtonState ? "#ffffff" : "#828282"
                                }

                                CheckBox {
                                    id: forceInterruptBox
                                    anchors.right: parent.right
                                    anchors.rightMargin: 25
                                    anchors.bottom: parent.bottom
                                    anchors.bottomMargin: 2
                                    indicator.width: 15
                                    indicator.height: 15
                                    checkState: processController.isForceInterruption() ? Qt.Checked : Qt.Unchecked
                                    onClicked: {
                                        processController.setFoceInterruption(forceInterruptBox.checked)
                                    }
                                }

                                Rectangle {
                                    id: forceInterruptHelp
                                    width: 14
                                    height: 14
                                    radius: 7
                                    border.width: 1
                                    border.color: "#828282"
                                    anchors.left: forceInterruptBox.right
                                    anchors.leftMargin: 1
                                    anchors.bottom: forceInterruptBox.bottom
                                    anchors.bottomMargin: 13

                                    OwnText {
                                        anchors.centerIn: parent
                                        text: "?"
                                        font.pixelSize: 8
                                    }

                                    MouseArea {
                                        id: forceInterruptMouse
                                        anchors.fill: parent
                                        hoverEnabled: true
                                    }

                                    Hint {
                                        width: 380
                                        height: 20
                                        anchors.bottom: forceInterruptHelp.top
                                        anchors.right: forceInterruptHelp.right
                                        text: "Select different running process after interruption, if possible"
                                        visible: forceInterruptMouse.containsMouse
                                    }
                                }

                                OwnText {
                                    id: forceInterruptText
                                    anchors.top: forceInterruptBox.bottom
                                    anchors.topMargin: -13
                                    anchors.right: forceInterruptBox.right
                                    anchors.rightMargin: -20
                                    text: qsTr("Force interruption")
                                    font.pixelSize: 10
                                    color: processController.getButtonState(ProcessModel.LS) ? "#ffffff" : "#828282"
                                }
                            }

                            //item for different positioning of select all/none buttons
                            Item {
                                Layout.columnSpan: 3
                                Layout.preferredWidth: parent.width
                                Layout.preferredHeight: parent.height/4

                                //Select All Button
                                SchedulingButton {
                                    id: allSchedulingButton
                                    width: parent.width/3 - 10
                                    height: parent.height
                                    anchors.left: parent.left
                                    anchors.leftMargin: parent.width/6
                                    text: qsTr("Select All")

                                    onToggled: {
                                        processController.setAllButtons()
                                        fcfsButton.active = processController.getButtonState(ProcessModel.FCFS)
                                        sjfButton.active = processController.getButtonState(ProcessModel.SJF)
                                        srtfButton.active = processController.getButtonState(ProcessModel.SRTF)
                                        rrButton.active = processController.getButtonState(ProcessModel.RR)
                                        psButton.active = processController.getButtonState(ProcessModel.PSnon)
                                        lsButton.active = processController.getButtonState(ProcessModel.LS)
                                        priorityHeading.visible = processController.getButtonState(ProcessModel.PSnon)
                                        ticketsHeading.visible = processController.getButtonState(ProcessModel.LS)
                                        quantumHeading.visible = processController.getButtonState(ProcessModel.LS)
                                        removeHeading.activeLS = processController.getButtonState(ProcessModel.LS)
                                        rrQuantumInput.visible = processController.getButtonState(ProcessModel.RR)
                                        lsQuantumInput.visible = processController.getButtonState(ProcessModel.LS)
                                        processInput.psActive = processController.getButtonState(ProcessModel.PSnon)
                                        processInput.lsActive = processController.getButtonState(ProcessModel.LS)
                                        forceInterruptText.color = processController.getButtonState(ProcessModel.LS) ? "#ffffff" : "#828282"
                                    }
                                }

                                //Select None Button
                                SchedulingButton {
                                    id: noneSchedulingButton
                                    width: parent.width/3 - 10
                                    height: parent.height
                                    text: qsTr("Select None")
                                    anchors.right: parent.right
                                    anchors.rightMargin: parent.width/6
                                    onToggled: {
                                        processController.clearAllButtons()
                                        fcfsButton.active = processController.getButtonState(ProcessModel.FCFS)
                                        sjfButton.active = processController.getButtonState(ProcessModel.SJF);
                                        srtfButton.active = processController.getButtonState(ProcessModel.SRTF);
                                        rrButton.active = processController.getButtonState(ProcessModel.RR);
                                        psButton.active = processController.getButtonState(ProcessModel.PSnon);
                                        lsButton.active = processController.getButtonState(ProcessModel.LS);
                                        priorityHeading.visible = processController.getButtonState(ProcessModel.PSnon)
                                        ticketsHeading.visible = processController.getButtonState(ProcessModel.LS)
                                        quantumHeading.visible = processController.getButtonState(ProcessModel.LS)
                                        removeHeading.activeLS = processController.getButtonState(ProcessModel.LS)
                                        rrQuantumInput.visible = processController.getButtonState(ProcessModel.RR);
                                        lsQuantumInput.visible = processController.getButtonState(ProcessModel.LS)
                                        processInput.psActive = processController.getButtonState(ProcessModel.PSnon)
                                        processInput.lsActive = processController.getButtonState(ProcessModel.LS)
                                        forceInterruptText.color = processController.getButtonState(ProcessModel.LS) ? "#ffffff" : "#828282"
                                    }
                                }
                            }

                        }

                    }

                    //Queue heading
                    OwnText {
                        id: queueHeading
                        anchors.top: typeSelectionBorder.bottom
                        anchors.topMargin: 20
                        anchors.left: parent.left
                        anchors.leftMargin: 20
                        font.pixelSize: 22
                        text: qsTr("Arrival & Runtime of processes")
                    }

                    OwnText {
                        id: queueHint
                        anchors.top: typeSelectionBorder.bottom
                        anchors.topMargin: 28
                        anchors.right: parent.right
                        anchors.rightMargin: 40
                        font.pixelSize: 14
                        text: qsTr("Scheduling note")

                        Rectangle {
                            id: queueHelp
                            width: 14
                            height: 14
                            radius: 7
                            border.width: 1
                            border.color: "#828282"
                            anchors.left: queueHint.right
                            anchors.leftMargin: 5
                            anchors.bottom: queueHint.bottom
                            anchors.bottomMargin: 3

                            OwnText {
                                anchors.centerIn: parent
                                text: "?"
                                font.pixelSize: 8
                            }

                            MouseArea {
                                id: queueMouse
                                anchors.fill: parent
                                hoverEnabled: true
                            }

                            Hint {
                                width: 750
                                height: 40
                                anchors.bottom: queueHelp.top
                                anchors.right: queueHelp.right
                                text: "If the input data leads to multiple schedules for FCFS, the schedule sorted ascending by ID is displayed.\nIf the input data leads to multiple schedules for other algortihms, FCFS is used to determine the displayed schedule."
                                visible: queueMouse.containsMouse
                            }
                        }
                    }

                    //Queue box
                    Rectangle {
                        id: queueBorder
                        border.color: "#bbbbbb"
                        border.width: 0.5
                        width: parent.width - 30
                        height: 92
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: queueHeading.bottom

                        ProcessPreview {
                            id: processPreview
                            height: parent.height
                            width: parent.width
                            //reload result views when user changed a color
                            onClicked: {
                                resultView2.model = 0
                                resultView2.model = processController.getNumScheduleResults()
                                processDiagram.model = 0
                                processDiagram.model = processController.getNumScheduleResults()

                            }
                        }
                    }

                    //Processes heading
                    OwnText {
                        id: processHeading
                        anchors.top: queueBorder.bottom
                        anchors.topMargin: 20
                        anchors.left: parent.left
                        anchors.leftMargin: 20
                        font.pixelSize: 22
                        text: qsTr("Processes")
                    }

                    // processes input box
                    Rectangle {
                        id: processBorder
                        border.color: "#bbbbbb"
                        border.width: 0.5

                        property int amountOfEntries: processController.getProcAmountAtQueue()

                        Connections {
                            target: processController
                            onDataChanged: {
                                processBorder.amountOfEntries = processController.getProcAmountAtQueue()
                            }
                        }

                        width: parent.width - 30
                        height: amountOfEntries < 10 ? 50 + (amountOfEntries * 35) : 400
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: processHeading.bottom

                        property var addProcButtonRight : addProcess.right
                        property var clearProcButtonLeft : clearProcess.left

                        RowLayout {
                            id: tableHeading
                            width: parent.width

                            anchors.left: parent.left
                            anchors.leftMargin: 0
                            anchors.top: parent.top
                            anchors.topMargin: 2

                            OwnText {
                                text: qsTr("ID")
                                color: "#525252"
                                font.pixelSize: 16
                                anchors.left: parent.left
                                anchors.leftMargin: parent.width/20
                            }

                            OwnText {
                                text: qsTr("Arrival")
                                color: "#525252"
                                font.pixelSize: 16
                                anchors.left: parent.left
                                anchors.leftMargin: parent.width/7
                            }

                            OwnText {
                                text: qsTr("Runtime")
                                color: "#525252"
                                font.pixelSize: 16
                                anchors.left: parent.left
                                anchors.leftMargin: 2*(parent.width/7)
                            }

                            OwnText {
                                id: priorityHeading
                                text: qsTr("Priority")
                                color: "#525252"
                                font.pixelSize: 16
                                visible: processController.getButtonState(ProcessModel.PSnon)
                                anchors.left: parent.left
                                anchors.leftMargin: 3*(parent.width/7)


                                Rectangle {
                                    id: priorityHelp
                                    width: 14
                                    height: 14
                                    radius: 7
                                    border.width: 1
                                    border.color: "#828282"
                                    anchors.left: priorityHeading.right
                                    anchors.leftMargin: 3
                                    anchors.bottom: priorityHeading.bottom
                                    anchors.bottomMargin: 3

                                    Text {
                                        anchors.centerIn: parent
                                        text: "?"
                                        font.pixelSize: 8
                                        color: "#828282"
                                    }

                                    MouseArea {
                                        id: priorityHelpMouse
                                        anchors.fill: parent
                                        hoverEnabled: true
                                    }

                                    Hint {
                                        width: 250
                                        height: 20
                                        anchors.bottom: priorityHelp.top
                                        anchors.horizontalCenter: priorityHelp.horizontalCenter
                                        text: "Lower value → Higher priority"
                                        visible: priorityHelpMouse.containsMouse
                                    }
                                }
                            }

                            OwnText {
                                id: ticketsHeading
                                text: qsTr("Tickets")
                                color: "#525252"
                                font.pixelSize: 16
                                visible: processController.getButtonState(ProcessModel.LS)
                                anchors.left: parent.left
                                anchors.leftMargin: 4*(parent.width/7)

                            }

                            OwnText {
                                id: quantumHeading
                                text: qsTr("LS Quantum Usage")
                                color: "#525252"
                                font.pixelSize: 16
                                visible: processController.getButtonState(ProcessModel.LS)
                                anchors.left: parent.left
                                anchors.leftMargin: 5*(parent.width/7)

                                Rectangle {
                                    id: quantumUsageHelp
                                    width: 14
                                    height: 14
                                    radius: 7
                                    border.width: 1
                                    border.color: "#828282"
                                    anchors.left: quantumHeading.right
                                    anchors.leftMargin: 3
                                    anchors.bottom: quantumHeading.bottom
                                    anchors.bottomMargin: 3

                                    Text {
                                        anchors.centerIn: parent
                                        text: "?"
                                        font.pixelSize: 8
                                        color: "#828282"
                                    }

                                    MouseArea {
                                        id: quantumUsageHelpMouse
                                        anchors.fill: parent
                                        hoverEnabled: true
                                    }

                                    Hint {
                                        width: 250
                                        height: 20
                                        anchors.bottom: quantumUsageHelp.top
                                        anchors.horizontalCenter: quantumUsageHelp.horizontalCenter
                                        text: "LS Quantum Usage > 0"
                                        visible: quantumUsageHelpMouse.containsMouse
                                    }
                                }
                            }


                            OwnText {
                                id: removeHeading
                                property bool activeLS: processController.getButtonState(ProcessModel.LS)
                                text: qsTr("Remove")
                                color: "#525252"
                                font.pixelSize: 16
                                anchors.right: parent.right
                                anchors.rightMargin:  20
                            }
                        }

                        Rectangle {
                            width: tableHeading.width - 40
                            height: 1
                            color: "#828282"
                            anchors.left: parent.left
                            anchors.leftMargin: 20
                            anchors.top: tableHeading.bottom
                            anchors.topMargin: 1
                        }

                        ProcessInput {
                            id: processInput
                            width: parent.width
                            height: 100
                            anchors.top: tableHeading.bottom
                            anchors.topMargin: 10
                            anchors.left: tableHeading.left
                            anchors.right: tableHeading.right
                            onClicked: {
                                resultPart.visible = false
                                scrollView.contentHeight = 450 + processInput.height
                            }
                        }

                        Rectangle {
                            id: addProcess
                            width: 100
                            height: 35
                            border.color: "#828282"
                            anchors.top: processInput.bottom
                            anchors.topMargin: 5
                            anchors.left: processBorder.left
                            color: addButtonMouse.containsMouse ? "#dddddd" : "#ffffff"

                            Text {
                                text: qsTr("+ Process")
                                anchors.centerIn: parent
                                color: "#525252"
                                font.family: "Ubuntu"
                                font.pixelSize: 16
                            }

                            MouseArea {
                                id: addButtonMouse
                                anchors.fill: parent
                                cursorShape: Qt.PointingHandCursor
                                hoverEnabled: true
                                onClicked: {
                                    addProcess.focus = true
                                    processController.appendProc()
                                    processInput.positionViewAtEnd()
                                    resultPart.visible = false
                                    scrollView.contentHeight = 450 + processInput.height

                                }
                            }
                        }

                        Rectangle {
                            id: clearProcess
                            width: 100
                            height: 35
                            border.color: "#828282"
                            anchors.top: processInput.bottom
                            anchors.topMargin: 5
                            anchors.left: addProcess.right
                            anchors.leftMargin: 200
                            color: clearButtonMouse.containsMouse ? "#dddddd" : "#ffffff"

                            Text {
                                text: qsTr("- Clear")
                                anchors.centerIn: parent
                                color: "#525252"
                                font.family: "Ubuntu"
                                font.pixelSize: 16
                            }

                            MouseArea {
                                id: clearButtonMouse
                                anchors.fill: parent
                                cursorShape: Qt.PointingHandCursor
                                hoverEnabled: true
                                onClicked: {
                                    clearProcess.focus = true
                                    processController.clearProcList()
                                    processInput.positionViewAtEnd()
                                    resultPart.visible = false
                                }
                            }
                        }

                    }

                    // LS quantum Input
                    Rectangle {
                        id: lsQuantumInput
                        width: 150
                        height: 35
                        border.color: "#828282"
                        border.width: 1
                        anchors.top: processBorder.bottom
                        anchors.topMargin: 5
                        anchors.right: rrQuantumInput.visible ? rrQuantumInput.left : calcButton.left
                        anchors.rightMargin: 5
                        visible: processController.getButtonState(ProcessModel.LS)

                        OwnText {
                            id: lsQuantumHeading
                            text: "LS"
                            color: "#525252"
                            font.bold: true
                            anchors.left: parent.left
                            anchors.leftMargin: 5
                            anchors.verticalCenter: parent.verticalCenter
                        }

                        OwnText {
                            text: "Quantum"
                            color: "#525252"
                            anchors.left: lsQuantumHeading.right
                            anchors.leftMargin: 8
                            anchors.verticalCenter: parent.verticalCenter
                        }

                        TextField {
                            id: lsQuantumInputField
                            width: 35
                            height: parent.height - 4
                            horizontalAlignment: TextInput.AlignRight
                            selectByMouse: true
                            maximumLength: 2
                            inputMethodHints: Qt.ImhDigitsOnly
                            validator: IntValidator {bottom: 0}
                            font.family: "Ubuntu"
                            font.pixelSize: 16
                            placeholderText: "0"
                            anchors.right: parent.right
                            anchors.rightMargin: 5
                            anchors.bottom: lsQuantumInput.bottom
                            anchors.bottomMargin: 2
                            text: processController.getLSQuantum()
                            onActiveFocusChanged: activeFocus ? selectAll() : deselect()
                            onEditingFinished: {
                                processController.setLSQuantum(text)
                                focus = false
                            }
                        }
                    }


                    // RR quantum Input
                    Rectangle {
                        id: rrQuantumInput
                        width: 150
                        height: 35
                        border.color: "#828282"
                        border.width: 1
                        anchors.top: processBorder.bottom
                        anchors.topMargin: 5
                        anchors.right: calcButton.left
                        anchors.rightMargin: 5
                        visible: processController.getButtonState(ProcessModel.RR)

                        OwnText {
                            id: rrQuantumHeading
                            text: "RR"
                            color: "#525252"
                            font.bold: true
                            anchors.left: parent.left
                            anchors.leftMargin: 5
                            anchors.verticalCenter: parent.verticalCenter
                        }

                        OwnText {
                            text: "Quantum"
                            color: "#525252"
                            anchors.left: rrQuantumHeading.right
                            anchors.leftMargin: 8
                            anchors.verticalCenter: parent.verticalCenter
                        }

                        TextField {
                            id: rrQuantumInputField
                            width: 35
                            height: parent.height - 4
                            selectByMouse: true
                            maximumLength: 2
                            inputMethodHints: Qt.ImhDigitsOnly
                            validator: IntValidator {bottom: 0}
                            horizontalAlignment: TextInput.AlignRight
                            font.family: "Ubuntu"
                            font.pixelSize: 16
                            placeholderText: "0"
                            anchors.right: parent.right
                            anchors.rightMargin: 5
                            anchors.bottom: rrQuantumInput.bottom
                            anchors.bottomMargin: 2
                            text: processController.getRRQuantum()
                            onActiveFocusChanged: activeFocus ? selectAll() : deselect()
                            onEditingFinished: {
                                processController.setRRQuantum(text)
                                focus = false
                            }
                        }
                    }

                    //generate random button
                    Rectangle {
                        id: randButton
                        anchors.top: processBorder.bottom
                        anchors.topMargin: 5
                        anchors.left: processBorder.left
                        anchors.leftMargin: 110
                        border.color: "#828282"
                        border.width: 1
                        color: randButtonArea.containsMouse ? "#dddddd" : "#ffffff"
                        width: 180
                        height: 35

                        Text {
                            text: qsTr("+ Random Processes")
                            anchors.centerIn: parent
                            color: "#525252"
                            font.family: "Ubuntu"
                            font.pixelSize: 16
                            //font.bold: true
                        }

                        MouseArea {
                            id: randButtonArea
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            hoverEnabled: true
                            onClicked: {
                                randButton.focus = true
                                randInputView.visible = true
                                randInputDialog.visible = true
                            }
                        }
                    }

                    //random attribute dialog
                    Dialog {
                        id: randInputDialog
                        visible: false
                        title: "Attributes"

                        modal: true

                        x: (swipeView.width - width) / 2
                        y: (swipeView.height - height) / 2

                        //generate random processes window
                        Rectangle {
                            id: randInputView
                            implicitWidth: 600
                            implicitHeight: 100
                            anchors.bottomMargin: 10
                            color: "#eeeeee"
                            border.color: "#828282"
                            border.width: 1

                            ProcessGenerateAttributes {
                                id: randProcAmount
                                anchors.left: randInputView.left
                                headerText: qsTr("#Processes")
                                valueText: processController.getGenerateAmount()
                                onEditingFinished: processController.setGenerateAmount(valueText)
                            }

                            ProcessGenerateAttributes {
                                id: randProcMaxValue
                                anchors.left: randProcAmount.right
                                anchors.leftMargin: -15
                                headerText: qsTr("Max Value")
                                valueText: processController.getGenerateMax()
                                onEditingFinished: processController.setGenerateMax(valueText)

                                Rectangle {
                                    id: maxValueHelp
                                    width: 14
                                    height: 14
                                    radius: 7
                                    border.width: 1
                                    border.color: "#828282"
                                    anchors.left: randProcMaxValue.right
                                    anchors.leftMargin: -22
                                    anchors.top: randProcMaxValue.top
                                    anchors.topMargin: 12

                                    Text {
                                        anchors.centerIn: parent
                                        text: "?"
                                        font.pixelSize: 8
                                        color: "#828282"
                                    }

                                    MouseArea {
                                        id: maxValueHelpMouse
                                        anchors.fill: parent
                                        hoverEnabled: true
                                    }

                                    Hint {
                                        width: 300
                                        height: 20
                                        anchors.bottom: maxValueHelp.top
                                        anchors.horizontalCenter: maxValueHelp.horizontalCenter
                                        text: "Specifies max arrival and duration of processes"
                                        visible: maxValueHelpMouse.containsMouse
                                    }
                                }
                            }

                            ProcessGenerateAttributes {
                                id: randLSQuantumValue
                                anchors.left: randProcMaxValue.right
                                headerText: qsTr("LS Quantum")
                                valueText: processController.getGenerateLSQuantum()
                                onEditingFinished: processController.setGenerateLSQuantum(valueText)

                                Rectangle {
                                    id: lsQuantumHelp
                                    width: 14
                                    height: 14
                                    radius: 7
                                    border.width: 1
                                    border.color: "#828282"
                                    anchors.left: randLSQuantumValue.right
                                    anchors.leftMargin: -16
                                    anchors.top: randLSQuantumValue.top
                                    anchors.topMargin: 11

                                    Text {
                                        anchors.centerIn: parent
                                        text: "?"
                                        font.pixelSize: 8
                                        color: "#828282"
                                    }

                                    MouseArea {
                                        id: lsQuantumHelpMouse
                                        anchors.fill: parent
                                        hoverEnabled: true
                                    }

                                    Hint {
                                        width: 550
                                        height: 20
                                        anchors.bottom: lsQuantumHelp.top
                                        anchors.bottomMargin: -3
                                        anchors.horizontalCenter: lsQuantumHelp.horizontalCenter
                                        text: "Specify the maximum LS quantum based on which a single schedule will be computed."
                                        visible: lsQuantumHelpMouse.containsMouse
                                    }
                                }
                            }

                            ProcessGenerateAttributes {
                                id: randRRQuantumValue
                                anchors.left: randLSQuantumValue.right
                                headerText: qsTr("RR Quantum")
                                valueText: processController.getGenerateRRQuantum()
                                onEditingFinished: processController.setGenerateRRQuantum(valueText)

                                Rectangle {
                                    id: rrQuantumHelp
                                    width: 14
                                    height: 14
                                    radius: 7
                                    border.width: 1
                                    border.color: "#828282"
                                    anchors.left: randRRQuantumValue.right
                                    anchors.leftMargin: -16
                                    anchors.top: randRRQuantumValue.top
                                    anchors.topMargin: 11

                                    Text {
                                        anchors.centerIn: parent
                                        text: "?"
                                        font.pixelSize: 8
                                        color: "#828282"
                                    }

                                    MouseArea {
                                        id: rrQuantumHelpMouse
                                        anchors.fill: parent
                                        hoverEnabled: true
                                    }

                                    Hint {
                                        width: 550
                                        height: 20
                                        anchors.bottom: rrQuantumHelp.top
                                        anchors.right: parent.right
                                        anchors.rightMargin: -100
                                        text: "Specify the maximum RR quantum based on which a single schedule will be computed."
                                        visible: rrQuantumHelpMouse.containsMouse
                                    }
                                }
                            }


                            Item {
                                id: randProcSolutions
                                anchors.left: randRRQuantumValue.right
                                width: parent.width/5
                                height: parent.height/2 + parent.height/5

                                OwnText {
                                    id: randProcSolutionHeading
                                    text: "Single Schedule"
                                    color: "#525252"
                                    anchors.top: parent.top
                                    anchors.topMargin: 10
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    font.pixelSize: 14
                                }

                                Rectangle {
                                    id: altHelp
                                    width: 14
                                    height: 14
                                    radius: 7
                                    border.width: 1
                                    border.color: "#828282"
                                    anchors.left: randProcSolutionHeading.right
                                    anchors.leftMargin: 1
                                    anchors.top: randProcSolutionHeading.top
                                    anchors.topMargin: 1

                                    Text {
                                        anchors.centerIn: parent
                                        text: "?"
                                        font.pixelSize: 8
                                        color: "#828282"
                                    }

                                    MouseArea {
                                        id: altHelpMouse
                                        anchors.fill: parent
                                        hoverEnabled: true
                                    }

                                    Hint {
                                        width: 520
                                        height: 20
                                        anchors.bottom: altHelp.top
                                        anchors.right: parent.right
                                        text: "If checked, then input data will be generated that results in exactly one 1 schedule"
                                        visible: altHelpMouse.containsMouse
                                    }
                                }

                                CheckBox {
                                    id: randSolutionCheckbox
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    anchors.top: randProcSolutionHeading.bottom
                                    anchors.topMargin: 4
                                    checkState: processController.isSingleSolution() ? Qt.Checked : Qt.Unchecked
                                    onClicked: processController.setSingleSolution(randSolutionCheckbox.checked)
                                }
                            }


                            Rectangle {
                                id: randGenerateButton
                                width: randInputView.width - 40
                                height: 20
                                anchors.horizontalCenter: randInputView.horizontalCenter
                                anchors.bottom: randInputView.bottom
                                anchors.bottomMargin: 5
                                border.color: "#828282"
                                border.width: 1
                                color: randGenerateButtonMouse.containsMouse ? "#e2e2e2" : "#ffffff"

                                Text {
                                    anchors.centerIn: parent
                                    text: qsTr("Generate")
                                    color: "#525252"
                                    font.family: "Ubuntu"
                                    font.pixelSize: 12
                                }

                                MouseArea {
                                    id: randGenerateButtonMouse
                                    anchors.fill: parent
                                    cursorShape: Qt.PointingHandCursor
                                    hoverEnabled: true
                                    onClicked: {
                                        randGenerateButton.focus = true
                                        processController.setRRQuantum(processController.getGenerateRRQuantum())
                                        processController.setLSQuantum(processController.getGenerateLSQuantum())
                                        rrQuantumInputField.text = processController.getRRQuantum()
                                        lsQuantumInputField.text = processController.getLSQuantum()
                                        if (!processController.isGenerateAttributesValid()) {
                                            processController.setErrorHeading("Invalid Attributes");
                                            processController.setErrorDescription(processController.getGenerateErrorMessage());
                                            randInputDialog.visible = false
                                            processController.setError(true);
                                        }
                                        else {
                                            processController.generateRandomList();
                                            if (!processController.isGeneratedListValid()) {
                                                processController.setErrorHeading("Unable to generate");
                                                processController.setErrorDescription("Failed to generate processes with given attributes. "
                                                                                      +"Maybe increase max or decrease conflicts!");
                                                processController.setError(true);
                                            }

                                            randInputDialog.visible = false
                                            scrollView.contentHeight = 450 + processInput.height
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //calculate button
                    Rectangle {
                        id: calcButton
                        anchors.top: processBorder.bottom
                        anchors.topMargin: 5
                        anchors.right: processBorder.right
                        border.color: "#828282"
                        border.width: 1
                        color: calcButtonArea.containsMouse ? "#dddddd" : "#ffffff"
                        width: 120
                        height: 35

                        signal clicked()

                        Text {
                            text: qsTr("Calculate")
                            anchors.centerIn: parent
                            color: "#525252"
                            font.family: "Ubuntu"
                            font.pixelSize: 16
                            font.bold: true
                        }

                        MouseArea {
                            id: calcButtonArea
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            hoverEnabled: true
                            onClicked: {
                                calcButton.focus = true
                                inputWithResultPage.showResult = true
                                processController.clearProcResultList()
                                processController.calcScheduling()
                                if (!processController.isSchedulingValid()) {
                                    processController.setErrorHeading("Unable to schedule");
                                    processController.setErrorDescription(processController.getSchedulingErrorMessage());
                                    processController.setError(true);
                                }
                                else {
                                    scrollView.contentHeight = 450 + processInput.height + processController.getNumScheduleResults() * 160 + 200
                                    processDiagram.model = 0
                                    processDiagram.model = processController.getNumScheduleResults()
                                    inputWithResultPage.showResult = true
                                    resultPart.visible = true
                                    resultView2.model = processController.getNumScheduleResults()
                                    calcButton.clicked()
                                    scrollView.triggered()
                                }
                            }
                        }

                    }


                    Item {
                        id: resultPart

                        width: swipeView.width - 15
                        height: swipeView.height - 2
                        anchors.top: calcButton.bottom
                        anchors.topMargin: 40
                        visible: false

                        Rectangle {
                            width: parent.width
                            height: 1
                            color: "#828282"
                            anchors.top: parent.top
                        }

                        Text {
                            id: resultHeader
                            text: qsTr("Results")
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.top: parent.top
                            anchors.topMargin: 20
                            font.pixelSize: 22
                            color: "#828282"
                            font.bold: true
                            font.family: "Ubuntu"
                        }

                        Rectangle {
                            id: resultBorder2
                            width: parent.width - 30
                            height: parent.height - 120
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.top: resultHeader.bottom
                            anchors.topMargin: 10

                            signal clicked()

                            ProcessDetailDialog {
                                id: resultDialog
                            }

                            ProcessResult {
                                id: resultView2
                                onClicked: {
                                    resultDialog.clicked()
                                    resultDialog.runningColor = processController.getColorById(processController.getMetricId())
                                    resultDialog.runningModel = processController.getNumQueue() + 2
                                    resultDialog.runningId= qsTr("Process ") + qsTr("ID: ") + "P"+processController.getMetricId()
                                    resultDialog.runningPosition = qsTr("Timestamp")+ ": " + processController.getMetricPos()
                                    resultDialog.runningRespone = qsTr("Response time: ") + processController.getMetricResponse()
                                    resultDialog.runningWait= qsTr("Waiting time: ") + processController.getMetricWait()
                                    resultDialog.runningTurnaround = qsTr("Turnaround time: ") + processController.getMetricTurnaround()
                                    resultDialog.y = processController.getClickPosition()
                                    resultDialog.runningVisible = true
                                }
                            }

                        }

                    }
                }
            }
        }

        Item {
            id: secondPage

            Rectangle {
                id: resultBorder
                //border.color: "#bbbbbb"
                //border.width: 0.5
                width: parent.width
                height: parent.height
                anchors.horizontalCenter: parent.horizontalCenter

                signal clicked()

                    Item {
                        id: resultItem
                        height: resultBorder.height
                        width: resultBorder.width

                        ProcessDetailDialog {
                            id: resultDialog2
                        }

                        ProcessDiagram {
                            id: processDiagram
                            anchors.fill: parent
                            onClicked: {
                                resultDialog2.clicked()
                                resultDialog2.runningColor = processController.getColorById(processController.getMetricId())
                                resultDialog2.runningModel = processController.getNumQueue() + 2
                                resultDialog2.runningId = qsTr("Process ") + qsTr("ID: ") + "P"+processController.getMetricId()
                                resultDialog2.runningPosition = qsTr("Timestamp")+ ": " + processController.getMetricPos()
                                resultDialog2.runningRespone = qsTr("Response time: ") + processController.getMetricResponse()
                                resultDialog2.runningWait = qsTr("Waiting time: ") + processController.getMetricWait()
                                resultDialog2.runningTurnaround = qsTr("Turnaround time: ") + processController.getMetricTurnaround()
                                resultDialog2.runningVisible = true
                            }
                        }
                    }
            }
        }
    }

    Rectangle {
        id: errorMessage
        height: parent.height/4 + 20
        width: parent.width
        color: "#E53935"
        anchors.verticalCenter: parent.verticalCenter
        visible: processController.isErrorActive()

        Connections {
            target: processController
            onDataChanged: {
                errorMessage.visible = processController.isErrorActive()
                errorHeading.text = processController.getErrorHeading()
                errorDescription.text = processController.getErrorDescription()
            }
        }


        Text {
            id: errorHeading
            color: "#ffffff"
            text: "Error"
            font.family: "Ubuntu"
            font.bold: true
            font.pixelSize: 26
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 20
        }

        Rectangle {
            height: 1
            border.width: 4
            border.color: "#ffffff"
            anchors.left: errorHeading.left
            anchors.right: errorHeading.right
            anchors.top: errorHeading.bottom
            anchors.topMargin: 2
        }

        Rectangle {
            id: descriptionBox
            width: parent.width - 60
            height: parent.height/3
            anchors.top: errorHeading.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 30
            border.width: 1
            border.color: "#ffffff"
            color: "#E53935"
        }

        Text {
            id: errorDescription
            color: "#ffffff"
            text: "If you see this text, something went wrong!"
            font.family: "Ubuntu"
            font.pixelSize: 16
            anchors.verticalCenter: descriptionBox.verticalCenter
            anchors.left: descriptionBox.left
            anchors.leftMargin: 10
            anchors.right: descriptionBox.right
            anchors.rightMargin: 10
        }

        Rectangle {
            id: errorButton
            height: 30
            width: 80
            color: errorButtonMouse.containsMouse ? "#ffffff" : "#E53935"
            border.width: 1
            border.color: "#ffffff"
            anchors.top: descriptionBox.bottom
            anchors.topMargin: 5
            anchors.right: descriptionBox.right

            Text {
                text: "OK"
                anchors.centerIn: parent
                color: errorButtonMouse.containsMouse ? "#E53935" : "#ffffff"
                font.family: "Ubuntu"
                font.pixelSize: 14
            }

            MouseArea {
                id: errorButtonMouse
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                onClicked: {
                    processController.setError(false)
                    errorMessage.visible = false
                }
            }
        }
    }
}
