#pragma once

#include <QAbstractListModel>


#include <QObject>
#include <QVector>
#include <QPair>
#include <QPoint>
#include <QMetaType>



class PageFrame
{


public:
		PageFrame(int size) : m_size(size) 
		{
			m_frame = QVector<QPair<int,int>>(size, QPair<int,int>(0,0));
		}
		PageFrame(int size, const int content, const int lastaccessed) : m_size(size)
		{
			m_frame = QVector<QPair<int,int>>(size, QPair<int,int>(lastaccessed,content));
		}

		PageFrame() : m_size(0){};

		PageFrame(const PageFrame & pageFrame)
		{
			m_size = pageFrame.m_size;
			m_frame = QVector<QPair<int,int>>(pageFrame.m_frame);
		}

		PageFrame& operator=(const PageFrame& pageFrame)
		{
			m_size = pageFrame.m_size;
			m_frame = QVector<QPair<int,int>>(pageFrame.m_frame);

			return *this;
		}

		~PageFrame() = default;

		Q_INVOKABLE int getContent(int index) const
		{
			return m_frame[index].second;
		}

		Q_INVOKABLE int getLastAccessed(int index) const
		{
			return m_frame[index].first;
		}

		QPoint getPair(int index) const
		{
			return QPoint(m_frame[index].first, m_frame[index].second);
		}

		void set(int index, int content, int lastaccessed)
		{
			m_frame[index] = QPair<int,int>(lastaccessed, content);
		}

		int contains(int element)
		{
			int i = 0;
			for(const auto & e : m_frame)
			{
				if(e.second == element)
				{
					return i;
				}
				i++;
			}
			return -1;
		}

		QVector<int> getContents()
		{
			QVector<int> contents = QVector<int>(m_size);
			for(const auto & frame : m_frame)
			{
				contents.push_back(frame.second);
			}
			return contents;
		}

		int getSize() const
		{
			return m_size;
		}

		int m_size;
private:
		QVector<QPair<int,int>> m_frame;

};

