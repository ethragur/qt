#pragma once


#include <QObject>
#include <QVector>
#include <QDebug>
#include "pageframe.h"


class PagingAlgorithm 
{

	public:
		//Overwrite this function for the different Algorithms
		virtual void solve() = 0;

		void print(int step) const 
		{
			qDebug() << m_pageFaults[step][0] << m_pageFaults[step][1] << m_pageFaults[step][2];
		}



		const QVector<PageFrame>& getPageFrames() const 
		{
			return m_pageFrames;
		}

		const QVector<QVector<int>>& getPageFaults() const
		{
			return m_pageFaults;
		}

		int getPageFaultsCount() const
		{
			return m_pageFaultsCount;
		}

		void reset(int pageFramesCount, const QVector<int> & input)
		{
			m_pageFramesCount = pageFramesCount;
			m_input = input;
			m_pageFrames.clear();
			m_pageFrames.push_back(PageFrame(m_pageFramesCount));

			m_pageFaults.clear();
			m_pageFaults.push_back(QVector<int>(3,0));
			m_pageFaultsCount = 0;
		}


		virtual int getPointerPos(int) const
		{
			//Return -1 for all other algorithms, clock will Overwrite this function
			return -1;
		}




	protected:
		//Protected constructor, should never be instanced directly
		PagingAlgorithm(int pageFramesCount, const QVector<int> & input) 
			: m_pageFramesCount(pageFramesCount), m_input(input) 
		{
			m_pageFrames =  QVector<PageFrame>();
			m_pageFrames.push_back(PageFrame(m_pageFramesCount));

			m_pageFaults.push_back(QVector<int>(3,0));
			m_pageFaultsCount = 0;
		}


		// Number of page Frames
		int m_pageFramesCount;
		// Number of page Faults that occured
		int m_pageFaultsCount;
		// Input List TODO: make this shared_ptr (all Algorithms will have the same input list)
		QVector<int> m_input;
		//Page Frames (For each step)
		QVector<PageFrame> m_pageFrames;
		// 3 Fields for Page Faults:  0: page Frame, 1: old page, 2: new page
		QVector<QVector<int>> m_pageFaults;
};
