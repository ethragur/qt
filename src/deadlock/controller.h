/*
 * Author: Robert Schaffenrath
 * Supervisor: Dr. Thomas Fahringer
 *
 * University of Innsbruck, Institute of Computer Science, http://dps.uibk.ac.at
 */

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QVector>

#include "src/deadlock/banker_process.h"
#include "src/deadlock/calculate.h"

class Controller : public QObject
{
    Q_OBJECT
public:

    explicit Controller(QObject *parent = nullptr);

    QVector<banker_process_t> getProcList() const;

    bool setProcAt(int index, const banker_process_t &proc);

signals:
    void preProcAppend();
    void postProcAppend();

    void preProcRemove(int index);
    void postProcRemove();

    void preClearList();
    void postClearList();

    void dataChanged();

public slots:
    void appendProc();
    void popProc();
    void setNumOfProcs(QString amountStr);
    void resetInternalData() {procList.clear();}
    int getNumOfProcs();
    int getNumOfResources();
    void clearInput();
    QString getResourceColor(int index);

    void addResource();
    void removeResource();
    void setAllocationAt(int procIndex, int resourceIndex, int value);
    void setNeedAt(int procIndex, int resourceIndex, int value);
    void setMaxAt(int procIndex, int resourceIndex, int value);
    bool isNewAllocationTotalValid(int resourceIndex, int value);
    void setAllocationTotalAt(int resourceIndex, int value);
    int getAllocationAt(int procIndex, int resourceIndex);
    int getNeedAt(int procIndex, int resourceIndex);
    int getMaxAt(int procIndex, int resourceIndex);
    bool isMaxValid(int procIndex, int resourceIndex, int value);
    int getAllocationTotalAt(int resourceIndex);
    int getAllocationSumAt(int resourceIndex);
    bool isAllocationTotalValid(int index);

    void toggleNeed();
    bool isNeedSelected();

    void setAvailableAt(int index, int value);
    int getAvailableAt(int index);

    void calcSequences();
    int getNumOfSequences();
    int getSeqResultAt(int resultIndex, int posIndex);

    bool isErrorActive();
    void setError(bool state);
    void setErrorHeading(QString heading);
    void setErrorDescription(QString description);
    QString getErrorHeading();
    QString getErrorDescription();

    void setSequenceIndex(int index);
    int getSequenceIndex();
    int getStepNeed(int procIndex, int posIndex);
    void calcSteps();
    int getStepAt(int lineIndex, int posIndex);
    int getStepId(int lineIndex);

    void changeButtonState();
    bool buttonChanged();
    void findMinAvail();

    int getGenerateProcAmount();
    void setGenerateProcAmount(int amount);
    int getGenerateResourceAmount();
    void setGenerateResourceAmount(int amount);
    bool isGenerationAttributesValid();
    QString getGenerateErrorMessage();
    void generateProcs();



private:
    QVector<banker_process_t> procList;
    QVector<int> available;
    QVector<int> allocationSum;
    QVector<int> allocationTotal;
    QVector<QString> resourceColors;


    result_seq_t sequences;
    result_steps_t steps;
    int numOfResources = 0;
    int selectedSequence = -1;
    int generate_proc_amount = 0;
    int generate_resource_amount = 0;
    bool needSelected = false;
    bool buttonStateChanged = false;
    bool error = false;
    QString errorHeading;
    QString errorDescription;
    int maxProcIdBanker = 0;
};

#endif // CONTROLLER_H
